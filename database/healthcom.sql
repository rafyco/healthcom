-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Czas wygenerowania: 08 Gru 2013, 00:35
-- Wersja serwera: 5.5.27
-- Wersja PHP: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Baza danych: `healtchcom`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `api`
--

CREATE TABLE IF NOT EXISTS `api` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `idUsera` bigint(20) NOT NULL,
  `kod` char(30) COLLATE utf8_polish_ci NOT NULL,
  `Apipanel` bigint(20) NOT NULL,
  `data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `end` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `apipanel`
--

CREATE TABLE IF NOT EXISTS `apipanel` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) COLLATE utf8_polish_ci NOT NULL,
  `numer` char(30) COLLATE utf8_polish_ci NOT NULL,
  `data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=2 ;

--
-- Zrzut danych tabeli `apipanel`
--

INSERT INTO `apipanel` (`id`, `name`, `numer`, `data`) VALUES
(1, 'Aplikacja Mobilna (Karol Kowalczyk)', '6kMX1KEOxvtaLR52fuRT7ECEZ1IQUS', '2013-11-01 16:45:00');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `authorize`
--

CREATE TABLE IF NOT EXISTS `authorize` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kod` char(30) COLLATE utf8_polish_ci NOT NULL,
  `idUsera` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `kod` (`kod`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=15 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `dzienniczek`
--

CREATE TABLE IF NOT EXISTS `dzienniczek` (
  `idDzienniczka` bigint(20) NOT NULL AUTO_INCREMENT,
  `idPacjenta` bigint(20) NOT NULL,
  `ktozapisal` text COLLATE utf8_polish_ci,
  `data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `cisskurcz` int(11) NOT NULL,
  `cisrozkurcz` int(11) NOT NULL,
  `samopoczucie` smallint(11) NOT NULL,
  `tetno` int(11) NOT NULL DEFAULT '0',
  `tresc` text COLLATE utf8_polish_ci NOT NULL,
  `uprawnienia` int(11) NOT NULL,
  PRIMARY KEY (`idDzienniczka`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=2 ;


--
-- Wyzwalacze `dzienniczek`
--
DROP TRIGGER IF EXISTS `Usuwanie komentarzy`;
DELIMITER //
CREATE TRIGGER `Usuwanie komentarzy` AFTER DELETE ON `dzienniczek`
 FOR EACH ROW DELETE FROM `komentarze`
WHERE OLD.idDzienniczka=`komentarze`.idPosta
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `gabinety`
--

CREATE TABLE IF NOT EXISTS `gabinety` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nazwa` varchar(100) COLLATE utf8_polish_ci NOT NULL,
  `ulica` varchar(45) COLLATE utf8_polish_ci NOT NULL,
  `numer` varchar(10) COLLATE utf8_polish_ci NOT NULL,
  `kodPocz` varchar(5) COLLATE utf8_polish_ci NOT NULL,
  `miasto` varchar(45) COLLATE utf8_polish_ci NOT NULL,
  `telefon` varchar(12) COLLATE utf8_polish_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=8 ;



-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `grkrwi`
--

CREATE TABLE IF NOT EXISTS `grkrwi` (
  `id` int(11) NOT NULL,
  `grupa` varchar(2) COLLATE utf8_polish_ci NOT NULL,
  `rh` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `grkrwi`
--

INSERT INTO `grkrwi` (`id`, `grupa`, `rh`) VALUES
(1, '0', 0),
(2, '0', 1),
(3, 'A', 0),
(4, 'A', 1),
(5, 'B', 0),
(6, 'B', 1),
(7, 'AB', 0),
(8, 'AB', 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `hashforgot`
--

CREATE TABLE IF NOT EXISTS `hashforgot` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kod` char(30) COLLATE utf8_polish_ci NOT NULL,
  `idUsera` bigint(20) NOT NULL,
  `data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=1 ;


-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `komentarze`
--

CREATE TABLE IF NOT EXISTS `komentarze` (
  `idkoment` bigint(20) NOT NULL AUTO_INCREMENT,
  `idPosta` bigint(20) NOT NULL,
  `idAutora` bigint(20) NOT NULL,
  `data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `tresc` text COLLATE utf8_polish_ci NOT NULL,
  PRIMARY KEY (`idkoment`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=1 ;



-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `lekarstwa`
--

CREATE TABLE IF NOT EXISTS `lekarstwa` (
  `idLeku` bigint(20) NOT NULL AUTO_INCREMENT,
  `idPac` bigint(20) NOT NULL,
  `nazwa` varchar(50) COLLATE utf8_polish_ci NOT NULL,
  `rodzajlacz` int(11) NOT NULL,
  `at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idLeku`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=1;



--
-- Wyzwalacze `lekarstwa`
--
DROP TRIGGER IF EXISTS `Leki`;
DELIMITER //
CREATE TRIGGER `Leki` AFTER DELETE ON `lekarstwa`
 FOR EACH ROW DELETE FROM `przyjmowanie`
WHERE OLD.idLeku=`przyjmowanie`.idLeku
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `lekarze`
--

CREATE TABLE IF NOT EXISTS `lekarze` (
  `id` int(11) DEFAULT NULL,
  `nrPWZ` char(7) COLLATE utf8_polish_ci DEFAULT NULL,
  `pesel` char(11) COLLATE utf8_polish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;



-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `lekarzgabinet`
--

CREATE TABLE IF NOT EXISTS `lekarzgabinet` (
  `idLekarza` bigint(11) NOT NULL,
  `idGabinet` bigint(11) NOT NULL,
  `idpol` bigint(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`idpol`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=1 ;



-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `pacjenci`
--

CREATE TABLE IF NOT EXISTS `pacjenci` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `pesel` char(11) COLLATE utf8_polish_ci DEFAULT NULL,
  `grupakrwi` int(11) NOT NULL,
  `nrubez` int(11) NOT NULL,
  `tel` varchar(11) CHARACTER SET latin1 NOT NULL,
  `ulica` varchar(45) COLLATE utf8_polish_ci DEFAULT NULL,
  `nrDomu` varchar(10) COLLATE utf8_polish_ci DEFAULT NULL,
  `kodPocztowy` varchar(5) COLLATE utf8_polish_ci DEFAULT NULL,
  `miasto` varchar(45) COLLATE utf8_polish_ci DEFAULT NULL,
  `idDoc` bigint(20) NOT NULL DEFAULT '0',
  `isDoc` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;



-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `porady`
--

CREATE TABLE IF NOT EXISTS `porady` (
  `idPorady` int(11) NOT NULL AUTO_INCREMENT,
  `idAutora` int(11) DEFAULT NULL,
  `tresc` text CHARACTER SET utf8 COLLATE utf8_bin,
  PRIMARY KEY (`idPorady`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=11 ;



-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `przyjaznie`
--

CREATE TABLE IF NOT EXISTS `przyjaznie` (
  `idprzyjazni` bigint(20) NOT NULL AUTO_INCREMENT,
  `idOne` bigint(20) NOT NULL,
  `idTwo` bigint(20) NOT NULL,
  `isOne` tinyint(1) NOT NULL DEFAULT '0',
  `isTwo` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idprzyjazni`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=21 ;



--
-- Struktura tabeli dla tabeli `przyjmowanie`
--

CREATE TABLE IF NOT EXISTS `przyjmowanie` (
  `idPrzyjmowania` bigint(20) NOT NULL AUTO_INCREMENT,
  `idLeku` bigint(20) NOT NULL,
  `data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ilosc` varchar(10) COLLATE utf8_polish_ci NOT NULL DEFAULT '1 szt.',
  `isprzyjal` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idPrzyjmowania`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=48 ;




--
-- Struktura tabeli dla tabeli `rodzaj`
--

CREATE TABLE IF NOT EXISTS `rodzaj` (
  `idRodzaju` int(11) NOT NULL,
  `rodzaj` varchar(30) COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `rodzaj`
--

INSERT INTO `rodzaj` (`idRodzaju`, `rodzaj`) VALUES
(1, 'antybiotyk'),
(2, 'witaminowy'),
(3, 'odpornościowy'),
(4, 'immunologiczny'),
(5, 'ziołowy'),
(6, 'inny');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `udostepnienia`
--

CREATE TABLE IF NOT EXISTS `udostepnienia` (
  `idUdst` bigint(20) NOT NULL AUTO_INCREMENT,
  `idViewer` bigint(20) NOT NULL,
  `idPac` bigint(20) NOT NULL,
  `dataUdst` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idUdst`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=16 ;



--
-- Struktura tabeli dla tabeli `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `status` tinyint(1) DEFAULT '0',
  `email` varchar(60) COLLATE utf8_polish_ci DEFAULT NULL,
  `imie` varchar(60) COLLATE utf8_polish_ci DEFAULT NULL,
  `nazwisko` varchar(60) COLLATE utf8_polish_ci NOT NULL,
  `rola` varchar(10) COLLATE utf8_polish_ci DEFAULT NULL,
  `hash` text COLLATE utf8_polish_ci,
  `at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=2 ;

--
-- Zrzut danych tabeli `users`
--

INSERT INTO `users` (`id`, `status`, `email`, `imie`, `nazwisko`, `rola`, `hash`, `at`) VALUES
(1, 1, 'admin@health.com', 'Jan', 'Niezbędny', 'admin', 'd4fbb7d8d5603db43ac2094f5955787c', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ustawienia`
--

CREATE TABLE IF NOT EXISTS `ustawienia` (
  `wiadommail` tinyint(1) NOT NULL DEFAULT '1',
  `apilog` tinyint(1) NOT NULL DEFAULT '1',
  `upraw` int(11) NOT NULL DEFAULT '0',
  `idUsera` int(10) unsigned NOT NULL,
  PRIMARY KEY (`idUsera`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;




--
-- Struktura tabeli dla tabeli `wiadomosci`
--

CREATE TABLE IF NOT EXISTS `wiadomosci` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `od` bigint(20) DEFAULT NULL,
  `do` bigint(20) DEFAULT NULL,
  `temat` text COLLATE utf8_polish_ci NOT NULL,
  `tresc` text COLLATE utf8_polish_ci NOT NULL,
  `read` tinyint(1) NOT NULL DEFAULT '0',
  `data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=34 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
