<?php
error_reporting(E_ALL|E_STRICT);
date_default_timezone_set('UTC');

$arrLocales = array('pl_PL', 'pl','Polish_Poland.28592');
setlocale( LC_ALL, $arrLocales );
setlocale(LC_TIME,"pl_PL.UTF-8");


set_include_path('.' . PATH_SEPARATOR . './Library'
   . PATH_SEPARATOR . './Application/Models'
   . PATH_SEPARATOR . get_include_path());
   
include "Zend/Loader.php";
$exit = false;
$css = null;
//Wczytywanie klas


function __autoload($className){
	Zend_Loader::loadClass($className);
}

/** 
	Funkcja wczytująca stronę instalacji oraz przerywająca program.
*/
function install(){
	$layout = new Zend_Layout();
	$layout->setLayoutPath('./Application/views/layout');
	$view = new Zend_View();
	$view->title = "Przygotuj bazę danych do pracy";
	$view->setScriptPath('./Application/views/scripts/error/');
	$layout->content = $view->render("DB.phtml");
	$layout->setLayout('welcome');
	echo $layout->render();
	exit();
}

	// Wczytanie rejestru
	$registry = Zend_Registry::getInstance();
	
	// Uruchomienie sesji
	Zend_Session::start();
	
	
try{ // Wczytanie konfiguracji, styli i bazy danych
	$config = new Zend_Config_Ini('./Application/config.ini' , 'general');	
	$registry->set('config' , $config);

	//Ustawianie stylu css
	if(isset($config->my->config->css)){
		$css =  $config->my->config->css;
	}
	
	// ustawienie bazy danych
	$db = Zend_Db::factory('PDO_MYSQL', $config->db->config->toArray() );
	Zend_Db_Table::setDefaultAdapter($db);


} catch (Zend_Config_Exception $ex){ // Domyślna baza danych
	$db = Zend_Db::factory('PDO_MYSQL',array(
		'dbname' => 'healthcom',
		'password' => 'xxx',
		'username' => 'healthcom'
	));
	Zend_Db_Table::setDefaultAdapter($db);
	$exit = true;
}
	
	// ustawienie maila
	if ('development' === getenv('APPLICATION_ENV')) {
		$transport = new Rafyco_MailTransport();
		$transport->setSavePath('./../');
	} else {
		error_reporting(0);
		$transport = new Zend_Mail_Transport_Sendmail();
	}
	Zend_Mail::setDefaultTransport($transport);
	//Zend_Mail::setDefaultFrom('admin@health.com');
	
	
	if($css == null){
			$css = 'default';
		}
	$registry->set('css', $css );
	
	// Ustawienie layouta
	Zend_Layout::startMvc(array(
		'layout'     => 'default',
		'layoutPath' => './Application/views/layout',
	));
	// ustawienie kontrolera
	$frontController = Zend_Controller_Front::getInstance();
	$frontController->throwExceptions(false);
	if(isset($config->db->config->baseurl)){
		$frontController->setBaseUrl($config->db->config->baseurl);
	} else {
		$frontController->setBaseUrl('/healthcom'); 
	}
	$frontController->setControllerDirectory('./Application/Controllers');

try{
	$user = new Application_Models_Users();
	$user->fetchRow();
	$frontController->dispatch();
} catch (Zend_Db_Adapter_Exception $ex){
	$exit = true;
} catch(Zend_Db_Table_Exception $ex){
	$exit = true;
}

if($exit) install(); // Gdy nie można wczytać ustawień