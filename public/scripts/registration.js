
var registration ={
	
	init:function(){
		if($('#role-d').is(':checked')){
			$('span.changer').html('Czy chcesz zarejestrować się jako <a href="javascript:registration.change()">pacjent</a>?');
			$('#pwz_no-label').add('#pwz_no-element').show();
			$('#role-element').add('#role-label').hide();
			$('#blood-label').add('#blood-element').hide();
			$('#security_no-label').add('#security_no-element').hide();
			$('#address_street-label').add('#address_street-element').hide();
			$('#address_city-label').add('#address_city-element').hide();
			$('#address_home_no-label').add('#address_home_no-element').hide();
			$('#address_code-label').add('#address_code-element').hide();
			$('#phone-label').add('#phone-element').hide();
			$('h2').html('Rejestracja Doktora');
		} else {
			$('span.changer').html('Czy chcesz zarejestrować się jako <a href="javascript:registration.change()">lekarz</a>?');
			$('#pwz_no-label').add('#pwz_no-element').add('#role-element').add('#role-label').hide();
			$('#blood-label').add('#blood-element').show();
			$('h2').html('Rejestracja Pacjenta');
		}
		
	},	
	reload:function(){
	
		alert('nic ciekawego');
	
	},
	
	whatFind: function(){
		var zmiennap = $('#role-p').is(':checked');
		var zmiennad = $('#role-d').is(':checked');
		
		if(zmiennap){
			return 'p';
		} else if (zmiennad){
			return 'd';
		} else {
			return 'e';
		}
		
		
	},
	
	test: function(){
		alert(this.whatFind());
	},
	
	go: function(){ // Wyświetlanie innego formularza
		if(this.whatFind()=='p'){
			$('#pwz_no-label').add('#pwz_no-element').hide();
			$('#blood-label').add('#blood-element').show();
		} else 
		
		if(this.whatFind()=='d'){
			$('#pwz_no-label').add('#pwz_no-element').show();
			$('#blood-label').add('#blood-element').hide();
		} else
		
		if(this.whatFind()=='e'){
			$('#pwz_no-label').add('#pwz_no-element').show();
			$('#blood-label').add('#blood-element').show();
			$('#role-element').add('#role-label').show();
			$('span.changer').html('Wystąpił błąd, wyświetlono wszytkie pola formularza');
		}
		
	
	},
	
	setDoctor: function(){ 
		//$('#role-p').attr('checked',none);
		$('#role-d').prop('checked',true);
		
		registration.go();
		$('span.changer').html('Czy chcesz zarejestrować się jako <a href="javascript:registration.setPatient()">pacjent</a>?');
		$('#security_no-label').add('#security_no-element').hide();
		$('#address_street-label').add('#address_street-element').hide();
		$('#address_home_no-label').add('#address_home_no-element').hide();
		$('#address_code-label').add('#address_code-element').hide();
		$('#address_city-label').add('#address_city-element').hide();
		$('#phone-label').add('#phone-element').hide();
		$('h2').html('Rejestracja Lekarza');
		
		
	},
	
	setPatient: function(){
		//$('#role-d').attr('checked',false);
		$('#role-p').prop('checked',true);
		
		registration.go();
		$('#security_no-label').add('#security_no-element').show();
		$('#address_street-label').add('#address_street-element').show();
		$('#address_home_no-label').add('#address_home_no-element').show();
		$('#address_code-label').add('#address_code-element').show();
		$('#address_city-label').add('#address_city-element').show();
		$('#phone-label').add('#phone-element').show();
		$('span.changer').html('Czy chcesz zarejestrować się jako <a href="javascript:registration.setDoctor()">lekarz</a>?');
		$('h2').html('Rejestracja Pacjenta');
		
	},
	
	change: function(){
		if(registration.whatFind() == 'p'){
			registration.setDoctor();
		} else {
			registration.setPatient();
		}
	}
	
}

$(document).ready(registration.init);