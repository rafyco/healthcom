$('document').ready(function() {

	$('#banner-2 img').load(function() {
		$('#banner-loader').fadeOut({
			complete: function() {
				$('#banner-2').fadeToggle();
			}
		});
	});

	var imagesCount = 0;
	$("#banner-1 img").load(function() {
		imagesCount++;
		if(imagesCount == $("#banner-1 img").length) {
			$('#banner-1').fadeToggle({
				duration: 2500,
				complete: function() {
					$('#banner-2').hide();
				}
			});
		}
	});

});