<?php

class Application_Form_Edition extends Zend_Form{

	private $dane;
	
	public function getDana($key){
		return $this->dane[$key];
	}

	public function __construct($dane){
		$this->dane = $dane;
		parent::__construct();
	}
	
	public function init(){
	
		$first_name = $this->createElement('text','first_name')
			->setLabel('Imię')
			->setValue($this->getDana('imie'))
			->setRequired(true)
			->addValidator(new Zend_Validate_StringLength(array( 'min' => 0, 'max' => 60)));
		
		$last_name = $this->createElement('text','last_name');
		$last_name->setLabel('Nazwisko')
			->setValue($this->getDana('nazwisko'))
			->setRequired(true)
			->addValidator(new Zend_Validate_StringLength(array( 'min' => 0, 'max' => 60)));
			
		$this->addElements(array($first_name,$last_name));	
			
		// button potwierdzajacy
		
		$submit_btn = $this->createElement('submit', 'sub_edit');
		$submit_btn->setLabel('Edytuj dane');
		
		
		$this->addElement($submit_btn);
	}

	public function __toString(){
		
		$old = parent::__toString();
		$co = array('<dl class="zend_form">','</dl>','<dt ','<dd ','</dt>','</dd>',"regulamin");
		$naco = array('','','<div class="object">'."\n".'<div ','<div ','</div>',"</div>\n</div>",'<a href="'.Zend_Controller_Front::getInstance()->getBaseUrl().'/index/rules">regulamin</a>');
		return str_replace($co,$naco,$old);
	}

	

	
		
}
