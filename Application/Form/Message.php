<?php 
	
class Application_Form_Message extends Zend_Form{

	public function init(){
	
			
		$do = $this->createElement('text','ddo');
		$do->setLabel('Adresat')->setRequired(true)->setAttrib('size',30)->
			addFilters(array(
					new Zend_Filter_StringToLower(),
					new Zend_Filter_StringTrim(),
					new Zend_Filter_StripNewlines(),
					new Zend_Filter_StripTags()
			))->
            addValidators(array(
					new Zend_Validate_EmailAddress(),
					new Zend_Validate_NotEmpty()
			));
		if(isset($_GET['mailto'])) $do->setValue($_GET['mailto']);
		
		$temat = $this->createElement('text','temat');
		$temat->setLabel('Temat: ')->setAttrib('size',30);
		if(isset($_GET['subject'])) $temat->setValue($_GET['subject']);
		
		
		$tresc = $this->createElement('textarea', 'tresc');
		$tresc->setLabel('Tresc:');
		$tresc->setAttrib("rows", 9);
		if(isset($_GET['body'])) $tresc->setValue($_GET['body']);
		
		$this->addElements(array($do,$temat,$tresc,
			array('submit', 'submit', array('label' => 'Wyslij'))
        ));

	}
	
	public function isValid($data){
	
		//Wykonanie w rodzicu
		$ret = parent::isValid($data);
		
		if (!Application_Models_Users::isUserWork($this->ddo->getValue())){
			
			$this->ddo->addError("Nie mozesz napisac do tej osoby");
			$ret = false;
		}
		
		
		return $ret;
		
	}

	public function __toString(){
		
		$old = parent::__toString();
		$co = array('<dl class="zend_form">','</dl>','<dt ','<dd ','</dt>','</dd>',"regulamin");
		$naco = array('','','<div class="object">'."\n".'<div ','<div ','</div>',"</div>\n</div>",'<a href="'.Zend_Controller_Front::getInstance()->getBaseUrl().'/index/rules">regulamin</a>');
		return str_replace($co,$naco,$old);
	}

	


}
