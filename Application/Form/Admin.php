<?php

class Application_Form_Admin extends Zend_Form{

	public function init(){
	
		$email = $this->createElement('text','email');
		$email->setLabel('E-mail');
		$email->setRequired(true);
		$email->addValidators(array(
			new Zend_Validate_EmailAddress(),
			new Zend_Validate_NotEmpty(),
			new Zend_Validate_StringLength(array( 'min' => 0, 'max' => 60))
		));
		$email->addFilters(array(
			new Zend_Filter_StringTrim(),
			new Zend_Filter_StripNewlines(),
			new Zend_Filter_StringToLower()
		));
		
		$first_name = $this->createElement('text','first_name');
		$first_name->setLabel('Imię');
		$first_name->setRequired(true);
		$first_name->addValidator(new Zend_Validate_StringLength(array( 'min' => 0, 'max' => 60)));
		
		$last_name = $this->createElement('text','last_name');
		$last_name->setLabel('Nazwisko');
		$last_name->setRequired(true);
		$last_name->addValidator(new Zend_Validate_StringLength(array( 'min' => 0, 'max' => 60)));
		
		
		
		$submit_btn = $this->createElement('submit', 'submit_btn');
		$submit_btn->setLabel('Dodaj Admina');
		
		
		$this->addElements(array(
			$email,
			$first_name,
			$last_name,
			
			$submit_btn
			 
		));
	}
	
	/**
        Metoda sprawdzająca czy pola są poprawne
    */
    public function isValid($data){

        //Wykonanie w rodzicu
        $ret = parent::isValid($data);

        // Warunek mogący uznać, że się nie formularz jest niepoprawnie wypełniony
        if(Application_Models_Users::isUserExist($this->email->getValue())){
			$this->email->addError("Niestety ten adres mail jest już zarejestrowany.");
			$ret = false;
		}
		
		
		
		


        return $ret;
    }

	public function __toString(){
		
		$old = parent::__toString();
		$co = array('<dl class="zend_form">','</dl>','<dt ','<dd ','</dt>','</dd>',"regulamin");
		$naco = array('','','<div class="object">'."\n".'<div ','<div ','</div>',"</div>\n</div>",'<a href="'.Zend_Controller_Front::getInstance()->getBaseUrl().'/index/rules">regulamin</a>');
		return str_replace($co,$naco,$old);
	}

	


	
		
}
