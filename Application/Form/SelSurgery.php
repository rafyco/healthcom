<?php

class Application_Form_SelSurgery extends Zend_Form{
	
	private $label_text,$label_submit;
	
	public function __construct($label_text= 'Nazwa gabinetu',$label_submit = 'Dodaj gabinet do listy'){
		$this->label_text = $label_text;
		$this->label_submit = $label_submit;
		parent::__construct();
	}
	
	public function init(){
	
		$name = $this->createElement('text','find')
			->setLabel($this->label_text)
			->setRequired(true)
			->addValidator(new Zend_Validate_StringLength(array( 'min' => 0, 'max' => 60)));
				
		// button potwierdzajacy
		
		$submit_select = $this->createElement('submit', 'submit_select')
			->setLabel($this->label_submit);
		
		
		$this->addElements(array(
			$name,
			$submit_select
		));
	}
	
	public static function getSearche(){
		$form = new Application_Form_SelSurgery('Wprowadź fragment imienia lub nazwiska poszukiwanej osoby: ','Wyszukaj użytkownika');
		$form->setAction(Zend_Controller_Front::getInstance()->getBaseUrl().'/index/search')->setMethod('get');
		return $form;
	}

	public function __toString(){
		
		$old = parent::__toString();
		$co = array('<dl class="zend_form">','</dl>','<dt ','<dd ','</dt>','</dd>',"regulamin");
		$naco = array('','','<div class="object">'."\n".'<div ','<div ','</div>',"</div>\n</div>",'<a href="'.Zend_Controller_Front::getInstance()->getBaseUrl().'/index/rules">regulamin</a>');
		return str_replace($co,$naco,$old);
	}

	

	
		
}
