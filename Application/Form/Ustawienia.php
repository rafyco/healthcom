<?php 
	
class Application_Form_Ustawienia extends Zend_Form{
	
	private $id;
	
	public function __construct($id){
		$this->id = $id;
		parent::__construct();
	}
	
	public function init(){  
		
		$ust = Application_Models_Ustawienia::factory($this->id);
		
		$element[] = $this->createElement('checkbox','wiadommail')
			->setLabel("Potwierdzenie otrzymania wiadomości w mailu: ")
			->setCheckedValue(true)
			->setUncheckedValue(false)
			->setValue($ust->wiadommail);
			
		$element[] = $this->createElement('checkbox','apilog')
			->setLabel("Zezwolenie na korzystanie z API: ")
			->setCheckedValue(true)
			->setUncheckedValue(false)
			->setValue($ust->apilog);

		$element[] = $this->createElement('select','upraw')
			->addMultiOptions(array( 0 => 'Wszyscy znajomi', 2 => 'Tylko mój lekarz'))
			->setLabel("Domyślna widoczność wpisów: ")
			->setValue($ust->upraw);
		
		$element[] = $this->createElement('submit','submit_ustawienia')
			->setLabel("Zapisz ustawienia");
			
		$this->addElements($element);
	} 

	public function __toString(){
		
		$old = parent::__toString();
		$co = array('<dl class="zend_form">','</dl>','<dt ','<dd ','</dt>','</dd>',"regulamin");
		$naco = array('','','<div class="object">'."\n".'<div ','<div ','</div>',"</div>\n</div>",'<a href="'.Zend_Controller_Front::getInstance()->getBaseUrl().'/index/rules">regulamin</a>');
		return str_replace($co,$naco,$old);
	}

	

	
	
}
