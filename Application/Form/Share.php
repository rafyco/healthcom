<?php 
	
class Application_Form_Share extends Zend_Form{

	private $id;
	
	public function __construct($id){
		$this->id = $id;
		parent::__construct();
	}
	
	public function init(){
	
		$email = $this->createElement('text','email');
		$email->setLabel('E-mail');
		$email->setRequired(true);
		$email->addValidators(array(
			new Zend_Validate_EmailAddress(),
			new Zend_Validate_NotEmpty(),
			new Zend_Validate_StringLength(array( 'min' => 0, 'max' => 60))
		));
		$email->addFilters(array(
			new Zend_Filter_StringTrim(),
			new Zend_Filter_StripNewlines(),
			new Zend_Filter_StringToLower()
		));
		
		
		$button = array('submit', 'submit', array('label' => 'Udostępnij konto'));
		
		$this->addElements(array($email,$button));

	}
	
	public function isValid($dane){
		$ret = parent::isValid($dane);
		
		$user = new Application_Models_Users();
		$udst = new Application_Models_Udostepnienia();
		$person = $user->getUsersByEmail($this->email->getValue());
		
		if(isset($person->rola) && $person->rola != 'viewer'){
			$this->email->addError("Nie można udostępniać danych użytkownikowi, który ma swoje konto, a nie jest widzem");
			$ret = false;
		} else if($udst->isUdst($this->id,$user->getId($this->email->getValue()))){
			$this->email->addError("Twoje konto zostało już udostępnione temu użytkownikowi.");
			$ret = false;
		}
		
		return $ret;
		
	}

	public function __toString(){
		
		$old = parent::__toString();
		$co = array('<dl class="zend_form">','</dl>','<dt ','<dd ','</dt>','</dd>');
		$naco = array('','','<div class="object">'."\n".'<div ','<div ','</div>',"</div>\n</div>");
		return str_replace($co,$naco,$old);
	}

	

	


}
