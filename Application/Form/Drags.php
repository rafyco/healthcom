<?php

class Application_Form_Drags extends Zend_Form {
	
	private $id;
	
	public function __construct($id){
		$this->id = $id;
		parent::__construct();
	}
	
	public function init(){
	
		$elements[] = $this->createElement('text','drag_name')
			->setLabel("Nazwa leku:")
			->setRequired(true)
			->addValidators(array(
				new Zend_Validate_NotEmpty(),
			));
			
		$elements[] = $this->createElement('select','rodzaj')
			->setLabel("Rodzaj leku:")
			->addMultiOptions(Application_Models_Rodzaj::toArray());
		
		$elements[] = $this->createElement('hidden','idPac')
			->setValue($this->id);
		
		/*$elements[] = $this->createElement('text','dawka')
			->setLabel("Pojedyńcza dawka:")
			->setRequired(true)
			->addValidators(array(
				new Zend_Validate_NotEmpty(),
				new Zend_Validate_StringLength(array( 'min' => 0, 'max' => 10))
			));*/
		
		$elements[] = $this->createElement('text','dawka')
			->setLabel("Pojedyńcza dawka")
			->setRequired(true)
			->setAttrib("class", "number")
			->setAttrib("max", 20)
			->setAttrib("min",1)
			->addValidators(array(
				new Zend_Validate_Between(array( 'min' => 1, 'max' => 20))
			));	
		$elements[] = $this->createElement('select','dawka2')
			->setLabel("Jednostka dawki:")
			->addMultiOptions(array( 
				"szt." => 'szt.',
				"łyżek" => 'łyżek',
				"tabletka" => 'tabletka',
				"zastrzyk" => 'zastrzyk'
			));	
		
			
		$elements[] = $this->createElement('text','ildawek')
			->setLabel("Ilość dawek:")
			->setRequired(true)
			->setAttrib("class", "number")
			->setAttrib("max", 20)
			->setAttrib("min",1)
			->addValidators(array(
				new Zend_Validate_Between(array( 'min' => 1, 'max' => 20))
			));
			
		$date = new DateTime('+1 day');
		
		$elements[] = $this->createElement('text','first_dat')
			->setLabel("Data pierwszej dawki:")
			->setAttrib("class","date")
			->setRequired(true)
			->setValue($date->format('Y-m-d'));
		$elements[] = $this->createElement('text','first_time')
			->setLabel("Godzina pierwszej dawki:")
			->setAttrib("class","time")
			->setRequired(true)
			->setValue($date->format('G:i'));

		$elements[] = $this->createElement('select','czestosc')
			->setLabel("Częstotliwość przyjmowania:")
			->addMultiOptions(array( 
				1 => 'Co godzinę',
				8 => 'Co 8 godzin',
				12 => 'Co 12 godzin',
				24 => 'Codziennie',
				48 => 'Raz na dwa dni'
			));
		
		
		$elements[] = $this->createElement('submit','submit')
			->setLabel("Dodaj lek");
		
		$this->addElements($elements);
	}
	
	public function isValid($data){
	
		$ret = parent::isValid($data);
		
		if(!preg_match('!^((?:19|20)\d\d)[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$!',$this->first_dat->getValue())){
			$this->first_dat->addError('Niepoprawny format daty');
			$ret = false;
		}
		
		/*if(!preg_match('!^(20|21|22|23|[01]d|d)(([:][0-5]d){1,2})$!',$this->first_time->getValue())){
			$this->first_time->addError('Niepoprawny format czasu');
			$ret = false;
		}*/
		
		return $ret;
	
	}

	public function __toString(){
		
		$old = parent::__toString();
		$co = array('<dl class="zend_form">','</dl>','<dt ','<dd ','</dt>','</dd>',"regulamin");
		$naco = array('','','<div class="object">'."\n".'<div ','<div ','</div>',"</div>\n</div>",'<a href="'.Zend_Controller_Front::getInstance()->getBaseUrl().'/index/rules">regulamin</a>');
		return str_replace($co,$naco,$old);
	}

	

		
}
