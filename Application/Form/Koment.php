<?php

class Application_Form_Koment extends Zend_Form{

	private $idPosta;
	
	public function __construct($idPosta=0){
		$this->idPosta = $idPosta;
		parent::__construct();
	}
	
	
	public function init(){
		
		
		$opis = $this->createElement('textarea','opis')
			->setLabel("Daj komentarz:")
			->setAttrib("rows", "2")
			->setAttrib("placeholder", "Skomentuj")
			->setRequired(true);
		
		$hidden = $this->createElement('hidden','idPosta')
			->setValue($this->idPosta);
		
		$submit_btn = $this->createElement('submit', 'subKom_'.$this->idPosta);
		$submit_btn->setLabel('Wyślij');
		
		
		$this->addElements(array(
			$opis,
			$hidden,
			$submit_btn
		));
	}

	public function __toString(){
		
		$old = parent::__toString();
		$co = array('<dl class="zend_form">','</dl>','<dt ','<dd ','</dt>','</dd>',"regulamin");
		$naco = array('','','<div class="object">'."\n".'<div ','<div ','</div>',"</div>\n</div>",'<a href="'.Zend_Controller_Front::getInstance()->getBaseUrl().'/index/rules">regulamin</a>');
		return str_replace($co,$naco,$old);
	}

}
