<?php 
	
class Application_Form_UstPac extends Zend_Form{
	
	private $id;
	
	public function __construct($id){
		$this->id = (int)$id;
		parent::__construct();
	}
	
	public function init(){  
		
		$pacjenci = new Application_Models_Pacjenci();
		$pac = $pacjenci->getPatient($this->id);
		
		$element[] = $this->createElement('text','maxcisroz')
			->setLabel("Max ciśnienie rozkurczowe: ")
			->setValue($pac->maxcisroz)
			->setRequired(true)
			->setAttrib("class", "number")
			->setAttrib("min", 10)
			->setAttrib("max", 300)
			->addValidators(array(
				new Zend_Validate_Digits(),
				new Zend_Validate_Between(10,300)
			));
			
		$element[] = $this->createElement('text','maxcisskur')
			->setLabel("Max ciśnienie skurczowe: ")
			->setValue($pac->maxcisskur)
			->setRequired(true)
			->setAttrib("class", "number")
			->setAttrib("min", 10)
			->setAttrib("max", 300)
			->addValidators(array(
				new Zend_Validate_Digits(),
				new Zend_Validate_Between(10,300)
			));

		$element[] = $this->createElement('text','maxtent')
			->setLabel("Maksymalna wartość tętna: ")
			->setValue($pac->maxtent)
			->setRequired(true)
			->setAttrib("class", "number")
			->setAttrib("min", 40)
			->setAttrib("max", 200)
			->addValidators(array(
				new Zend_Validate_Digits(),
				new Zend_Validate_Between(40,200)
			));
			
		$element[] = $this->createElement('hidden','id')
			->setValue($pac->id);
		
		$element[] = $this->createElement('submit','submit_ustpac')
			->setLabel("Zapisz wartości");
			
		$this->addElements($element);
	} 

	public function __toString(){
		
		$old = parent::__toString();
		$co = array('<dl class="zend_form">','</dl>','<dt ','<dd ','</dt>','</dd>',"regulamin");
		$naco = array('','','<div class="object">'."\n".'<div ','<div ','</div>',"</div>\n</div>",'<a href="'.Zend_Controller_Front::getInstance()->getBaseUrl().'/index/rules">regulamin</a>');
		return str_replace($co,$naco,$old);
	}

	

	
	
}
