<?php 
	
class Application_Form_Photo extends Zend_Form{
	
	public function init(){  
  
		$element = new Zend_Form_Element_File('nazwapliku', array('required' => true));  
		$element  
		  ->setLabel('Dodaj zdjęcie:')  
		  ->addValidator('NotEmpty', true)  
		  ->addValidator('Count', true, 1)  
		  ->addValidator('Size', true, 307200)  
		  //->addValidator('NotExists','./public/uploads') 
		  ->addValidator('Extension', false, 'jpg,jpeg,png');  
		$this->addElement($element, 'nazwapliku');  
	  
		/*$this->nazwapliku->getValidator('NotExists')->setMessages(array(  
		  Zend_Validate_File_NotExists::DOES_EXIST => "Plik '%value%' już istnieje!",  
		));*/  
		$this->nazwapliku->getValidator('Upload')->setMessages(array(  
		  Zend_Validate_File_Upload::NO_FILE => "Nazwa pliku nie może być pusta!",  
		));  
		$this->nazwapliku->getValidator('Size')->setMessages(array(  
		  Zend_Validate_File_Size::TOO_BIG => "Maksymalny...",  
		)); 
	  
		$this->addElement('submit', 'pht', array(  
		  'label' => 'Zapisz',  
		));  
	} 
	
	public function getAdres(){
		return $_FILES['nazwapliku']['tmp_name'];
	}
	
}
