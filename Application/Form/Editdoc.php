<?php

class Application_Form_Editdoc extends Zend_Form{

	private $dane;
	
	public function getDana($key){
		return $this->dane[$key];
	}

	public function __construct($dane){
		$this->dane = $dane;
		parent::__construct();
	}
	
	public function init(){
		
		$pesel = $this->createElement('text','pesel');
		$pesel->setLabel('PESEL')
			->setValue($this->getDana('pesel'));;
		$pesel->addValidators(array(
			new Zend_Validate_Digits(),
			new Zend_Validate_Pesel()
		));
		
		$pwz_no = $this->createElement('text','pwz_no');
		$pwz_no->setLabel('Numer PWZ')
			->setValue($this->getDana('nrPWZ'));;
		$pwz_no->addValidator(new Zend_Validate_StringLength(array( 'min' => 0, 'max' => 60)));
			
		$this->addElements(array($pesel,$pwz_no));	
			
		// button potwierdzajacy
		
		$submit_btn = $this->createElement('submit', 'sub_edit_doc');
		$submit_btn->setLabel('Edytuj dane');
		
		
		$this->addElement($submit_btn);
	}

	public function __toString(){
		
		$old = parent::__toString();
		$co = array('<dl class="zend_form">','</dl>','<dt ','<dd ','</dt>','</dd>',"regulamin");
		$naco = array('','','<div class="object">'."\n".'<div ','<div ','</div>',"</div>\n</div>",'<a href="'.Zend_Controller_Front::getInstance()->getBaseUrl().'/index/rules">regulamin</a>');
		return str_replace($co,$naco,$old);
	}

	

		
}
