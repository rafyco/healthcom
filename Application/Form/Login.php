<?php 
	
class Application_Form_Login extends Zend_Form{

	public function init(){
	
		$email = $this->createElement('text','email');
		$email->setLabel('E-mail')->setRequired(true)->setAttrib('size',30)->
			addFilters(array(
					new Zend_Filter_StringToLower(),
					new Zend_Filter_StringTrim(),
					new Zend_Filter_StripNewlines(),
					new Zend_Filter_StripTags()
			))->
            addValidators(array(
					new Zend_Validate_EmailAddress(),
					new Zend_Validate_NotEmpty()
			));

		$haslo = $this->createElement('password', 'password');
		$haslo->setLabel('Hasło')
			->setRequired(TRUE)
			->setAttrib('size', 30)
			->addFilters(array(
				new Zend_Filter_StringTrim(),
				new Zend_Filter_StripNewlines(),
				new Zend_Filter_StripTags()
			))
            ->addValidators(array(
                new Zend_Validate_NotEmpty()
			));

		
		$this->addElements(array($email,$haslo,
			array('submit', 'submit', array('label' => 'zaloguj'))
        ));

	}
	
	public function isValid($dane){
		
		$ret = parent::isValid($dane);
		
		$user = new Application_Models_Users();
		
		$odp = $user->fetchRow("email LIKE '".$this->email->getValue()."' AND status=0 AND hash LIKE '".md5(md5($this->password->getValue()))."'");
		
		if(isset($odp->id)){
			$this->password->addError("Podany użytkownik jest zablokowany");
			$ret = false;
		}
		
		return $ret;
	}
	
	public function __toString(){
		
		$old = parent::__toString();
		$co = array('<dl class="zend_form">','</dl>','<dt ','<dd ','</dt>','</dd>');
		$naco = array('','','<div class="object">'."\n".'<div ','<div ','</div>',"</div>\n</div>");
		return str_replace($co,$naco,$old);
	}

	
		
}
