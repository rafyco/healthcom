<?php

class Application_Form_ChHash extends Zend_Form{

	public function init(){
	
		$pass = $this->createElement('password','pass');
		$pass->setLabel('Hasło');
		$pass->setRequired(true);
		$pass->addValidators(array(
			new Zend_Validate_NotEmpty()
		));
		$pass->addFilters(array(
			new Zend_Filter_StringTrim(),
			new Zend_Filter_StripNewlines(),
			new Zend_Filter_StripTags()
		));
		
		$pass_repeat = $this->createElement('password','pass_repeat');
		$pass_repeat->setLabel('Powtórz hasło');
		$pass_repeat->setRequired(true);
		$pass_repeat->addValidators(array(
			new Zend_Validate_NotEmpty()
		));
		$pass_repeat->addFilters(array(
			new Zend_Filter_StringTrim(),
			new Zend_Filter_StripNewlines(),
			new Zend_Filter_StripTags()
		));
		
		
		$submit_btn = $this->createElement('submit', 'submit_btn');
		$submit_btn->setLabel('Zmień hasło');
		
		
		$this->addElements(array(
			$pass,
			$pass_repeat,
			$submit_btn
		));
	}
	
	/**
        Metoda sprawdzająca czy pola są poprawne
    */
    public function isValid($data){

        //Wykonanie w rodzicu
        $ret = parent::isValid($data);

        if ($this->pass->getValue() != $this->pass_repeat->getValue()) {
            $this->pass_repeat->addError("Podane hasła nie zgadzają się.");
            $ret = false;
        }
		
		return $ret;
    }

	public function __toString(){
		
		$old = parent::__toString();
		$co = array('<dl class="zend_form">','</dl>','<dt ','<dd ','</dt>','</dd>',"regulamin");
		$naco = array('','','<div class="object">'."\n".'<div ','<div ','</div>',"</div>\n</div>",'<a href="'.Zend_Controller_Front::getInstance()->getBaseUrl().'/index/rules">regulamin</a>');
		return str_replace($co,$naco,$old);
	}

	

	
}