<?php 
	
class Application_Form_Forgot extends Zend_Form{

	public function init(){
	
	
		$email = $this->createElement('text','email');
		$email->setLabel('E-mail')->setRequired(true)->setAttrib('size',30)->
			addFilters(array(
					new Zend_Filter_StringToLower(),
					new Zend_Filter_StringTrim(),
					new Zend_Filter_StripNewlines(),
					new Zend_Filter_StripTags()
			))->
            addValidators(array(
					new Zend_Validate_EmailAddress(),
					new Zend_Validate_NotEmpty()
			));

		

		
		$this->addElements(array($email,
			array('submit', 'submit', array('label' => 'Przypomnij'))
        ));

	}
	
	public function isValid($data){
	
		//Wykonanie w rodzicu
		$ret = parent::isValid($data);
		
		if (!Application_Models_Users::isUserWork($this->email->getValue())) {
			return false;
		}
		
		return $ret;
		
	}

	public function __toString(){
		
		$old = parent::__toString();
		$co = array('<dl class="zend_form">','</dl>','<dt ','<dd ','</dt>','</dd>',"regulamin");
		$naco = array('','','<div class="object">'."\n".'<div ','<div ','</div>',"</div>\n</div>",'<a href="'.Zend_Controller_Front::getInstance()->getBaseUrl().'/index/rules">regulamin</a>');
		return str_replace($co,$naco,$old);
	}

	


}
