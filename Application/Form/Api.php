<?php 
	
class Application_Form_Api extends Zend_Form{

	
	
	public function init(){
	
		$tresc = $this->createElement('text', 'tresc');
		$tresc->setLabel('Nazwa urządzenia:')->setRequired(true);
		$tresc->addValidators(array(
			new Zend_Validate_NotEmpty(),
			new Zend_Validate_StringLength(array( 'min' => 0, 'max' => 60))
		));
		$elements[] = $tresc;
		

		
		
		$button = $this->button;
		$elements[] = array('submit', 'submit', array('label' => 'Zatwierdź urządzenie'));
		
		$this->addElements($elements);

	}

	public function __toString(){
		
		$old = parent::__toString();
		$co = array('<dl class="zend_form">','</dl>','<dt ','<dd ','</dt>','</dd>',"regulamin");
		$naco = array('','','<div class="object">'."\n".'<div ','<div ','</div>',"</div>\n</div>",'<a href="'.Zend_Controller_Front::getInstance()->getBaseUrl().'/index/rules">regulamin</a>');
		return str_replace($co,$naco,$old);
	}

	

	


}
