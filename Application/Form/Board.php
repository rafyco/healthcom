<?php

class Application_Form_Board extends Zend_Form{
	
	private $bool,$label,$id;
	
	public function __construct($id,$bool = true,$label="Wprowadź dane"){
		$this->bool = $bool;
		$this->label = $label;
		$this->id = $id;
		parent::__construct();
	}
	
	public function init(){
		
		$ust = Application_Models_Ustawienia::factory($this->id);
		
		$cisS = $this->createElement('text','cisS')
			->setLabel('Ciśnienie Skurczowe:')
			->setRequired(true)
			->setAttrib("class", "number")
			->setAttrib("min", 10)
			->setAttrib("max", 300)
			->addValidators(array(
				new Zend_Validate_Digits(),
				new Zend_Validate_Between(10,300)
			));
		
		$cisR = $this->createElement('text','cisR')
			->setLabel('Ciśnienie Rozkurczowe:')
			->setAttrib("class", "number")
			->setAttrib("min", 10)
			->setAttrib("max", 300)
			->setRequired(true)
			->addValidators(array(
				new Zend_Validate_Digits(),
				new Zend_Validate_Between(10,300)
			));
		
		$tetno = $this->createElement('text','tetno')
			->setLabel('Tętno:')
			->setAttrib("class", "number")
			->setAttrib("min", 40)
			->setAttrib("max", 200)
			->addValidators(array(
				new Zend_Validate_Digits(),
				new Zend_Validate_Between(40,200)
			));
		
		$samopoczucie = $this->createElement('text','samopoczucie')
			->setLabel('Samopoczucie:')
			->setAttrib("class", "range")
			->addValidators(array(
				new Zend_Validate_Digits(),
				new Zend_Validate_Between(0,100)
			));
			
		$opis = $this->createElement('textarea','opis')
			->setLabel("Napisz jak się czujesz:")
			->setAttrib("rows", "8");
		
		if(!$this->bool){
			$opis->setAttrib("rows", "3")
				->setAttrib("placeholder", "Jak się czujesz?")
				->setRequired(true);
		
		}
		
		$upr[0] = 'Wszyscy znajomi';
		$upr[2] = 'Tylko mój lekarz';
		
		
		
		$uprawnienia = $this->createElement('select','uprawnienia')
			->setLabel('Kto może zobaczyć ten wpis?')
			->addMultiOptions($upr)
			->setValue($ust->upraw);
		
		
		$submit_btn = $this->createElement('submit', 'submit_btn');
		$submit_btn->setLabel($this->label);
		
		
		if($this->bool){
			$this->addElements(array(
				$cisS,
				$cisR,
				$tetno,
				$samopoczucie,
				$opis,
				$uprawnienia,
				$submit_btn
			));
		} else {
			$this->addElements(array(
				$opis,
				$uprawnienia,
				$submit_btn
				 
			));		
		}
	}
	
	/**
        Metoda sprawdzająca czy pola są poprawne
    */
    public function isValid($data){

        //Wykonanie w rodzicu
        $ret = parent::isValid($data);
		
		if($this->uprawnienia->getValue()<0 || $this->uprawnienia->getValue()>2){
			$ret = false;
			$this->uprawnienia->addError("Nieprawidłowe uprawnienia");
		}
		if($this->bool){
			if($this->cisS->getValue() < $this->cisR->getValue()){
				$ret = false;
				$this->cisR->addError("Ciśnienie rozkurczowe nie może być większe niż skurczowe");
			}
		}
        return $ret;
    }
	
	public function __toString(){
		
		$old = parent::__toString();
		$co = array('<dl class="zend_form">','</dl>','<dt ','<dd ','</dt>','</dd>');
		$naco = array('','','<div class="object">'."\n".'<div ','<div ','</div>',"</div>\n</div>");
		return str_replace($co,$naco,$old);
	}

	


	
		
}
