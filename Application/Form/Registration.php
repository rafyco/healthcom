<?php

class Application_Form_Registration extends Zend_Form{

	public function init(){
	
		$email = $this->createElement('text','email');
		$email->setLabel('E-mail');
		$email->setRequired(true);
		$email->addValidators(array(
			new Zend_Validate_EmailAddress(),
			new Zend_Validate_NotEmpty(),
			new Zend_Validate_StringLength(array( 'min' => 0, 'max' => 60))
		));
		$email->addFilters(array(
			new Zend_Filter_StringTrim(),
			new Zend_Filter_StripNewlines(),
			new Zend_Filter_StringToLower()
		));
		
		$pass = $this->createElement('password','pass');
		$pass->setLabel('Hasło');
		$pass->setRequired(true);
		$pass->addValidators(array(
			new Zend_Validate_NotEmpty()
		));
		$pass->addFilters(array(
			new Zend_Filter_StringTrim(),
			new Zend_Filter_StripNewlines(),
			new Zend_Filter_StripTags()
		));
		
		$pass_repeat = $this->createElement('password','pass_repeat');
		$pass_repeat->setLabel('Powtórz hasło');
		$pass_repeat->setRequired(true);
		$pass_repeat->addValidators(array(
			new Zend_Validate_NotEmpty()
		));
		$pass_repeat->addFilters(array(
			new Zend_Filter_StringTrim(),
			new Zend_Filter_StripNewlines(),
			new Zend_Filter_StripTags()
		));
		
		$first_name = $this->createElement('text','first_name');
		$first_name->setLabel('Imię');
		$first_name->setRequired(true);
		$first_name->addValidator(new Zend_Validate_StringLength(array( 'min' => 0, 'max' => 60)));
		
		$last_name = $this->createElement('text','last_name');
		$last_name->setLabel('Nazwisko');
		$last_name->setRequired(true);
		$last_name->addValidator(new Zend_Validate_StringLength(array( 'min' => 0, 'max' => 60)));
		
		$pesel = $this->createElement('text','pesel');
		$pesel->setLabel('PESEL');
		$pesel->addValidators(array(
			new Zend_Validate_Digits(),
			new Zend_Validate_Pesel()
		));
		
		$accept_rules = $this->createElement('checkbox','accept_rules');
		$accept_rules->setLabel('Przeczytałem(am) i akceptuję regulamin');
		
		$accept_rules->setRequired(true)->setCheckedValue(true)
			->setUncheckedValue(false)->setValue(false);;
		
		// przelacznik rejestracji
		
		$role = $this->createElement('radio','role');
		$role->setLabel('Rejestracja jako');
		$role->addMultiOptions(array('p'=>'Pacjent', 'd'=>'Doktor'));
		// Rafyco:: Domyślne ustawienie radiobuttona na pacjenta
		$role->setValue('p');
		$role->setRequired(true);
		
		// pola dla lekarza
		
		$pwz_no = $this->createElement('text','pwz_no');
		$pwz_no->setLabel('Numer PWZ');
		$pwz_no->addValidator(new Zend_Validate_StringLength(array( 'min' => 0, 'max' => 60)));
		
		// pola dla pacjenta, nieobowiazkowe - wyswietlane po dodatkowym kliknieciu
		
		$blood = $this->createElement('select','blood');
		$blood->setLabel('Grupa krwi');
		$blood->addMultiOptions(Application_Models_Grkrwi::toArray());
		
		$security_no = $this->createElement('text','security_no');
		$security_no->setLabel('Numer ubezpieczenia');
		$security_no->addValidator(new Zend_Validate_StringLength(array( 'min' => 0, 'max' => 60)));
		
		$address_street = $this->createElement('text','address_street');
		$address_street->setLabel('Ulica');
		$address_street->addValidator(new Zend_Validate_StringLength(array( 'min' => 0, 'max' => 45)));
		
		$address_home_no = $this->createElement('text','address_home_no');
		$address_home_no->setLabel('Numer domu/mieszkania');
		
		
		$address_code = $this->createElement('text','address_code');
		$address_code->setLabel('Kod pocztowy');
		$address_code->addValidators(array(
			new Zend_Validate_Digits(),
			new Zend_Validate_StringLength(array( 'min' => 0, 'max' => 60))
		));
		
		$address_code->addFilter(new Krzycholud_Filters_AddressCode());
		
		$address_city = $this->createElement('text','address_city');
		$address_city->setLabel('Miasto');
		$address_city->addValidator(new Zend_Validate_StringLength(array( 'min' => 0, 'max' => 45)));
		
		$phone = $this->createElement('text','phone');
		$phone->setLabel('Numer telefonu');
		$phone->addValidators(array(
			new Zend_Validate_Digits(),
			new Zend_Validate_StringLength(array( 'min' => 0, 'max' => 9)),
		));
		
		// button potwierdzajacy
		
		$submit_btn = $this->createElement('submit', 'submit_btn');
		$submit_btn->setLabel('Zarejestruj się');
		
		
		$this->addElements(array(
			$role,
			$email,
			$pass,
			$pass_repeat,
			$first_name,
			$last_name,
			$pesel,			
			// lekarz
			$pwz_no,
			//pacjent
			$blood,


			$security_no,
			$address_street,
			$address_home_no,
			$address_code,
			$address_city,
			$phone,
			$accept_rules,
			$submit_btn
			 
		));
	}
	
	/**
        Metoda sprawdzająca czy pola są poprawne
    */
    public function isValid($data){

        //Wykonanie w rodzicu
        $ret = parent::isValid($data);

        // Warunek mogący uznać, że się nie formularz jest niepoprawnie wypełniony
        if(Application_Models_Users::isUserExist($this->email->getValue())){
			$this->email->addError("Niestety ten adres mail jest już zarejestrowany.");
			$ret = false;
		}
		
		if ($this->pass->getValue() != $this->pass_repeat->getValue()) {
            $this->pass_repeat->addError("Podane hasła nie zgadzają się.");
            $ret = false;
        }
		
		


        return $ret;
    }

	public function __toString(){
		
		$config = Zend_Registry::getInstance()->get('config');
		
		$old = parent::__toString();
		$co = array('<dl class="zend_form">','</dl>','<dt ','<dd ','</dt>','</dd>',"regulamin");
		$naco = array('','','<div class="object">'."\n".'<div ','<div ','</div>',"</div>\n</div>",
		'<a href="'.Zend_Controller_Front::getInstance()->getBaseUrl().'/index/rules" '." onclick=\"window.open('".Zend_Controller_Front::getInstance()->getBaseUrl().'/index/rules?nolayout'."','Regulamin','width=500,height=300');return false;\" >regulamin</a>");
		return str_replace($co,$naco,$old);
	}

	
		
}
