<?php

class Application_Form_Surgery extends Zend_Form{

	public function init(){
	
		$name = $this->createElement('text','nazwa')
			->setLabel('Nazwa gabinetu')
			->setRequired(true)
			->addValidator(new Zend_Validate_StringLength(array( 'min' => 0, 'max' => 60)));
		
		
				
		$address_street = $this->createElement('text','ulica')
			->setLabel('Ulica')
			->setRequired(true)
			->addValidator(new Zend_Validate_StringLength(array( 'min' => 0, 'max' => 45)));
		
		$address_home_no = $this->createElement('text','numer')
			->setLabel('Numer domu/mieszkania')
			->setRequired(true);
		
		
		$address_code = $this->createElement('text','kodPocz')
			->setLabel('Kod pocztowy')
			->setRequired(true)
			->addValidators(array(
				new Zend_Validate_Digits(),
				new Zend_Validate_StringLength(array( 'min' => 0, 'max' => 60))
			));
			
		$address_code->addFilter(new Krzycholud_Filters_AddressCode());
		
		$address_city = $this->createElement('text','miasto')
			->setRequired(true)
			->setLabel('Miasto')
			->addValidator(new Zend_Validate_StringLength(array( 'min' => 0, 'max' => 45)));
		
		$phone = $this->createElement('text','tel')
			->setLabel('Numer telefonu')
			->addValidators(array(
				new Zend_Validate_Digits(),
				new Zend_Validate_StringLength(array( 'min' => 0, 'max' => 9)),
			));
		
		// button potwierdzajacy
		
		$submit_btn = $this->createElement('submit', 'submit_btn')
			->setLabel('Dodaj gabinet');
		
		
		$this->addElements(array(
			$name,
			$address_street,
			$address_home_no,
			$address_code,
			$address_city,
			$phone,

			$submit_btn
			 
		));
	}

	public function __toString(){
		
		$old = parent::__toString();
		$co = array('<dl class="zend_form">','</dl>','<dt ','<dd ','</dt>','</dd>',"regulamin");
		$naco = array('','','<div class="object">'."\n".'<div ','<div ','</div>',"</div>\n</div>",'<a href="'.Zend_Controller_Front::getInstance()->getBaseUrl().'/index/rules">regulamin</a>');
		return str_replace($co,$naco,$old);
	}

	

	
		
}
