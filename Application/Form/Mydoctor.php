<?php

class Application_Form_Mydoctor extends Zend_Form{

	public function init(){
	
		$email = $this->createElement('text','email');
		$email->setLabel('E-mail');
		$email->setRequired(true);
		$email->addValidators(array(
			new Zend_Validate_EmailAddress(),
			new Zend_Validate_NotEmpty(),
			new Zend_Validate_StringLength(array( 'min' => 0, 'max' => 60))
		));
		$email->addFilters(array(
			new Zend_Filter_StringTrim(),
			new Zend_Filter_StripNewlines(),
			new Zend_Filter_StringToLower()
		));
		
		
		
		// button potwierdzajacy
		
		$submit_btn = $this->createElement('submit', 'submit_mydoctor');
		$submit_btn->setLabel('Wybierz tego lekarza');
		
		
		$this->addElements(array(
			$email,
			$submit_btn
			 
		));
	}
	
	/**
        Metoda sprawdzająca czy pola są poprawne
    */
    public function isValid($data){

        //Wykonanie w rodzicu
        $ret = parent::isValid($data);

        // Warunek mogący uznać, że się nie formularz jest niepoprawnie wypełniony
        if(!Application_Models_Users::isUserExist($this->email->getValue())){
			$this->email->addError("Niestety nie istnieje taki użytkownik");
			$ret = false;
		} else 
		
		if(!Application_Models_Users::isDoctor($this->email->getValue())){
			$this->email->addError("Przykro mi, ale ten użytkownik nie jest lekarzem");
			$ret = false;
		}
		
		
		
		


        return $ret;
    }

	public function __toString(){
		
		$old = parent::__toString();
		$co = array('<dl class="zend_form">','</dl>','<dt ','<dd ','</dt>','</dd>',"regulamin");
		$naco = array('','','<div class="object">'."\n".'<div ','<div ','</div>',"</div>\n</div>",'<a href="'.Zend_Controller_Front::getInstance()->getBaseUrl().'/index/rules">regulamin</a>');
		return str_replace($co,$naco,$old);
	}

	


	
		
}
