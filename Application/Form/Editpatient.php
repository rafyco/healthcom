<?php

class Application_Form_Editpatient extends Zend_Form{

	private $dane;
	
	public function getDana($key){
		return $this->dane[$key];
	}

	public function __construct($dane){
		$this->dane = $dane;
		parent::__construct();
	}
	
	public function init(){
	
		$pesel = $this->createElement('text','pesel');
		$pesel->setLabel('PESEL')
			->setValue($this->getDana('pesel'));
		$pesel->addValidators(array(
			new Zend_Validate_Digits(),
			new Zend_Validate_Pesel()
		));
		
		$blood = $this->createElement('select','blood');
		$blood->setLabel('Grupa krwi')
			->setValue($this->getDana('grupakrwi'));
		$blood->addMultiOptions(Application_Models_Grkrwi::toArray());
		
		$security_no = $this->createElement('text','security_no');
		$security_no->setLabel('Numer ubezpieczenia')
			->setValue($this->getDana('nrubez'));
		$security_no->addValidator(new Zend_Validate_StringLength(array( 'min' => 0, 'max' => 60)));
		
		$address_street = $this->createElement('text','address_street');
		$address_street->setLabel('Ulica')
			->setValue($this->getDana('ulica'));
		$address_street->addValidator(new Zend_Validate_StringLength(array( 'min' => 0, 'max' => 45)));
		
		$address_home_no = $this->createElement('text','address_home_no');
		$address_home_no->setLabel('Numer domu/mieszkania')
			->setValue($this->getDana('nrDomu'));
		$address_home_no->addValidator(new Zend_Validate_Digits());
		
		
		$address_code = $this->createElement('text','address_code');
		$address_code->setLabel('Kod pocztowy')
			->setValue($this->getDana('kodPocztowy'));
		$address_code->addValidators(array(
			new Zend_Validate_Digits(),
			new Zend_Validate_StringLength(array( 'min' => 5, 'max' => 5))
		));
		
		$address_code->addFilter(new Krzycholud_Filters_AddressCode());
		
		$address_city = $this->createElement('text','address_city');
		$address_city->setLabel('Miasto')
			->setValue($this->getDana('miasto'));
		$address_city->addValidator(new Zend_Validate_StringLength(array( 'min' => 0, 'max' => 45)));
		
		$phone = $this->createElement('text','phone');
		$phone->setLabel('Numer telefonu')
			->setValue($this->getDana('tel'));
		$phone->addValidators(array(
			new Zend_Validate_Digits(),
			new Zend_Validate_StringLength(array( 'min' => 0, 'max' => 9)),
		));
			
		$this->addElements(array($pesel,$blood,$security_no,$address_street,$address_home_no,$address_code,$address_city,$phone));	
			
		// button potwierdzajacy
		
		$submit_btn = $this->createElement('submit', 'sub_edit_pat');
		$submit_btn->setLabel('Edytuj dane');
		
		
		$this->addElement($submit_btn);
	}

	public function __toString(){
		
		$old = parent::__toString();
		$co = array('<dl class="zend_form">','</dl>','<dt ','<dd ','</dt>','</dd>',"regulamin");
		$naco = array('','','<div class="object">'."\n".'<div ','<div ','</div>',"</div>\n</div>",'<a href="'.Zend_Controller_Front::getInstance()->getBaseUrl().'/index/rules">regulamin</a>');
		return str_replace($co,$naco,$old);
	}

	

	
}
