<?php 
	
class Application_Form_Info extends Zend_Form{

	private $tresc="Tutaj wpisz swoją poradę";
	private $button;
	private $id=null;
	
	public function __construct($tresc,$button="Wyślij",$id=null){
		$this->button = $button;
		$this->tresc = $tresc;
		$this->id = $id;
		
		parent::__construct();
	}
	
	
	public function init(){
	
		$tresc = $this->createElement('textarea', 'tresc');
		$tresc->setLabel('Tresc:')->setRequired(true)->setValue($this->tresc)->setAttrib('rows', 8);
		$elements[] = $tresc;
		
		if($this->id != null){
			$temp = $this->createElement('hidden','id');
			$temp->setValue($this->id);
			$elements[] = $temp;
		}
		
		
		$button = $this->button;
		$elements[] = array('submit', 'submit', array('label' => $button));
		
		$this->addElements($elements);

	}
	
	public function isValid($data){
	
		//Wykonanie w rodzicu
		$ret = parent::isValid($data);
		
		
		
		
		return $ret;
		
	}

	public function __toString(){
		
		$old = parent::__toString();
		$co = array('<dl class="zend_form">','</dl>','<dt ','<dd ','</dt>','</dd>',"regulamin");
		$naco = array('','','<div class="object">'."\n".'<div ','<div ','</div>',"</div>\n</div>",'<a href="'.Zend_Controller_Front::getInstance()->getBaseUrl().'/index/rules">regulamin</a>');
		return str_replace($co,$naco,$old);
	}

	


}
