<?php
class SurgeryController extends Application_Controllers_DefaultController {

	public function init(){
		
		$auth = Zend_Auth::getInstance();
		if($auth->hasIdentity()){
			$this->view->identity = $auth->getIdentity();
		}
		
		if($this->view->identity == null){
			return $this->login();
		}
		
		
	}
   
	public function indexAction() { // wyświetlenie listy gabinetów
	  
		$this->view->title = "Gabinety";
		if($this->view->identity->rola != 'doctor'&& $this->view->identity->rola != 'admin'){
			return $this->_redirect('/error/unenter');
		}
		Zend_Loader::loadClass('Application_Models_Gabinety');
		
		$gabs = new Application_Models_Gabinety();
		if($this->view->identity->rola == 'admin'){
			$gab = $gabs->fetchAll();
		} else {
			$lekgab = new Application_Models_Lekarzgabinet();
			$gab = Application_Models_Lekarzgabinet::getByDoctor($this->view->identity->id);
		}
		
		$paginator = Zend_Paginator::factory($gab);
		$paginator->setItemCountPerPage(7);
		$page=$this->_getParam('page',1);
		$paginator->setCurrentPageNumber($page);
		
		
		
		
		$i=0;
		$dane = null;
		foreach($paginator as $temp){
		
			$dane[$i]['nazwa'] = $temp->nazwa;
			$dane[$i]['adres'] = $temp->ulica." ".$temp->numer."<br />";
			$dane[$i]['adres'] .= substr($temp->kodPocz,0,2)."-".substr($temp->kodPocz,2,3)." ".$temp->miasto;
			$dane[$i]['id'] = $temp->id;
			
			if($temp->telefon != 0){
				$dane[$i]['telefon'] = Rafyco_Template::telefon($temp->telefon);
			} else {
				$dane[$i]['telefon'] = "";
			}
			$i++;
		}
		
		if($this->view->identity->rola == 'admin'){
		
			$form = new Application_Form_Surgery();
			$form->setAction(Zend_Controller_Front::getInstance()->getBaseUrl().'/surgery')->setMethod('post');
			
			if($this->_request->isPost() && $form->isValid($_POST)){
				$gabinety = new Application_Models_Gabinety();
				
				$tempg['nazwa'] = $form->nazwa->getValue();
				$tempg['ulica'] = $form->ulica->getValue();
				$tempg['numer'] = $form->numer->getValue();
				$tempg['kodPocz'] = $form->kodPocz->getValue();
				$tempg['miasto'] = $form->miasto->getValue();
				if($form->tel->getValue() != '')
					$tempg['telefon'] = $form->tel->getValue();
				
				$gabinety->insert($tempg);
				Rafyco_Logi::getInstance()->addText("Dodałeś nowy gabinet do bazy danych");
				
				return $this->_redirect('/surgery');
			}
				
			$this->view->form = $form;
			
		} else if($this->view->identity->rola == 'doctor'){ 
			// Wersja doktora
			$form = new Application_Form_SelSurgery();
			$form->setAction(Zend_Controller_Front::getInstance()->getBaseUrl().'/surgery/search')->setMethod('get');
			
							
			$this->view->form = $form;
		
		
		
		} else {
			$this->view->form = null;
		}
		$this->view->dane = $dane;
		$this->view->paginator = $paginator;
		
	}


	public function delAction() {	// Usunięcie gabinetu
		$this->view->title = "Usuń gabinet";
		$this->form_setView(
			"/surgery/del",
			"Czy na pewno usunąć ten gabinet?",
			"Gabinet o takim identyfikatorze nie istnieje");
		
			$this->view->id = (int) $this->_request->getParam('id');
		
		if(!($this->view->identity->rola == 'admin' || $this->view->identity->rola == 'doctor')){
			Rafyco_Logi::getInstance()->addText("Nie możesz skasować tego gabinetu");
			return $this->form_redirect('/surgery');
		}
		$this->form_setDane(null);
		$gabinety = new Application_Models_Gabinety();
		
		if($this->_request->isPost()){
			$filter = new Zend_Filter_Alpha();
			$del = $filter->filter($this->_request->getPost('del'));
			
			if($del == 'tak' && ($this->view->identity->rola == 'admin')){
				$row = $gabinety->delete("id=".$this->view->id);
				Rafyco_Logi::getInstance()->addText("Usunięto gabinet");
			}
			
			if($del == 'tak' && $this->view->identity->rola == 'doctor'){
				$gablek = new Application_Models_Lekarzgabinet();
				$gl = $gablek->fetchRow("idLekarza=".$this->view->identity->id." AND idGabinet=".$this->view->id);
				if(isset($gl)){
					$gablek->delete("idLekarza=".$this->view->identity->id." AND idGabinet=".$this->view->id);
					Rafyco_Logi::getInstance()->addText("Usunięto powiązanie z gabinetem");
				} else {
					Rafyco_Logi::getInstance()->addText("Nie możesz usunąć tego gabinetu");
				}
			}
			
			
		} else {
			
			$por = $gabinety->fetchRow("id=".$this->view->id);
			
						
			if($this->view->identity->rola == 'admin' || $this->view->identity->rola == 'doctor'){
				$this->form_setDane($por);
				return $this->form_yesno();
			} 
			
		}
		$this->form_redirect('/surgery');
		
		
		
		
		
	}	
	
	public function searchAction(){

		if($this->view->identity->rola != 'doctor'){
			return $this->_redirect('/surgery');
		}
		
		$this->view->title = "Wyszukiwarka gabinetów";
		
		$gab = new Application_Models_Gabinety();
		$form = new Application_Form_SelSurgery('Czego szukać: ','Wyszukaj gabinetu');
		$form->setAction(Zend_Controller_Front::getInstance()->getBaseUrl().'/surgery/search')->setMethod('get');
		
		$find = $this->_request->getParam('find');
		
		$wyniki = $gab->search($find);
		
		$paginator = Zend_Paginator::factory($wyniki);
		$paginator->setItemCountPerPage(15);
		$page = $this->_getParam('page',1);
		$paginator->setCurrentPageNumber($page);
		
		$this->view->form = $form;
		$this->view->dane = $paginator;
		
	
	}
	
	public function addAction(){
		$this->view->title = "Dodaj gabinet";
		$this->form_setView(
			"/surgery/add",
			"Czy na pewno dodać ten gabinet?",
			"Gabinet o takim identyfikatorze nie istnieje");
		
		$this->view->id = (int) $this->_request->getParam('id');
		
		if(!($this->view->identity->rola == 'doctor')){
			Rafyco_Logi::getInstance()->addText("Nie możesz dodać tego gabinetu");
			return $this->form_redirect('/surgery');
		}
		$this->form_setDane(null);
		$gabinety = new Application_Models_Gabinety();
		
		if($this->_request->isPost()){
			$filter = new Zend_Filter_Alpha();
			$del = $filter->filter($this->_request->getPost('del'));
						
			if($del == 'tak' && $this->view->identity->rola == 'doctor'){
				$gablek = new Application_Models_Lekarzgabinet();
				Application_Models_Lekarzgabinet::connect($this->view->identity->id,$this->view->id);
				Rafyco_Logi::getInstance()->addText("Dodano nowy gabinet do Twojego konta");
			}
			
			
		} else {
			
			$por = $gabinety->fetchRow("id=".$this->view->id);
			
						
			if($this->view->identity->rola == 'doctor'){
				$this->form_setDane($por);
				return $this->form_yesno();
			} 
			
		}
		
		$this->form_redirect('/surgery');
		
		
		
		
		
	
	}

   
}