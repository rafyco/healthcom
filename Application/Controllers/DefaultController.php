<?php
class Application_Controllers_DefaultController extends Zend_Controller_Action {

	static $end;
	private $api;

	protected function setView($controll,$action="index"){
		if($controll == null){
			$this->_helper->viewRenderer->setNoRender();
		} else {
			$this->_helper->viewRenderer->renderBySpec('question', array('module' => 'default', 'controller' => 'index'));		
		}
		$this->json['odp'] = 'ERROR';
		
	}
	
	protected function form_yesno(){
		$this->_helper->viewRenderer->renderBySpec('question', array('module' => 'default', 'controller' => 'index'));
		
	}
	
	protected function form_setDane($dane){
		$this->view->gab = $dane;
	}
	
	protected function form_setView($adres,$pytanie,$ifnot,$end =" "){
		$this->json['odp'] = 'ERROR';
		$this->view->formadres = $this->view->baseUrl().$adres;
		$this->view->formpytanie = $pytanie;
		$this->view->formifnot = $ifnot;
		self::$end = $end;
		
	}
	
	protected function form_redirect($adres){
		if(isset($_POST['display']) || $this->api){
			if($_POST['display'] == 'json' || $this->api){
				$this->view->odp = $this->json;
				$this->_helper->viewRenderer->renderBySpec('index', array('module' => 'default', 'controller' => 'api'));
				return;
			}	
		
		}
		return $this->_redirect($adres);
	}
	
	protected function form_setMiddle($cos){
		$this->view->formpytanie = $this->view->formpytanie.$cos.self::$end;
	}
	
	protected function form_ok(){
		$this->json['odp'] = 'OK';
	}
	protected function login(){
		$url = $_SERVER['REQUEST_URI'];
		$url = str_replace($this->view->baseUrl(),"",$url);
		$url = urlencode($url);
		return $this->_redirect("/index/login?go=$url");
	}
	
	/**
		Wczytuje plik javascript na pocz�tku strony.
		$bool - okre�la czy u�ywa� na pocz�tku adresu serwisu
	*/
	protected function loadJavaScript($adres,$bool=true){
		if($bool){
			$adres = $this->view->baseUrl().$adres;
		}
		$this->view->script .= '<script language="javascript" type="text/javascript" src="'.$adres."\"></script>\n";	
	}
	
	protected function loadCss($adres,$bool=true){
		if($bool){
			$adres = $this->view->baseUrl().$adres;
		}
		$this->view->script .= '<link rel="stylesheet" type="text/css" media="screen"
      href="'.$adres."\" />\n";	
	}

}

