<?php
class TestsController extends Application_Controllers_DefaultController {

	public function init(){
		
		$auth = Zend_Auth::getInstance();
		if($auth->hasIdentity()){
			$this->view->identity = $auth->getIdentity();
		}
		
		if($this->view->identity == null){
			return $this->login();
		}
		
		if($this->view->identity->rola != 'admin'){
			return $this->_redirect('/error/unenter');
		}
		
		
	}
   
	public function makewpisAction(){ // Wpisanie 1000 losowych wpisów.
		$nowychwpisow = 1000;
		
		$fp = fopen("./public/test/text.txt", "r");
		
		$dzienniczek = new Application_Models_Dzienniczek();
		
		$users = new Application_Models_Users();
		
		$us = $users->fetchAll();
		
		foreach($us as $u){
			if($u->rola != 'viewer')
				$tab[] = $u->id;
		}
		
		
		for($i=0;$i<$nowychwpisow;$i++){
			$dzien = $dzienniczek->fetchNew();
			
			$tekst = fgets($fp, 1024);
			if($tekst != ""){
				$dzien['idPacjenta'] = array_rand($tab);
					
				$dzien['cisskurcz'] = rand(51,200);
				$dzien['cisrozkurcz'] = rand(50,$dzien['cisskurcz']);
				$dzien['samopoczucie'] = rand(0,100);
				$dzien['data'] = date('Y-m-d H:i:s',rand(946686569,1385857769));
				$dzien['tetno'] = rand(50,120);
				$dzien['tresc'] = $tekst;
				$dzien['uprawnienia'] = array_rand(array(0,2));
				
				$dzien->save();
			}
		}
		
		
		
		fclose($fp);
		
		
		Rafyco_Logi::getInstance()->addText($tekst);
		
		
	
		return $this->_redirect('/admin');
	}



   
}