<?php
class FriendController extends Application_Controllers_DefaultController {

	public function init(){
		
		$auth = Zend_Auth::getInstance();
		if($auth->hasIdentity()){
			$this->view->identity = $auth->getIdentity();
		}
		
		if($this->view->identity == null){
			return $this->login();
		}
		
		if($this->view->identity->rola == 'viewer'){
			return $this->_redirect('/index');
		}
		
			
	}
   
	public function indexAction() { // wyświetlenie wiadomości
	  	$this->view->title = "Lista znajomych";
		$friends = new Application_Models_Przyjaznie();
		$list = $friends->getAll($this->view->identity->id);
		
		$paginator = Zend_Paginator::factory($list);
		$paginator->setItemCountPerPage(20);
		$page=$this->_getParam('page',1);
		$paginator->setCurrentPageNumber($page);
		
		$this->view->paginator = $paginator;
		
	}

	public function addAction() {
		$this->view->title = "Dodanie przyjaciela"; 
		$this->form_setView(
			"/friend/add",
			"Czy chesz aby ",
			"Nie można dodać przyjaciela",
			" był Twoim przyjacielem w serwisie?");
		$this->view->id = (int) $this->_request->getParam('id');
		$this->form_setDane(array( 'id' => $this->view->id));
		
		$friend = new Application_Models_Przyjaznie();
		
		if($this->_request->isPost()){
			$filter = new Zend_Filter_Alpha();
			$del = $filter->filter($this->_request->getPost('del'));
			
			if($del == 'tak'){
				
				$friend->add($this->view->identity->id,$this->view->id);
				switch(Application_Models_Przyjaznie::getDane($this->view->identity->id,$this->view->id)){
					case 1:
						
						$view = new Zend_View();
						$view->setScriptPath('./Application/views/scripts/mail/');
						$view->podpis = $this->view->idenitity->imie." ".$this->view->idenitity->nazwisko;
						$view->adres = Zend_Controller_Front::getInstance()->getBaseUrl()."/index/show/id/".$this->view->identity->id;
						$view->akcept = Zend_Controller_Front::getInstance()->getBaseUrl()."/friend/add/id/".$this->view->identity->id;
						
						
						Application_Models_Wiadomosci::newSysMessage(
							$this->view->identity->id,
							$this->view->id,
							"Zostań moim przyjacielem",
							$view->render('friend.phtml')
						);
					
						Rafyco_Logi::getInstance()->addText("Wysłano propozycję przyjaźni dla ".Application_Models_Users::staticAlias($this->view->id));
						break;
					case 3: 
						Rafyco_Logi::getInstance()->addText("Ty i ".Application_Models_Users::staticAlias($this->view->id)." jesteście przyjaciółmi");
						break;
						
				}
				$this->form_ok();
			}
			
			
						
		} else {
			
			if(Application_Models_Users::isUserExistId($this->view->id)){
				$this->form_setMiddle(Application_Models_Users::staticAlias($this->view->id));
				$this->form_yesno();
				return;
			} 
			
		}
		
		return $this->form_redirect('/friend');
	}

	public function delAction() {
		$this->view->title = "usunięcie przyjaciela";
		$this->form_setView(
			"/friend/del",
			"Czy na pewno usunąć ",
			"Nie można usunąć przyjaciela",
			" z listy przjaciół?"); 
		$this->view->id = (int) $this->_request->getParam('id');
		$this->form_setDane(array( 'id' => $this->view->id));

		$friend = new Application_Models_Przyjaznie();
		
		if(!$friend->isConnect($this->view->identity->id,$this->view->id)){
			Rafyco_Logi::getInstance()->addText("Przyjaźń którą chcesz usunąć nie istnieje");
			return $this->_redirect('/friend');
		}
				
		if($this->_request->isPost()){
			$filter = new Zend_Filter_Alpha();
			$del = $filter->filter($this->_request->getPost('del'));
			
			if($del == 'tak'){
				$friend->del($this->view->identity->id,$this->view->id);
				$this->form_ok();
				Rafyco_Logi::getInstance()->addText("Usunięto więzy przyjaźni");
			}
			
			
		} else {
			
			if(Application_Models_Users::isUserExistId($this->view->id)){
				$this->form_setMiddle(Application_Models_Users::staticAlias($this->view->id));
				$this->form_yesno();
				return;
			} 
			
		}
		
		$this->form_redirect('/friend');
	}

   
}