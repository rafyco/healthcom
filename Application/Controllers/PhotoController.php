<?php
class PhotoController extends Application_Controllers_DefaultController {

	public function init(){
		
		
	}
	
	private function setPhoto($odp){
		header("Content-type: image/jpeg");
		$odp->display();
		$this->_helper->viewRenderer->renderBySpec('photo', array('module' => 'default', 'controller' => 'api'));
	}
   
	public function indexAction() { 
	
		$photo = new Rafyco_Photo('./public/images/none.jpg');	
		
		$id = (int)$this->_request->getParam('id');
		
		if(Application_Models_Users::isUserExistId($id)){
			$photo->open('./public/images/unknown.jpg');
			try{
				$photo->open('./database/photo/'.$id.'.jpg');
			} catch(Exception $ex){}
		} else {
			$photo->ver();
		}
		
		$photo->resize(300,300);
		$this->setPhoto($photo->display());
	}
	
	public function miniAction(){
	
		$photo = new Rafyco_Photo('./public/images/none.jpg');	
		
		
		$id = (int)$this->_request->getParam('id');
		$exist = Application_Models_Users::isUserExistId($id);
		
		if($exist && is_file('./database/photo/'.$id.'_mini.jpg')){
			$photo->open('./database/photo/'.$id.'_mini.jpg');
			$photo->save('./database/photo/'.$id.'_mini.jpg');
			$this->setPhoto($photo);
			return;
		} else if($exist){
			$photo->open('./public/images/unknown.jpg');
			try{
				$photo->open('./database/photo/'.$id.'.jpg');
			} catch(Exception $ex){ }
		} else {
			$photo->ver();
		}
		
		
		$photo->resize(75,75);
		
		$this->setPhoto($photo->display());
	}
	
	public function mobileAction(){
	
		$photo = new Rafyco_Photo('./public/images/none.jpg');	
		
		
		$id = (int)$this->_request->getParam('id');
		$exist = Application_Models_Users::isUserExistId($id);
		
		if($exist && is_file('./database/photo/'.$id.'_mini.jpg')){
			$photo->open('./database/photo/'.$id.'_mini.jpg');
			$photo->save('./database/photo/'.$id.'_mini.jpg');
			$this->setPhoto($photo->display());
			return;
		} else if($exist){
			$photo->open('./public/images/unknown.jpg');
			try{
				$photo->open('./database/photo/'.$id.'.jpg');
			} catch(Exception $ex){ }
		} else {
			$photo->ver();
		}
		
		
		$photo->resize(100,100);
		
		
		$this->setPhoto($photo->display());
	}
	
   
}