<?php
class IndexController extends Application_Controllers_DefaultController {

	public function init(){
		
		$auth = Zend_Auth::getInstance();
		if($auth->hasIdentity()){
			$this->view->identity = $auth->getIdentity();
		}
		
	}
   
	public function indexAction() { // wyświetlenie wiadomości
	  
		$this->view->title = "Aktualności";
		if($this->view->identity == null){
			return $this->_redirect("/index/login");
		}
		
		
		$form = new Application_Form_Board($this->view->identity->id,false,'Wyślij');
		$form->setAction(Zend_Controller_Front::getInstance()->getBaseUrl().'/')->setMethod('post');
		$form2 = null;
		$dziennik = new Application_Models_Dzienniczek();
		$koment = new Application_Models_Komentarze();
		
		$por = $dziennik->getAktual($this->view->identity);
		$paginator = Zend_Paginator::factory($por);
		$paginator->setItemCountPerPage(15);
		$page=$this->_getParam('page',1);
		$paginator->setCurrentPageNumber($page);
				 
		$this->loadJavaScript('/script/index/page/'.(int)$page);
		foreach($paginator as $pag){
			
			$form2[$pag->idDzienniczka] = new Application_Form_Koment($pag->idDzienniczka);
			if($this->_request->isPost() && isset($_POST['subKom_'.$pag->idDzienniczka]) && $form2[$pag->idDzienniczka]->isValid($_POST)){
				
				
				$dane['idPosta']=$form2[$pag->idDzienniczka]->idPosta->getValue();
				$dane['idAutora'] = $this->view->identity->id;
				$dane['tresc']=$form2[$pag->idDzienniczka]->opis->getValue();
				$koment->insert($dane);
				
				Rafyco_Logi::getInstance()->addText("Dziękujemy za komentarz do wpisu: ".$this->view->escape($dane['tresc']));
				return $this->_redirect('/');
			}
			
		}
		$this->view->form2 = $form2;
		
		if($this->_request->isPost()){
		
			$form2 = new Application_Form_Koment();
			
			
			
			
			if(isset($_POST['submit_btn']) && $form->isValid($_POST)){
			
				$dane['idPacjenta'] = $this->view->identity->id;
				$dane['cisskurcz'] = 0;
				$dane['cisrozkurcz'] = 0;
				$dane['samopoczucie'] = 0;
				$dane['tresc'] = $form->opis->getValue();
				$dane['uprawnienia'] = $form->uprawnienia->getValue();
				$dziennik->insert($dane);
				Rafyco_Logi::getInstance()->addText("Dziękujemy za opinie");
				return $this->_redirect('/');
			}	
		}
		
		
		
		
		$this->view->form = $form;
		$this->view->dane = $paginator;
		
		
	}

	public function rulesAction() {
		$this->view->title = "Regulamin";
	}
	
	public function registryAction() {
		$this->view->title = "Rejestracja";
		if($this->view->identity!=null){
			return $this->_redirect('/');
		}		
		
		
		$form = new Application_Form_Registration();
		$form->setAction(Zend_Controller_Front::getInstance()->getBaseUrl().'/index/registry')->setMethod('post');
		$this->view->form = $form;
		
		$authorize = new Application_Models_Authorize();
		$users = new Application_Models_Users();
		
		if($this->_request->isPost()){
			// Jeśli ktoś wypełnił formularz
			if($this->view->form->isValid($_POST)){
				//Jeśli jest wszystko dobrze wypełnione
				$data = $this->view->form->getValues();
				/*
					1. Usunięcie złych wpisów
					2. Wypełnienie tabelek
					3. Losowanie kodu
					4. Ustawienie kodu w bazie
					5. Wysłanie maila
				
				*/
				
				// 1.
				
					$users->clear();
				
				// 2. 
					
					$table['email'] = $data['email'];
					$table['imie'] = $data['first_name'];
					$table['nazwisko'] = $data['last_name'];
					$table['hash'] = md5(md5($data['pass']));
					if($data['role']=='p'){
						$table['rola'] = 'patient';
					} else if ($data['role'] == 'd'){
						$table['rola'] = 'doctor';
					} else {
						$table['rola'] = 'error';
					}
					
					$users->insert($table);
					$id = $users->getUsersByEmail($table['email'])->id;
					
					if($table['rola'] == 'doctor'){
						$table2['id'] = $id;
						$table2['pesel'] = $data['pesel'];
						$table2['nrPWZ'] = $data['pwz_no'];
						
						$lekarze = new Application_Models_Lekarze();
						$lekarze->insert($table2);
					} else {
						$table2['id'] = $id;
						$table2['pesel'] = $data['pesel'];
						$table2['grupakrwi'] = $data['blood'];
						$table2['nrubez'] = $data['security_no'];
						$table2['tel'] = $data['phone'];
						$table2['ulica'] = $data['address_street'];
						$table2['nrDomu'] = $data['address_home_no'];
						$table2['kodPocztowy'] = $data['address_code'];	
						$table2['miasto'] = $data['address_city'];	
						
						$pacjenci = new Application_Models_Pacjenci();
						$pacjenci->insert($table2);						
					}
					
					
				// 3. 4.
					if($table['rola']=='patient')
						$kod = Application_Models_Authorize::setKod($id);
				
				
				// 5.
					$config = Zend_Registry::getInstance()->get('config');
				
					$email = new Zend_View();
					$email->setScriptPath('./Application/views/scripts/mail/');
					$email->imie = $data['firs_name'];
					if($table['rola']=='patient'){
						$email->adres = "http://".$config->my->config->host.$this->view->baseUrl()."/index/registry/kod/$kod";
						
						$mail = new Zend_Mail('UTF-8');
						$mail->setSubject('Rejestracja w serwisie HealthCom')
							->addTo($data['email'])
							->setBodyHtml($email->render('authorize.phtml'))
							->send();
					} else {
						$mail = new Zend_Mail('UTF-8');
						$mail->setSubject('Rejestracja w serwisie HealthCom')
							->addTo($data['email'])
							->setBodyHtml($email->render('authorizeDoc.phtml'))
							->send();
					}
				// 6. 
					Rafyco_Logi::getInstance()->addText("Rejestracja przeprowadzona poprawnie. Sprawdź swojego maila");
					return $this->_redirect('/');				
			} else {
				//Jeśli nie wypełnione dobrze
				return;
			}
		} else {
			//Jeśli nie wypełnialiśmy nic
			$kod = $this->_request->getParam('kod');
			
			if($kod == null){ // Nie podaliśmy kodu
			
				// Wyświetlanie formularza
				return;
				
			} else if($authorize->isKodExist($kod)){
				// kod istnieje
				$odp = $authorize->getUserByKod($kod);
				$users->athorize($odp->id);
				$authorize->clearKod($kod);
				
				Rafyco_Logi::getInstance()->addText("Twoje konto zostało potwierdzone");
				return $this->login();
				
				
			} else {
				//kod nie istnieje
				Rafyco_Logi::getInstance()->emerg("Użyto nieautoryzowanego kodu przypomnienia: ".$kod);
				Rafyco_Logi::getInstance()->addText("Nie możesz autoryzować używając podanego kodu: ".$kod);
				return $this->login();
			}
		}	
		
		
	}

	public function loginAction() {
		if($this->view->identity != null){
			return $this->_redirect('/');
		}
		
		$this->view->title = "Logowanie";
		
		
		$form = new Application_Form_Login();
		if(isset($_GET['go'])){
			$adres = Zend_Controller_Front::getInstance()->getBaseUrl().'/index/login?go='.urlencode($_GET['go']);
		} else {
			$adres = Zend_Controller_Front::getInstance()->getBaseUrl().'/index/login';
		}
		$form->setAction($adres)->setMethod('post');
		
		if($this->_request->isPost() && $form->isValid($_POST)){
			// Jeśli się dobrze zalogowano sprawdzamy dane
			//Pobieramy dane z formularza
			$data = $form->getValues();
			// Adapter bazy danych
			$db = Zend_Db_Table::getDefaultAdapter();
			// Tworzenie instancji adaptera
			$authAdapter = new Zend_Auth_Adapter_DbTable($db, 'users' , 'email', 'hash');
			//Wprowadzamy dane do adaptera
			$authAdapter->setIdentity($data['email']);
			$authAdapter->setCredential(md5(md5($data['password'])));
			
			//Sprawdzamy czy użytkownik aktywny
			$authAdapter->setCredentialTreatment("? AND status ='1'");
			//Autoryzacja
			$result = $authAdapter->authenticate();
			
			if($result->isValid()){
				//umieszczamy w sesji dane użytkownika
				$auth = Zend_Auth::getInstance();
				$storage  = $auth->getStorage();
				$storage->write($authAdapter->getResultRowObject(array(
					'id','email','imie','nazwisko','rola')));
				Rafyco_Logi::getInstance()->log('Zalogowano jako: '.$data['email']);
				if(isset($_GET['go'])) $url = $_GET['go']; else $url = "/";
				return $this->_redirect($url);	
		
			} else {
				$this->view->loginMessage = "Niepoprawny login lub hasło";
			}
		} 
		
		$this->view->form = $form;
	}
	
	public function logoutAction(){
		$auth = Zend_Auth::getInstance();
		$auth->clearIdentity();
		return $this->_redirect('/');
	}

	public function showAction() {
		
		if($this->view->identity == null){
			return $this->login();
		}
		
		// --------- Sprawdzenie czy istnieje użytkownik ------------
		
		
		$id = (int)$this->_request->getParam('id');
		$us = new Application_Models_Users();
		
		if(Application_Models_Users::isUserExistId($id)){
			
			$user = $us->fetchRow('id='.$id);
			
		} else if(Application_Models_Users::isUserExist($this->_request->getParam('email'))){
			$user = $us->fetchRow("email LIKE '".$this->_request->getParam('email')."'");
			$id = $us->getId($this->_request->getParam('email'));
		} else if ($id==0){
			$user = $this->view->identity;
		} else {
			return $this->_redirect('/error/unenter');
		}
		
		
		$this->view->title = $user->imie." ".$user->nazwisko;	
		// ----------------------------------------------------------
		$dane['imie'] = $user->imie;
		$dane['nazwisko'] = $user->nazwisko;
		
		
		if($user->rola == 'patient'){
				
			if($user->id == $this->view->identity->id || Application_Models_Przyjaznie::isFriendStatic($this->view->identity->id,$id) || Application_Models_Pacjenci::isMyDoctor($user->id,$this->view->identity->id)){
				$dane['email'] = $user->email;
				$patient = new Application_Models_Pacjenci();
				$odp = $patient->fetchRow("id=".$user->id);
				
				if($odp->idDoc != 0){
					$doctor = $us->getUserById($odp->idDoc);
					if($odp->isDoc == 1){
						$dane['lekarz prowadzacy'] = $doctor->imie." ".$doctor->nazwisko;
					} else {
						if($odp->id == $this->view->identity->id){
							$doctor = $us->getUserById($odp->idDoc);
							$dane['lekarz prowadzacy'] = $doctor->imie." ".$doctor->nazwisko." (niepotwierdzony)";
						}
					}
				
				}
				
				if($odp->pesel!="") $dane['pesel'] = $odp->pesel;
				$dane['grupa_krwi'] = Application_Models_Pacjenci::getGrKrwi($odp->grupakrwi);
				$dane['adres'] = Rafyco_Template::adres($odp->ulica,$odp->nrDomu,$odp->kodPocztowy,$odp->miasto);
				if($odp->nrubez!=0) $dane['numer ubezpieczenia'] = $odp->nrubez;
				if($odp->tel!="") $dane['tel'] = Rafyco_Template::telefon($odp->tel);
			}
		
		} else if($user->rola == 'doctor'){
			
			$dane['email'] = $user->email;
			$doctor = new Application_Models_Lekarze();
			$odp = $doctor->fetchRow("id=".$user->id);
			
			if($odp->pesel!="") $dane['pesel'] = $odp->pesel;
			if($odp->nrPWZ!="") $dane['numer PWZ'] = $odp->nrPWZ;
			
			
			
			$gabinety = Application_Models_Lekarzgabinet::getByDoctor($user->id);
			
			if($gabinety!=null){
				$i=0;
				foreach($gabinety as $gabtemp){
					$gab[$i]['nazwa'] = $gabtemp->nazwa;
					$gab[$i]['adres'] = Rafyco_Template::adres($gabtemp->ulica,$gabtemp->numer,$gabtemp->kodPocz,$gabtemp->miasto);
					$gab[$i]['telefon'] = Rafyco_Template::telefon($gabtemp->telefon);
					$i++;
				}
			} else {
				$gab=null;
			}
			
			@$this->view->gab = $gab;
		} else {
			if(Application_Models_Przyjaznie::isFriendStatic($this->view->identity->id,$id)){
				$dane['email'] = $user->email;			
			}
		}
		
		if($user->rola == 'patient' && Application_Models_Pacjenci::isMyDoctor($user->id,$this->view->identity->id)){
		
			$form = new Application_Form_UstPac($user->id);
		
			if($this->_request->isPost() && $form->isValid($_POST)){
				$pacjenci = new Application_Models_Pacjenci();
				$pac = $pacjenci->getPatient($user->id);
				
				$pac->maxcisroz = $form->maxcisroz->getValue();
				$pac->maxcisskur = $form->maxcisskur->getValue();
				$pac->maxtent = $form->maxtent->getValue();
				$pac->save();
				Rafyco_Logi::getInstance()->addText("Ustawienia zostały zatwierdzone.");
				return $this->_redirect('/index/show/id/'.(int)$form->id->getValue()."#ustpac");
			
			}		
			$this->view->form = $form;
		}	
		
		$this->view->user = $user;
		$this->view->dane = $dane;
		
	}
   
	public function deactiveAction() {
		$this->view->title = "Dezaktywacja konta";
		
	
		$this->form_setView(
			"/index/deactive",
			"Czy na pewno chcesz usunąć swoje konto?",
			"Nie można usunąć konta");
		
		if($this->view->identity == null){
			return $this->login();
		} else if($this->view->identity == 'admin'){
			Rafyco_Logi::getInstance()->addText("Nie można usunąć konta administratora.");
			return $this->_redirect('/index/');
		}
		$dane = $this->view->identity;
		$this->form_setDane($dane);
		$users = new Application_Models_Users();
		$this->form_yesno();
		if($this->_request->isPost()){
		
			$filter = new Zend_Filter_Alpha();
			$del = $filter->filter($this->_request->getPost('del'));
			if($del == 'tak'){
				// Jeśli zgodziłem się usunąć swoje konto
				// 1. Deaktywacja konta
					$users->unActive($this->view->identity->id);
				// 2. Losowanie kodu
					$kod = Application_Models_Authorize::setKod($this->view->identity->id);
				// 3. Mail co zrobić żeby je przywrócić
					$config = Zend_Registry::getInstance()->get('config');
				
					$email = new Zend_View();
					$email->setScriptPath('./Application/views/scripts/mail/');
					$email->imie = $this->view->identity->imie;
					$email->adres = "http://".$config->my->config->host.$this->view->baseUrl()."/index/registry/kod/$kod";
						
					$mail = new Zend_Mail('UTF-8');
					$mail->setSubject('HealthCom - Usunięcie konta')
						->addTo($this->view->identity->email)
						->setBodyHtml($email->render('unactive.phtml'))
						->send();
					
				
				// Wylogowanie 
				$auth = Zend_Auth::getInstance();
				$auth->clearIdentity();
				$this->form_ok();
				Rafyco_Logi::getInstance()->addText("Usunięto Twoje konto");
				return $this->form_redirect("/index/login");;
			}
		}
	}
	
	public function editAction(){
		$this->view->title = "Edycja konta";
		
		if($this->view->identity == null){
			return $this->login();
		}
		// Zapisytanie formularzy 
		$ret = false;
		$user = new Application_Models_Users();
		$dane['imie']=$this->view->identity->imie;
		$dane['nazwisko']=$this->view->identity->nazwisko;
		$form_edit = new Application_Form_Edition($dane);
		$form_edit->setAction(Zend_Controller_Front::getInstance()->getBaseUrl().'/index/edit')->setMethod('post');
		
		if($this->view->identity->rola == 'patient'){
			$patient = new Application_Models_Pacjenci();
			$pat = $patient->fetchRow("id=".$this->view->identity->id);
			
			$dane['pesel'] = $pat->pesel;
			$dane['grupakrwi'] = $pat->grupakrwi;
			$dane['nrubez'] = $pat->nrubez;
			$dane['ulica'] = $pat->ulica;
			$dane['nrDomu'] = $pat->nrDomu;
			$dane['kodPocztowy'] = $pat->kodPocztowy;
			$dane['miasto'] = $pat->miasto;
			$dane['tel'] = $pat->tel;
		
			$form_edit_pat = new Application_Form_Editpatient($dane);
			$form_edit_pat->setAction(Zend_Controller_Front::getInstance()->getBaseUrl().'/index/edit')->setMethod('post');
			
			$form_set_doc = new Application_Form_Mydoctor();
			$form_set_doc->setAction(Zend_Controller_Front::getInstance()->getBaseUrl().'/index/edit')->setMethod('post');
			if($pat->idDoc == 0){
				$this->view->form_set_doc_mess = "Nie wybrano żadnego lekarza";
			} else if($pat->idDoc != 0 && $pat->isDoc == 0 ){
				$this->view->form_set_doc_mess = "Oczekiwanie na potwierdzenie lekarza: ".$user->getAlias($pat->idDoc);
				$this->view->form_set_doc_mess .= "</p><p>Wpisanie nowego adresu spowoduje zaprzestanie oczekiwania na obecnego lekarza";
			} else {
				$this->view->form_set_doc_mess = "Twoim lekarzem jest: ".$user->getAlias($pat->idDoc);
				$this->view->form_set_doc_mess .= "</p><p>Wpisanie nowego adresu spowoduje skasowanie starego lekarza";
			}
		}
		
		if($this->view->identity->rola == 'doctor'){
			$doctor = new Application_Models_Lekarze();
			$doc = $doctor->fetchRow("id=".$this->view->identity->id);
		
			$dane['pesel'] = $doc->pesel;
			$dane['nrPWZ'] = $doc->nrPWZ;
		
			$form_edit_doc = new Application_Form_Editdoc($dane);
			$form_edit_doc->setAction(Zend_Controller_Front::getInstance()->getBaseUrl().'/index/edit')->setMethod('post');
			
		}
		
		$form_edit_hash = new Application_Form_Edithash();
		$form_edit_hash->setAction(Zend_Controller_Front::getInstance()->getBaseUrl().'/index/edit')->setMethod('post');
		
		$form_ustawienia = new Application_Form_Ustawienia($this->view->identity->id);
		$form_ustawienia->setAction(Zend_Controller_Front::getInstance()->getBaseUrl().'/index/edit')->setMethod('post');
		
		$formpht = new Application_Form_Photo();
		$formpht->setAction(Zend_Controller_Front::getInstance()->getBaseUrl().'/index/edit')->setMethod('post');
		$ret = "";
		// Obsługa formularzy
		if($this->_request->isPost()){
						
			if(isset($_POST['sub_edit']) && $form_edit->isValid($_POST)){
				
				$us = $user->fetchRow('id='.$this->view->identity->id);
				
				$us->imie = $form_edit->first_name->getValue();
				$us->nazwisko = $form_edit->last_name->getValue();
				$us->save();
				$this->view->identity->imie = $us->imie;
				$this->view->identity->nazwisko = $us->nazwisko;
				Rafyco_Logi::getInstance()->addText("Zmieniono dane osobiste");
				$ret = "elementry";
			
			}
			if(isset($_POST['sub_edit_pat']) && $form_edit_pat->isValid($_POST)){
				$pat->pesel = $form_edit_pat->pesel->getValue();
				$pat->grupakrwi = $form_edit_pat->blood->getValue();
				$pat->nrubez = $form_edit_pat->security_no->getValue();
				$pat->ulica = $form_edit_pat->address_street->getValue();
				$pat->nrDomu = $form_edit_pat->address_home_no->getValue();
				$pat->kodPocztowy = $form_edit_pat->address_code->getValue();
				$pat->miasto = $form_edit_pat->address_city->getValue();
				$pat->tel = $form_edit_pat->phone->getValue();
				$pat->save();
				Rafyco_Logi::getInstance()->addText("Zmieniono dane osobiste pacjenta");
				$ret = "personal";
			}
			if(isset($_POST['submit_mydoctor']) && $form_set_doc->isValid($_POST)){
				$idlekarza = $user->getUsersByEmail($form_set_doc->email->getValue())->id;
				$pat->idDoc = (int)$idlekarza;
				$pat->isDoc = 0;
				$pat->save();
				Rafyco_Logi::getInstance()->addText("Wysłano prośbę o przydzielenie lekarza");
				$ret = "doctor";
				
				
			}
			if(isset($_POST['sub_edit_doc']) && $form_edit_doc->isValid($_POST)){
				$doc->pesel = $form_edit_doc->pesel->getValue();
				$doc->nrPWZ = $form_edit_doc->pwz_no->getValue();
				$doc->save();
				Rafyco_Logi::getInstance()->addText("Zmieniono dane osobiste lekarza");
				$ret = "personal";
			}
			if(isset($_POST['sub_edit_hash']) && $form_edit_hash->isValid($_POST)){
			
				$us = $user->fetchRow('id='.$this->view->identity->id);
				$us->hash = md5(md5($form_edit_hash->pass->getValue()));
				$us->save();
				Rafyco_Logi::getInstance()->addText("Zmieniono hasło");
				$ret = "hash";
			}
			if(isset($_POST['submit_ustawienia']) && $form_ustawienia->isValid($_POST)){
				
				$ust = Application_Models_Ustawienia::factory($this->view->identity->id);
				
				$ust->wiadommail = $form_ustawienia->wiadommail->getValue();
				$ust->apilog = $form_ustawienia->apilog->getValue();
				$ust->upraw = $form_ustawienia->upraw->getValue();
				
				if(!$ust->apilog){
					$api = new Application_Models_Api();
					$api->delete("idUsera=".$this->view->identity->id);	
				}				
				
				$ust->save();
					
				Rafyco_Logi::getInstance()->addText("Zmieniono ustawienia konta");
				$ret= "acount";
				
			}
			if(isset($_POST['pht']) && $formpht->isValid($_POST)){
				try{
					$pht = new Rafyco_Photo('./public/images/none.jpg');
					$pht->open($formpht->getAdres());
					$pht->resize(300,300);
					$pht->delete($this->view->identity->id,'./database/photo/*ID*.jpg');
					$pht->save('./database/photo/'.$this->view->identity->id.'.jpg');
					Rafyco_Logi::getInstance()->addText("Twoje zdjęcie profilowe zostało zmienione");
					$ret = true;
				}catch (Exception $ex){
					Rafyco_Logi::getInstance()->addText("Niestety nie można zmienić Twojego zdjęcia profilowego");
					$ret = "avatar";
				}
			}
		}
		
		$this->view->form_edit = $form_edit;
		if($this->view->identity->rola == 'patient'){$this->view->form_edit_pat = $form_edit_pat;}
		if($this->view->identity->rola == 'patient'){$this->view->form_set_doc = $form_set_doc;}
		if($this->view->identity->rola == 'doctor'){$this->view->form_edit_doc = $form_edit_doc;}
		$this->view->form_edit_hash = $form_edit_hash;
		$this->view->formpht = $formpht;
		$this->view->form_ustawienia = $form_ustawienia;
		
		if($ret != "") $this->_redirect("/index/edit/#$ret");
		
		
	}
   
	public function forgotAction(){
		$this->view->title = "Przypomnienie hasła";
		
		// Jeśli zalogowany to do aktualności
		if($this->view->identity != null){
			return $this->_redirect('/index');
		}
		
		
		$forgot = new Application_Models_HashForgot();
		$forgot->clearBase();
		
		$this->view->form = new Application_Form_Forgot();
		
		if($this->_request->isPost()){
			
			if($this->view->form->isValid($_POST)){
				$data = $this->view->form->getValues();
								
				//echo $this->view->form->email;
				$user = new Application_Models_Users();
				$odp = 	$user->fetchRow("email='".$data['email']."'");
				$nr = $user->getId($data['email']);
				Rafyco_Logi::getInstance()->log($data['email']." [".$nr."]");
				$newkod = Application_Models_HashForgot::setKod($nr);
				
				// --------------- Wysyłanie maila ----------------------------
				$config = Zend_Registry::getInstance()->get('config');
				
				$email = new Zend_View();
				$email->setScriptPath('./Application/views/scripts/mail/');
				$email->imie = $user->getUsersByEmail($data['email'])->imie;
				$email->adres = "http://".$config->my->config->host.$this->view->baseUrl()."/index/forgot/kod/$newkod";
				
				//$tresc = "Witaj!!!<br />Aby wykonać przypomnienie odwiedź adres: <a href=\"$adres\">$adres</a><br />Healtchcom";
				
				$mail = new Zend_Mail('UTF-8');
				$mail->setSubject('Przypomnienie hasła w serwisie HealthCom')
					->addTo($data['email'])
					->setBodyHtml($email->render('addAdmin.phtml'))
					->send();
				
				
				// ------------------------------------------------------------
				
				
				
				$session = new Zend_Session_Namespace('message');
				$session->message = "Sprawdź swoją skrzynkę e-mail.";
				return $this->login();
			} else {
				Rafyco_Logi::getInstance()->addText("Podałeś niepoprawny email. Zapraszamy do rejestracji.");
				return;
			}
			
		} else {
		
			$kod = $this->_request->getParam('kod');
			
			if($kod == null){
			
				// Wyświetlanie formularza
				
			
				return;
			} else if($forgot->isKodExist($kod)){
				$odp = $forgot->getUserByKod($kod);
				$this->view->message = "Kod istnieje";
				$session = new Zend_Session_Namespace('forgot');
				$session->forgot = true;
				$session->kod = $kod;
				$session->id = $odp->id;
				
				return $this->_redirect('/index/changehash');
				// -----------------------------------------------------			
				
			} else {
				Rafyco_Logi::getInstance()->addText("Nie możesz przywrócić hasła z użyciem kodu: ".$kod);
				return $this->login();
			}
		
		
		
		}
		
	}
  
	public function changehashAction(){
		$this->view->title = "Przypomnienie hasła";
		
		$this->view->form = new Application_Form_ChHash();
		$this->view->form->setAction(Zend_Controller_Front::getInstance()->getBaseUrl().'/index/changehash')->setMethod('post');
		
		$session = new Zend_Session_Namespace('forgot');
		
		if($session->forgot != true || $this->view->identity != null){
			return $this->_redirect('/index/forgot');
		}
		
		$forgot = new Application_Models_HashForgot();
		$forgot->clearBase();
		
		if($this->_request->isPost() && $this->view->form->isValid($_POST)){
			
			$odp = $forgot->getUserByKod($session->kod);
			$data = $this->view->form->getValues();
			$odp->hash = md5(md5($data['pass']));
			$odp->status = 1;
			$odp->save();
			
			// ---------------- Logowanie ---------------------------
				$forgot->clearKod($kod);
					
				
				// Adapter bazy danych
				$db = Zend_Db_Table::getDefaultAdapter();
				// Tworzenie instancji adaptera
				$authAdapter = new Zend_Auth_Adapter_DbTable($db, 'users' , 'email', 'hash');
				//Wprowadzamy dane do adaptera
				$authAdapter->setIdentity($odp->email);
				$authAdapter->setCredential($odp->hash);

				//Sprawdzamy czy użytkownik aktywny
				$authAdapter->setCredentialTreatment("? AND status ='1'");
				//Autoryzacja
				$result = $authAdapter->authenticate();

				if($result->isValid()){
					//umieszczamy w sesji dane użytkownika
					$auth = Zend_Auth::getInstance();
					$storage  = $auth->getStorage();
					$storage->write($authAdapter->getResultRowObject(array(
						'id','email','imie','nazwisko','rola')));
					Rafyco_Logi::getInstance()->log('Procedura przywracania hasła dla: '.$odp->email);
					Rafyco_Logi::getInstance()->addText("Twoje hasło zostało zmienione");
					$session->forgot = false;
					$session->id = null;
					$session->kod = null;
					return $this->_redirect('/index/edit');
				} else {
					Rafyco_Logi::getInstance()->addText("Nie możesz przywrócić hasła z użyciem kodu: ".$kod);
					$session->forgot = false;
					$session->id = null;
					$session->kod = null;
					return $this->login();
				}
				
				
				
			
			// --------------------------------------------------------
		
		}
		
		
	}

	public function searchAction(){
		$this->view->title = "Wyszukiwarka użytkowników";
		
		$user = new Application_Models_Users();
		$form = new Application_Form_SelSurgery('Wpisz początek adresu e-mail, imienia lub nazwiska użytkownika którego chcesz znaleźć: ','Wyszukaj użytkownika');
		$form->setAction(Zend_Controller_Front::getInstance()->getBaseUrl().'/index/search')->setMethod('get');
		
		$find = $this->_request->getParam('find');
		
		$wyniki = $user->search($find,$this->view->identity->rola);
		
		$paginator = Zend_Paginator::factory($wyniki);
		$paginator->setItemCountPerPage(15);
		$page = $this->_getParam('page',1);
		$paginator->setCurrentPageNumber($page);
		
		$this->view->form = $form;
		$this->view->dane = $paginator;
	}
	
	public function shareAction(){
		$this->view->title = "Lista udostępnień";
		
		if($this->view->identity == null){
			return $this->login();
		}
		
		if($this->view->identity->rola != 'viewer'){
			return $this->_redirect('/index');
		}
		
		$udst = new Application_Models_Udostepnienia();
		$dane = $udst->getWhoUdst($this->view->identity->id);
		
		
		$paginator = Zend_Paginator::factory($dane);
		$paginator->setItemCountPerPage(15);
		$page=$this->_getParam('page',1);
		$paginator->setCurrentPageNumber($page);
		
		
		$this->view->dane = $paginator;
		
		
	}
}
