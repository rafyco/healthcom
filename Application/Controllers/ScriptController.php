<?php
class ScriptController extends Application_Controllers_DefaultController {

	public function init(){
		
		$auth = Zend_Auth::getInstance();
		if($auth->hasIdentity()){
			$this->view->identity = $auth->getIdentity();
		}
		
		if($this->view->identity == null && !preg_match("/pub*/",$this->_request->getActionName())){
			return $this->login();
		}
		
		$this->view->p = (int)$this->_request->getParam('page');
		
		
		
	}
	
	public function __call($metoda,$argumenty){
		$this->view->layout()->disableLayout();
		$this->view->render(header("Content-type: application/javascript"));
		$config = Zend_Registry::getInstance()->get('config');
		$this->view->host = $config->my->config->host;
		
		switch(str_replace("Action","",$metoda)){
			case "calendar":
				$this->view->dane = Application_Models_Lekarstwa::getAllFor($this->view->identity->id);
				break;
				
		}
	}
  
}