<?php
class DocController extends Application_Controllers_DefaultController {

	public function init(){
	
		$auth = Zend_Auth::getInstance();
		if($auth->hasIdentity()){
			$this->view->identity = $auth->getIdentity();
		}
		
		if($this->view->identity == null){
			return $this->login();
		}
		
		if($this->view->identity->rola != 'doctor'){
			return $this->_redirect('/error/unenter');
		}
		
	}
   
	public function indexAction() { // wyświetlenie wiadomości
	  	$this->view->title = "Lista pacjentów";
		$pacjenci = new Application_Models_Pacjenci();
		$lista = $pacjenci->getByDoctor($this->view->identity->id);
		
		$paginator = Zend_Paginator::factory($lista);
		$paginator->setItemCountPerPage(10);
		$page=$this->_getParam('page',1);
		$paginator->setCurrentPageNumber($page);
		
		$this->view->lista = $paginator;
	}	
	
	public function medicalAction(){
		
		$id = (int) $this->_request->getParam('id');
		$users = new Application_Models_Users();		
		
		if(!Application_Models_Pacjenci::isMyDoctor($id,$this->view->identity->id)){
			Rafyco_Logi::getInstance()->addText("Nie można modyfikować strony leków pacjenta");
			return $this->_redirect('/doc');
		}
		
		
		$this->view->user = $users->getUserById($id);
		$this->view->title = $this->view->user->imie." ".$this->view->user->nazwisko." - Lekarstwa";
		
		$dane = Application_Models_Lekarstwa::getForTree($id);
		
		$paginator = Zend_Paginator::factory($dane);
		$paginator->setItemCountPerPage(20);
		$page=$this->_getParam('page',1);
		$paginator->setCurrentPageNumber($page);
		
		
		$this->view->leki = $paginator;
	}
	
	public function addmedAction() { // wyświetlenie wiadomości
	  	$this->view->title = "dodaj lekarstwo";
		$this->loadJavaScript('/public/scripts/board.js');
		
		$form = new Application_Form_Drags((int) $this->_request->getParam('id'));
		
		
		if($this->_request->isPost() && $form->isValid($_POST)){
				if(Application_Models_Pacjenci::getDoctorById($form->idPac->getValue())->id == $this->view->identity->id){
				$lekarstwa = new Application_Models_Lekarstwa();
				
				$lek = $lekarstwa->fetchNew();
				$lek['idPac'] = (int)$form->idPac->getValue();
				$lek['nazwa'] = $this->view->escape($form->drag_name->getValue());
				$lek['rodzajlacz'] = $form->rodzaj->getValue();
				$lek->save();
				
				$n = $form->ildawek->getValue();
				$dat = new DateTime($form->first_dat->getValue()." ".$form->first_time->getValue());
				$date = $dat->format("U");
				
				$przyjmowanie = new Application_Models_Przyjmowanie();
				
				for($i=0;$i<$n;$i++){
					
					$prz = $przyjmowanie->fetchNew();
					$prz['idLeku'] = $lek->idLeku;
					$prz['data'] = date("Y-m-d H:i:s",$date);
					$prz['ilosc'] = $form->dawka->getValue()." ".$form->dawka2->getValue();
					$prz->save();
					$date = $date + ((int)$form->czestosc->getValue()*3600);
				
				
				}			
				
				
				Rafyco_Logi::getInstance()->addText("Wprowadzono lek dla pacjenta");
				return $this->_redirect('/doc/medical/id/'.$form->idPac->getValue().'#leki');
			} else {
				Rafyco_Logi::getInstance()->addText("Nie możesz wprowadzać leków dla tego użytkownika");
				return $this->_redirect('/doc');
			}					
		}
		
		$id = (int) $this->_request->getParam('id');
		
		$form->setAction(Zend_Controller_Front::getInstance()->getBaseUrl().'/doc/addmed/id/'.$id)->setMethod('post');
		
		if(!Application_Models_Pacjenci::isMyDoctor($id,$this->view->identity->id)){
			Rafyco_Logi::getInstance()->addText("Nie możesz wprowadzać leków dla tego użytkownika");
			return $this->_redirect('/doc/index');
		}
		
		$users = new Application_Models_Users();
		$this->view->pac = $users->getUserById($id);
		
		$this->view->form = $form;
		
	}

	public function delmedAction(){
		$pac = (int)$this->_request->getParam('pac');
		$this->view->title = "usunięcie leku";
		$this->form_setView(
			"/doc/delmed/pac/$pac",
			"Czy na pewno usunąć lek: ",
			"Nie możesz usunąć tego leku.",
			"?"); 
		
		$lekarstwa = new Application_Models_Lekarstwa();
		
		if($this->_request->isPost()){
		
			$filter = new Zend_Filter_Alpha();
			$this->view->id = (int) $this->_request->getPost('id');
			$del = $filter->filter($this->_request->getPost('del'));
			if($del == 'tak' && ($lekarstwa->isCanDel($this->view->identity->id,$this->view->id))){
				$lekarstwa->delLekById($this->view->id);
				Rafyco_Logi::getInstance()->addText("Usunięto lekarstwo");
			}
			
			
		} else {
			
			$this->view->id = $this->_request->getParam('id');
			$lek = $lekarstwa->getOne($this->view->id);
			
			if(!$lekarstwa->isCanDel($this->view->identity->id,$this->view->id)){
				return $this->_redirect("/doc/medical/id/$pac");
			}
			
			$cos['id'] = $this->view->id;
			
			$this->form_setMiddle($lek->nazwa);
			$this->form_setDane($cos);
			$this->form_yesno();
			return;
			 
			
		}
		
		$this->_redirect("/doc/medical/id/$pac");
	}
	
	public function authAction(){
		$this->view->title = "Autoryzowanie pacjenta";
		$this->form_setView(
			"/doc/auth",
			"Czy na pewno autoryzować pacjenta ",
			"Nie można autoryzować tego pacjenta.",
			"?"); 
			
		$pacjent = new Application_Models_Pacjenci();
		
		if($this->_request->isPost()){
		
			$filter = new Zend_Filter_Alpha();
			$this->view->id = (int) $this->_request->getPost('id');
			$del = $filter->filter($this->_request->getPost('del'));
			$al = $pacjent->getOneBy($this->view->id);
			if($del == 'tak' && ($this->view->identity->id == $al->idDoc)){
				$al->isDoc = 1;
				$al->save();
				$this->form_ok();
				Rafyco_Logi::getInstance()->addText("Autoryzowano pacjenta");
			}
			
			
		} else {
			
			$this->view->id = $this->_request->getParam('id');
			$al = $pacjent->getOneBy($this->view->id);
			
			if($al->id != $this->view->id){
				return $this->_redirect('/doc');
			}
			
			if($this->view->identity->id == $al->idDoc){
				$this->form_setMiddle($al->imie." ".$al->nazwisko);
				$this->form_setDane($al);
				$this->form_yesno();
				return;
			} 
			
		}
		
		$this->form_redirect('/doc');
	}

	public function delpatAction(){
		$this->view->title = "usunięcie pacjenta";
		$this->form_setView(
			"/doc/delpat",
			"Czy na pewno usunąć zaproszenie pacjenta ",
			"Nie możesz usunąć tego pacjenta.",
			"?"); 
		
		$pacjent = new Application_Models_Pacjenci();
		
		if($this->_request->isPost()){
		
			$filter = new Zend_Filter_Alpha();
			$this->view->id = (int) $this->_request->getPost('id');
			$del = $filter->filter($this->_request->getPost('del'));
			$al = $pacjent->getOneBy($this->view->id);
			if($del == 'tak' && ($this->view->identity->id == $al->idDoc)){
				$al->isDoc = 0;
				$al->idDoc = 0;
				$al->save();
				$this->form_ok();
				Rafyco_Logi::getInstance()->addText("Usunięto pacjenta");
			}
			
			
		} else {
			
			$this->view->id = $this->_request->getParam('id');
			$al = $pacjent->getOneBy($this->view->id);
			
			if($al->id != $this->view->id){
				return $this->_redirect('/doc');
			}
			
			if($this->view->identity->id == $al->idDoc){
				$this->form_setMiddle($al->imie." ".$al->nazwisko);
				$this->form_setDane($al);
				$this->form_yesno();
				return;
			} 
			
		}
		
		$this->_redirect('/doc');
	}
	
	

}