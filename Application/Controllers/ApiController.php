<?php
class ApiController extends Application_Controllers_DefaultController {

	protected $mobile = false;

	public function init(){
		
		$auth = Zend_Auth::getInstance();
		if($auth->hasIdentity()){
			$this->view->identity = $auth->getIdentity();
		}
		
		$this->api = new Application_Models_Api();
		$this->api->cleanBase();
		
		$kod = $this->_request->getParam('k');
		if($kod == null){
			$this->view->apiidentity = $this->view->identity;
		} else {
			$this->api->updateMe($kod);
			$this->view->apiidentity = $this->api->getUserByKode($kod);
			$this->mobile = true;
		}
		
		
	}
	
	
	private function setData($odp){
		$this->view->odp = $odp;
		$this->_helper->viewRenderer->renderBySpec('index', array('module' => 'default', 'controller' => 'api'));
	}
   
	private function secure(&$dane){
		if($this->view->apiidentity == null){
			$dane['odp'] = 'ERROR';
			$this->setData($dane);
			return true;
		} else {
			$kod = $this->_request->getParam('k');
			Rafyco_Apilog::getInstance()->log("search", $kod);
			$dane['odp'] = 'OK';
			return false;
		}
	
	}
   
   
   
	public function indexAction() { // wyświetlenie wiadomości
		if($this->secure($dane)) return;
		
		$dziennik = new Application_Models_Dzienniczek();
		$komm = new Application_Models_Komentarze();
		
		$wiadom = $dziennik->getAktual($this->view->apiidentity);
		$paginator = Zend_Paginator::factory($wiadom);
		$paginator->setItemCountPerPage(15);
		$page=$this->_getParam('p',1);
		$paginator->setCurrentPageNumber($page);
			
		$i=0;
		foreach($paginator as $pag){
			$dane[$i]['id'] = $pag->idDzienniczka;
			$dane[$i]['idPac'] = $pag->idPacjenta;
			$dane[$i]['imie'] = $this->view->escape($pag->imie);
			$dane[$i]['nazwisko'] = $this->view->escape($pag->nazwisko);
			if($this->mobile){
				$bbcode = Rafyco_Template::noBBcode();
			} else {
				$bbcode = Zend_Markup::factory('Bbcode');
			}
			if($pag->tresc != "")
				$dane[$i]['tresc'] = $bbcode->render($pag->tresc);
			else 
				$dane[$i]['tresc'] = "";
			
			$data = new DateTime($pag->data);
			if($this->mobile){
				$dane[$i]['data'] = Rafyco_Template::data($data,false);
			} else {
				$dane[$i]['data'] = Rafyco_Template::data($data);
			}
			$dane[$i]['skurczowe'] = $pag->cisskurcz;
			$dane[$i]['rozkurczowe'] = $pag->cisrozkurcz;
			$dane[$i]['tetno'] = $pag->tetno;
			$dane[$i]['samopoczucie'] = $pag->samopoczucie;
			$dane[$i]['lkom'] = $komm->countFor($pag->idDzienniczka);
			$dane[$i]['ktozapisal'] = $pag->ktozapisal;
			
			$i++;
		}
		
		$dane['n'] = $i; 
		$dane['pagx'] = $paginator->getCurrentPageNumber();
		$dane['pagn'] = ceil($paginator->getTotalItemCount()/15);
		
		$this->setData($dane);
		
	}
	
	public function addpostAction(){
		if($this->secure($opd)) return;
		$form = new Application_Form_Board($this->view->apiidentity->id,false,'Wyślij');
		$dziennik = new Application_Models_Dzienniczek();
		if($this->_request->isPost()){
			if($form->isValid($_POST)){
				
					$dane['idPacjenta'] = $this->view->apiidentity->id;
					$dane['cisskurcz'] = 0;
					$dane['cisrozkurcz'] = 0;
					$dane['samopoczucie'] = 0;
					$dane['tresc'] = $form->opis->getValue();
					$dane['uprawnienia'] = $form->uprawnienia->getValue();
					$apipanel = new Application_Models_Apipanel();
					
					$temp = $this->api->getByKode($this->_request->getParam('k'));
					if(isset($temp->Apipanel)){
					
						$apipanel = new Application_Models_Apipanel();
						$nazwa = $apipanel->fetchRow('id='.$temp->Apipanel);
						if(isset($nazwa->name))
						$dane['ktozapisal'] = $nazwa->name;
					}
					
					$dziennik->insert($dane);
					
					$opd['text'] = $dane['tresc'];
			} else {
				$opd['odp'] = 'NOVALID';
			}
		} else {
			$opd['odp'] = 'ERROR';
		}
		$this->setData($opd);
		
	}
	
	public function addboardAction(){
		if($this->secure($opd)) return;
		$form = new Application_Form_Board($this->view->apiidentity->id,true,'Wyślij');
		$dziennik = new Application_Models_Dzienniczek();
		if($this->_request->isPost()){
			if($form->isValid($_POST)){
				
					$dane['idPacjenta'] = $this->view->apiidentity->id;
					$dane['cisskurcz'] = (int)$form->cisS->getValue();
					$dane['cisrozkurcz'] = (int)$form->cisR->getValue();
					$dane['tetno'] = (int)$form->tetno->getValue();
					$dane['samopoczucie'] = (int)$form->samopoczucie->getValue();
					$dane['tresc'] = $form->opis->getValue();
					$dane['uprawnienia'] = $form->uprawnienia->getValue();
					$apipanel = new Application_Models_Apipanel();
					
					$temp = $this->api->getByKode($this->_request->getParam('k'));
					if(isset($temp->Apipanel)){
					
						$apipanel = new Application_Models_Apipanel();
						$nazwa = $apipanel->fetchRow('id='.$temp->Apipanel);
						if(isset($nazwa->name))
						$dane['ktozapisal'] = $nazwa->name;
					}
					
					$dziennik->insert($dane);
					
					$opd['text'] = $dane['tresc'];
			} else {
				$opd['odp'] = 'NOVALID';
			}
		} else {
			$opd['odp'] = 'ERROR';
		}
		$this->setData($opd);
		
	}
	
	public function delboardAction(){ // NO BOARD
		if($this->secure($opd)) return;
	
		$dzienniczek = new Application_Models_Dzienniczek();
		$this->view->id = (int)$this->_request->getPost('id');
		$por = $dzienniczek->fetchRow("idDzienniczka=".$this->view->id);
		
		if(!isset($por->idDzienniczka) || $por->idDzienniczka != $this->view->id || !$this->_request->isPost()){
			$opd['odp'] = 'ERROR';
			return $this->setData($opd);
		}
		
		if(($this->view->apiidentity->rola == 'admin' || $por->idPacjenta == $this->view->apiidentity->id) && $this->_request->getPost('del')=='tak'){
			$row = $dzienniczek->delWpis($this->view->id);
			$opd['odp'] = 'OK';		
		} else {
			$opd['odp'] = 'ERROR';
		}
		
		
		$this->setData($opd);
	}
	
	/* TO IMPROVE
		Sprawdzić czy warunek da się ładnie poprawić na sprawdzanie czy wpis jest przezemnie widoczny
	*/	
	public function addkommentAction(){
		if($this->secure($opd)) return;
		
		$form = new Application_Form_Koment();
		
		/*if(!Application_Models_Dzienniczek::isMyWpis($this->view->apiidentity->id,(int)$form->idPosta->getValue())){
			$opd['odp'] = 'ERROR';
			$this->setData($opd);
			return;
		}*/
		
		
		
		if($this->_request->isPost()){
			if($form->isValid($_POST)){
						
				$komm = new Application_Models_Komentarze();
				
				$dane = $komm->fetchNew();
				
				$dane['idPosta'] = (int)$form->idPosta->getValue();
				$dane['idAutora'] = (int)$this->view->apiidentity->id;
				$dane['tresc'] = $form->opis->getValue();
				$dane->save();
				
				$opd['text'] = $dane['tresc'];
			} else {
				$opd['odp'] = 'NOVALID';
			}
		} else {
			$opd['odp'] = 'ERROR';
		}
		$this->setData($opd);
	}
	
	public function kommentAction(){
		if($this->secure($dane)) return;
		
		$id = $this->_request->getParam('id');
		if(!is_numeric($id)){$id = 0;}
		
		$komentarze = Application_Models_Komentarze::getAll($id);
		
		
		$paginator = Zend_Paginator::factory($komentarze);
		$paginator->setItemCountPerPage(15);
		$page=$this->_getParam('p',1);
		$paginator->setCurrentPageNumber($page);
		$temp = $this->api->getByKode($this->_request->getParam('k'));
			if(isset($temp->Apipanel)){
				$bbcode = Rafyco_Template::noBBcode();
			} else {
				$bbcode = Zend_Markup::factory('Bbcode');
			}
		$i=0;
		foreach($paginator as $kom){
			$dane[$i]['id'] = $kom->idkoment;
			$dane[$i]['tresc'] = $bbcode->render($kom->tresc);	
			$dane[$i]['idAutora'] = $kom->idAutora;
			if($kom->idAutora == 0){ 
				$dane[$i]['alias'] = "Anonim";
			} else {
				$dane[$i]['alias'] = $kom->imie." ".$kom->nazwisko;
			}
				$i++;	
		}
		
		$dane['n'] = $i; 
		$dane['pagx'] = $paginator->getCurrentPageNumber();
		$dane['pagn'] = ceil($paginator->getTotalItemCount()/15);
		$this->setData($dane);
		
	}
	
	public function userAction(){
		if($this->secure($dane)) return;
		$us = new Application_Models_Users();
		
		$id = (int)$this->_request->getParam('id');
		
		if(Application_Models_Users::isUserExistId($id)){
		
			$user = $us->fetchRow('id='.$id);
			
		} else if ($id==0){
			$user = $this->view->apiidentity;
		} else {
			$dane['odp'] = 'ERROR';
			$this->setData($dane);
			return;
		}
			
		
		
		$dane['id'] = $user->id;
		$dane['imie'] = $user->imie;
		$dane['nazwisko'] = $user->nazwisko;
		$dane['rola'] = Rafyco_Template::rola($user->rola);
		
		if($user->rola == 'patient'){
			
			if($user->id == $this->view->apiidentity->id || Application_Models_Przyjaznie::isFriendStatic($this->view->apiidentity->id,$user->id) || Application_Models_Pacjenci::getIdDoctor(Application_Models_Pacjenci::getDoctorById($user->id)) == $this->view->apiidentity->id){
				
				$dane['email'] = $user->email;
				$patient = new Application_Models_Pacjenci();
				$odp = $patient->fetchRow("id=".$user->id);
				
				if($odp->idDoc != 0){
					$doctor = $us->getUserById($odp->idDoc);
					if($odp->isDoc == 1){
						$dane['lekarz prowadzacy'] = $doctor->imie." ".$doctor->nazwisko;
						$dane['idLekProw'] = $doctor->id;
					} else {
						if($odp->id == $this->view->apiidentity->id){
							$doctor = $us->getUserById($odp->idDoc);
							$dane['lekarz prowadzacy'] = $doctor->imie." ".$doctor->nazwisko." (niepotwierdzony)";
							$dane['idLekProw'] = $doctor->id;
						}
					}
				
				}
				
				$dane['pesel'] = $odp->pesel;
				$dane['grupa_krwi'] = Application_Models_Pacjenci::getGrKrwi($odp->grupakrwi);
				$dane['adres'] = str_replace("<br />","\n\t",Rafyco_Template::adres($odp->ulica,$odp->nrDomu,$odp->kodPocztowy,$odp->miasto));
				$dane['numer ubezpieczenia'] = $odp->nrubez;
				$dane['tel'] = $odp->tel;
			}
		} else if($user->rola == 'doctor'){
			
			$dane['email'] = $user->email;
			$doctor = new Application_Models_Lekarze();
			$odp = $doctor->fetchRow("id=".$user->id);
			
			$dane['pesel'] = $odp->pesel;
			$dane['numer PWZ'] = $odp->nrPWZ;
		} else {
			if(Application_Models_Przyjaznie::isFriendStatic($this->view->apiidentity->id,$id)){
				$dane['email'] = $user->email;			
			}
		}
		
		$this->setData($dane);
	}

	public function authAction() {
	
		$login = $this->_request->getParam('l');
		$hash = $this->_request->getParam('h');
		$apipanel = $this->_request->getParam('a');
		
		if($login == null || $hash == null || $apipanel == null){
			$kod = null;
		} else {
			$kod = $this->api->login($login,$hash,$apipanel);
		}
		
		if($kod == null){
			$dane['odp'] = "ERROR";
			$dane['kod'] = '000000000000000000000000000000';
		} else {
			$dane['odp'] = 'OK';
			$dane['kod'] = $kod;
		}
		
		if($dane['odp'] == 'OK'){
			Rafyco_Apilog::getInstance()->login($login, $kod,$param="Zalogowano");
		}
		$this->setData($dane);
	}

	public function tipAction() {
		$porada = new Application_Models_Porady();
		$this->setData($porada->losuj());
		
		
	}
	
	public function searchAction(){
		if($this->secure($dane)) return;
		
		$search = $this->_request->getParam('q');
		
		
		if($search == null) $search = "*";
		
		$user = new Application_Models_Users();
		$por = $user->search($search);
		
		$paginator = Zend_Paginator::factory($por);
		$paginator->setItemCountPerPage(20);
		$page=$this->_getParam('p',1);
		$paginator->setCurrentPageNumber($page);
		$i=0;
		foreach($paginator as $pag){
			
			$dane[$i]['id'] = $pag->id;
			$dane[$i]['imie'] = $pag->imie;
			$dane[$i]['nazwisko'] = $pag->nazwisko;
			$dane[$i]['email'] = $pag->email;
			$dane[$i]['rola'] = $pag->rola;
			$i++;
		}
		
		$dane['n'] = $i; 
		$dane['pagx'] = $paginator->getCurrentPageNumber();
		$dane['pagn'] = ceil($paginator->getTotalItemCount()/20);
		
		$this->setData($dane);
	}
   
	public function unlogAction(){
		$kod = $this->_request->getParam('k');
		$api = $this->api->getByKode($kod);
		$api->delete('kod='.$kod);
		$dane['odp'] = 'DEL';
		
		Rafyco_Apilog::getInstance()->login($this->view->apiidentity->email,$kod,"Wylogowano");
		$this->setData($dane);
	}
	
	public function messageAction(){
		if($this->secure($dane)) return;
		
		$mess = new Application_Models_Wiadomosci();
		
		$paginator = Zend_Paginator::factory($mess->getMess($this->view->apiidentity->id));
		$paginator->setItemCountPerPage(15);
		$page=$this->_getParam('page',1);
		$paginator->setCurrentPageNumber($page);
		$i=0;
		foreach($paginator as $wiadom){
			
			$dane[$i]['id'] = $wiadom->id;
			$dane[$i]['nadawca'] = Application_Models_Users::staticAlias($wiadom->od);
			$dane[$i]['nademail'] = Application_Models_Users::getEmail($wiadom->od);
			$dane[$i]['idnad'] = $wiadom->od;
			$dane[$i]['adresat'] = Application_Models_Users::staticAlias($wiadom->do);
			$dane[$i]['adresatemail'] = Application_Models_Users::getEmail($wiadom->do);
			$dane[$i]['idadr'] = $wiadom->do;
			$dane[$i]['temat'] = $wiadom->temat;
			
			if($this->mobile){
				$bbcode = Rafyco_Template::noBBcode();
			} else {
				$bbcode = Zend_Markup::factory('Bbcode');
			}
			if($wiadom->tresc != "")
				$dane[$i]['tresc'] = $bbcode->render($wiadom->tresc);
			else 
				$dane[$i]['tresc'] = "";
			
			$dane[$i]['read'] = $wiadom->read;
			$dane[$i]['data'] = Rafyco_Template::data(new DateTime($wiadom->data));
			$i++;
		}
		
		$dane['n'] = $i; 
		$dane['pagx'] = $paginator->getCurrentPageNumber();
		$dane['pagn'] = ceil($paginator->getTotalItemCount()/15);
		
		$this->setData($dane);
	}
	
	public function sendmessageAction(){
		if($this->secure($opd)) return;
		
		$form = new Application_Form_Message();
		$users = new Application_Models_Users();
		
		if($this->_request->isPost()){
			if($form->isValid($_POST)){
		
				$id = $this->view->apiidentity->id;
				$email2 = $form->ddo->getValue();
				$do = $users->getUsersByEmail($email2)->id;
				$temat = $form->temat->getValue();
				$tresc = $form->tresc->getValue();
				$opd['text'] = $tresc;
				
				require_once('CommController.php');
				
				CommController::sendSMT($id,$do,$temat,$tresc);
			
			} else {
				$opd['odp'] = 'NOVALID';
			}
		} else {
			$opd['odp'] = 'ERROR';
		}	
			
		$this->setData($opd);
		
	}
	
	public function messcountAction(){
		if($this->secure($dane)) return;
		
		$dane['count'] = Application_Models_Wiadomosci::staticCount($this->view->apiidentity->id);
		
		$this->setData($dane);
	}
	
	public function dragnotifAction(){
		if($this->secure($dane)) return;
		if($this->view->apiidentity->rola != 'patient'){
			$dane['odp'] = 'ERROR';
			$this->setData($dane);
			return;
		}
		$dziennik = new Application_Models_Dzienniczek();
		
		$dane['odp'] = 'OK';
		
		if($dziennik->isMust($this->view->apiidentity->id))
			$dane['powiadom'] = true;
		else 
			$dane['powiadom'] = false;
				
		//$dane['wynik'] = $dziennik->isMust($this->view->apiidentity->id);
		
		$this->setData($dane);
			
	}
	
	public function sessionmessAction(){
		if($this->secure($dane)) return;
		
		$session = Rafyco_Logi::getInstance();
		if($session->isText()){
			
			$bbcode = Zend_Markup::factory('Bbcode');
			$dane['odp'] = 	nl2br($bbcode->render($session->getText()));
			
		} else {
			$dane['odp'] = 'ERROR';
		}
		
		$this->setData($dane);
	}

	public function friendAction(){
		if($this->secure($dane)) return;
		
		$friend = new Application_Models_Przyjaznie();
		$list = $friend->getAll($this->view->apiidentity->id);
		
		if($this->view->apiidentity->rola == 'patient'){
			$patient = new Application_Models_Pacjenci();
			$my = $patient->getPatient($this->view->apiidentity->id);
			
			if($my->idDoc != 0){
				$doctor = Application_Models_Lekarze::getDoctor(Application_Models_Pacjenci::getDoctor($this->view->apiidentity)->id);
				if($my->isDoc == 1){
					// Lekarz potwierdzony
					$dane['doctor']['id'] = $doctor->id;
					$dane['doctor']['alias'] = $doctor->imie." ".$doctor->nazwisko;
					$dane['doctor']['stan'] = 1;
				} else {
					// Lekarz niepotwierdzony
					$dane['doctor']['id'] = $doctor->id;
					$dane['doctor']['alias'] = $doctor->imie." ".$doctor->nazwisko;
					$dane['doctor']['stan'] = 0;
				}
			
			} else {
				//Nie ma lekarza
				$dane['doctor']['stan'] = -1;
			}
		}
		
		$paginator = Zend_Paginator::factory($list);
		$paginator->setItemCountPerPage(15);
		$page=$this->_getParam('p',1);
		$paginator->setCurrentPageNumber($page);
		
		$i = 0;
		foreach($paginator as $one){
			$dane[$i]['id'] = $one->id;
			$dane[$i]['imie'] = $one->imie;
			$dane[$i]['nazwisko'] = $one->nazwisko;
			$dane[$i]['rola'] = $one->rola;
			$dane[$i]['email'] = $one->email;
			$i++;
		}
		
		$dane['n'] = $i; 
		$dane['pagx'] = $paginator->getCurrentPageNumber();
		$dane['pagn'] = ceil($paginator->getTotalItemCount()/15);
		
		$this->setData($dane);	
		
	}

	public function dragsAction(){
		if($this->secure($dane)) return;
		
		$leki = new Application_Models_Lekarstwa();
		$odp = $leki->getToHave($this->view->apiidentity->id);
		
		$paginator = Zend_Paginator::factory($odp);
		$paginator->setItemCountPerPage(15);
		$page=$this->_getParam('p',1);
		$paginator->setCurrentPageNumber($page);
		
		$i=0;
		
		foreach($paginator as $pag){
			$dane[$i]['id'] = $pag->idPrzyjmowania;
			$dane[$i]['idLeku'] = $pag->idLeku;
			$dane[$i]['nazwa'] = $pag->nazwa;
			$dane[$i]['rodzaj'] = $pag->rodzaj;
			$dane[$i]['data'] = $pag->data;
			$dane[$i]['ilosc'] = $pag->ilosc;
			$dane[$i]['isprzyjal'] = $pag->isprzyjal;
			$i++;
		}
		
		$dane['n'] = $i; 
		$dane['pagx'] = $paginator->getCurrentPageNumber();
		$dane['pagn'] = ceil($paginator->getTotalItemCount()/15);
		
		$this->setData($dane);
	}

	public function graphAction(){ 
		if($this->secure($dane)) return;
		
		if($this->view->apiidentity->rola != 'patient'){
			$dane['odp'] = 'ERROR';
			$this->setData($dane);
			return;
		}
		
		$dzienniczek = new Application_Models_Dzienniczek();
		$lista = $dzienniczek->getWyniki($this->view->apiidentity->id);
		$paginator = Zend_Paginator::factory($lista);
		$paginator->setItemCountPerPage(100);
		$page=$this->_getParam('page',1);
		$paginator->setCurrentPageNumber($page);
		
		$i = 0;
		foreach($paginator as $pag){
		
			$dane[$i]['cisSk'] = $pag->cisskurcz;
			$dane[$i]['cisRk'] = $pag->cisrozkurcz;
			$dane[$i]['sam'] = $pag->samopoczucie;
			if($pag->tetno != 0)
				$dane[$i]['tetno'] = $pag->tetno;
			$i++;
		}
		
		$dane['n'] = $i; 
		$dane['pagx'] = $paginator->getCurrentPageNumber();
		$dane['pagn'] = ceil($paginator->getTotalItemCount()/100);
		
		$this->setData($dane);
	}
	
	public function dragacceptAction(){ //no document
		if($this->secure($opd)) return;
		
		$leki = new Application_Models_Przyjmowanie();
		$this->view->id = (int)$this->_request->getPost('id');
		$por = $leki->getByPrzyjmowanie($this->view->id,$this->view->apiidentity->id);
		
		if(!isset($por->idPrzyjmowania) || !$this->_request->isPost()){
			$opd['odp'] = 'ERROR';
			return $this->setData($opd);
		}
		
		if($this->_request->getPost('del') == 'tak'){
			$por->isprzyjal = 1;
			$por->save();
			$opd['odp'] = 'OK';		
		} else {
			$opd['odp'] = 'ERROR';
		}
		
		
		$this->setData($opd);
	}
	
	public function readmessAction(){ // NO Document
		if($this->secure($opd)) return;
		
		$wiadomosci = new Application_Models_Wiadomosci();
		$this->view->id = (int)$this->_request->getPost('id');
		$mess = $wiadomosci->getMessIdentity($this->view->id,$this->view->apiidentity->id);
		
		if(!isset($mess->id) || $mess->id != $this->view->id || !$this->_request->isPost()){
			$opd['odp'] = 'ERROR';
			return $this->setData($opd);
		}
		
		if($this->_request->getPost('del') == 'tak'){
			$mess->read = 1;
			$mess->save();
			$opd['odp'] = 'OK';		
		} else {
			$opd['odp'] = 'ERROR';
		}
		
		
		$this->setData($opd);
	}
	
}