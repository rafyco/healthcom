<?php
class CommController extends Application_Controllers_DefaultController {

	static $base;
	
	public function init(){
		
		$auth = Zend_Auth::getInstance();
		if($auth->hasIdentity()){
			$this->view->identity = $auth->getIdentity();
		}
		
		if($this->view->identity == null){
			return $this->login();
		}
		
		$this->view->form = new Application_Form_Message();
		$this->view->form->setAction(Zend_Controller_Front::getInstance()->getBaseUrl().'/comm/send')->setMethod('post');
		self::$base = $this->view->baseUrl();
	}
   
	public function indexAction() { // wyświetlenie wiadomości
	  
		$this->view->title = "Skrzynka wiadomości"; 
		
		$mess = new Application_Models_Wiadomosci();
		
		$paginator = Zend_Paginator::factory($mess->getMess($this->view->identity->id));
		$paginator->setItemCountPerPage(10);
		$page=$this->_getParam('page',1);
		$paginator->setCurrentPageNumber($page);
		
		
		$this->view->messages = $paginator;
	}

	public function sendAction() {
		$this->view->title = "Nowa wiadomość";
		
		$users = new Application_Models_Users();
		
		if($this->_request->isPost() && $this->view->form->isValid($_POST)){
			
			$id = $this->view->identity->id;
			$email2 = $this->view->form->ddo->getValue();
			$do = $users->getUsersByEmail($email2)->id;
			$temat = $this->view->form->temat->getValue();
			$tresc = $this->view->form->tresc->getValue();
			
			self::sendSMT($id,$do,$temat,$tresc);
			//Wysłanie maila dla tych co chcą
			
			
			return $this->_redirect('/comm');
		}
		
		
	}

	public function delAction(){
		$this->view->title = "Usunięcie wiadomości";
		$this->form_setView(
			"/comm/del",
			"Czy na pewno usunąć wiadomość?<br />",
			"Nie można usunąć wiadomości."); 
		
		$mess = new Application_Models_Wiadomosci();
		
		if($this->_request->isPost()){
		
			$filter = new Zend_Filter_Alpha();
			$this->view->id = (int) $this->_request->getPost('id');
			$del = $filter->filter($this->_request->getPost('del'));
			$al = $mess->fetchRow('id='.$this->view->id);
			if($del == 'tak' && $this->view->identity->id == $al->do){
				$row = $mess->delete("id=".$this->view->id);
				Rafyco_Logi::getInstance()->addText("Wiadomosc usunięto");
			}
			
			
		} else {
			
			$this->view->id = $this->_request->getParam('id');
			$me = $mess->fetchRow("id=".$this->view->id);
			if($me->do == $this->view->identity->id){
				$this->form_setDane($me);
				$this->form_setMiddle($me->temat);
				$this->form_yesno();
				return;
			} 
			
		}
		
		$this->form_redirect('/comm');
		
		
		
	}
	
	public function showAction(){
	
		$id = $this->_request->getParam('id');
		$mess = new Application_Models_Wiadomosci();
		if($id)
			$odp = $mess->fetchRow('id='.$id);
	
		if(!isset($odp->id) || $odp->do != $this->view->identity->id){
			return $this->_redirect('/comm');
		}
		
		
	
		$this->view->title = $this->view->temat = $odp->temat;
		$this->view->tresc = $odp->tresc;
		$data = new DateTime($odp->data);
		$this->view->data = $data->format("d-m-Y  h:i:s");
		$this->view->idNadawcy = $odp->od;
		$this->view->idWiadomosci = $odp->id;
		$odp->read = 1;
		$odp->save();
	}
	
	public static function sendSMT($id,$do,$temat,$tresc){
		Application_Models_Wiadomosci::newMessage($id,$do,$temat,$tresc);
			$users = new Application_Models_Users();
			// --------- Wysyłanie maila ----------------
			if(Application_Models_Ustawienia::factory($do)->wiadommail == 1){
				
				$config = Zend_Registry::getInstance()->get('config');
				
				$email = new Zend_View();
				$email->setScriptPath('./Application/views/scripts/mail/');
				
				$me = $users->getUserById($id);
				$him = $users->getUserById($do);
				
				$email->imie = $him->imie;
				$email->odkogo = $me->imie." ".$me->nazwisko;
				$email->tytul = $temat;
				$email->tresc = $tresc;	
				$email->towiadom = "http://".$config->my->config->host.self::$base."/comm";
				$email->toustawienia = "http://".$config->my->config->host.self::$base."/index/edit";
				
				$mail = new Zend_Mail('UTF-8');
				$mail->setSubject('Otrzymałeś wiadomość w serwisie Healthcom')
					->addTo($him->email)
					->setBodyHtml($email->render('message.phtml'))
					->send();
					
				
			}
			
			
			// ------------------------------------------
			Rafyco_Logi::getInstance()->addText("Wiadomość do użytkownika ".$user->imie." ".$user->nazwisko
			." została wysłana.");
		
	
	}
}