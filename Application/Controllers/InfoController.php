<?php
class InfoController extends Application_Controllers_DefaultController {
	

	
	public function init(){
		
		$auth = Zend_Auth::getInstance();
		if($auth->hasIdentity()){
			$this->view->identity = $auth->getIdentity();
		}
		
		if($this->view->identity == null){
			return $this->login();
		}
		
		if($this->view->identity->rola != 'doctor' && $this->view->identity->rola != 'admin'){
			return $this->_redirect('/index');			
		}
		
		
	}
   
	public function indexAction() { // wyświetlenie wiadomości
	  
		$this->view->title = "Lista wprowadzonych porad";
		

		
		$session = new Zend_Session_Namespace('message');
		$this->view->message = $session->message;		
		
		$porady = new Application_Models_Porady();
		$us = new Application_Models_Users();
		if($this->view->identity->rola == 'admin'){
			$por = $porady->fetchAll();
		} else {
			$por = $porady->fetchAll('idAutora='.$this->view->identity->id);
		}
		
		$paginator = Zend_Paginator::factory($por);
		$paginator->setItemCountPerPage(10);
		$page=$this->_getParam('page',1);
		$paginator->setCurrentPageNumber($page);	
		
		
		$i=0;

		foreach($por as $porada){
			$dane[$i]['autor'] = $us->getAlias($porada->idAutora);
			$dane[$i]['porada'] = $porada->tresc;
			$dane[$i]['id'] = $porada->idAutora;
			$dane[$i]['idp'] = $porada->idPorady;
			
			$i++;
		}
	
		if($i<1)
			$dane = "Brak Porad";
		
		$this->view->dane = $dane;
		$this->view->paginator = $paginator;
		
	}

	public function addAction() {
		$this->view->title = "Dodanie porady"; 
		
		$this->view->form = new Application_Form_Info("","Dodaj poradę");
		$this->view->form->setAction(Zend_Controller_Front::getInstance()->getBaseUrl().'/info/add')->setMethod('post');
		
		
		
		if($this->_request->isPost() && $this->view->form->isValid($_POST)){
			
			$info = new Application_Models_Porady();
			$info->add($this->view->identity->id,$this->view->form->tresc->getValue());
			Rafyco_Logi::getInstance()->addText("Dodano poradę o treści: ".$this->view->form->tresc->getValue())->toLog();
			return $this->_redirect('/info');
		
		}
		
		
	}
	
	public function editAction(){
		$this->view->title = "Edytuj poradę";
		$this->view->id = $this->_request->getParam('id');
		$porady = new Application_Models_Porady();
		
		$por = $porady->fetchRow("idPorady=".$this->view->id);
		$this->view->porada = $por;
		
		if($por->idPorady!=$this->view->id || ($por->idAutora != $this->view->identity->id)){
			Rafyco_Logi::getInstance()->addText("Nie można edytować wskazanej porady");
			return $this->_redirect('/info');
		}
		
		//Wczytywanie formularza
		$form = new Application_Form_Info($por->tresc,"Zapisz",$por->idPorady);
		
		
		if($this->_request->isPost() && $form->isValid($_POST)){
		
			$por->tresc = $form->tresc->getValue();
			$por->save();
			Rafyco_Logi::getInstance()->addText("Zmieniono jedną z porad")->toLog();
			return $this->_redirect('/info');		
		}		
		$this->view->form = $form;
	}

	public function delAction(){
		$this->view->title = "Usuń poradę"; 
		$this->form_setView(
			"/info/del",
			"Czy usunąć poradę: \"",
			"Nie można usunąć porady",
			"\"?");
		$porady = new Application_Models_Porady();
		
		if($this->_request->isPost()){
		
			$filter = new Zend_Filter_Alpha();
			$this->view->id = (int) $this->_request->getPost('id');
			$del = $filter->filter($this->_request->getPost('del'));
			$al = $porady->fetchRow('idPorady='.$this->view->id);
			if($del == 'tak' && ($this->view->identity->rola == 'admin' || $this->view->identity->id == $al->idAutora)){
				$row = $porady->delete('idPorady='.$this->view->id);
				$this->form_ok();
				Rafyco_Logi::getInstance()->addText("Usunięto poradę");
			}
			
			
		} else {
			
			$this->view->id = $this->_request->getParam('id');
			$por = $porady->fetchRow("idPorady=".$this->view->id);
			
			if($por->idPorady != $this->view->id){
				return $this->form_redirect('/info');
			}
			
			if($this->view->identity->rola == 'admin' || $por->idPacjenta == $this->view->identity->id){
				$this->form_setMiddle($por->tresc);
				$dana['id'] = $por->idPorady;
				$this->form_setDane($dana);
				$this->form_yesno();
				return;
			} 
			
		}
		
		$this->form_redirect('/info');
			
			
			
	}

   
}