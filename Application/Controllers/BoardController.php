<?php
class BoardController extends Application_Controllers_DefaultController {

	public function init(){
		
		$auth = Zend_Auth::getInstance();
		if($auth->hasIdentity()){
			$this->view->identity = $auth->getIdentity();
		}
		$api = new Application_Models_Api();
		if($this->view->identity == null){
			return $this->login();
		}
		
	}
   
	public function indexAction() { // wyświetlenie wiadomości
	  
		$this->loadJavaScript('/public/scripts/flot/jquery.flot.js');
		  
		$id = (int)$this->_request->getParam('id');
		$us = new Application_Models_Users();
		
		// ---------- Ustalenie użytkownika którego chcemy obejrzeć
		if(Application_Models_Users::isUserExistId($id)){
			
			$user = $us->fetchRow('id='.$id);
			
		} else if ($id==0){
			$user = $this->view->identity;
			if($this->view->identity->rola != 'patient'){
				Rafyco_Logi::getInstance()->addText("Nie możesz zobaczyć swojego konta, ponieważ nie jesteś pacjentem");
				return $this->_redirect('/');
			}
			
		} else {
			return $this->_redirect('/error/unenter');
		}
		
		// --------- Wyjście jeśli nie jesteśmy upoważnieni do oglądania konta --------------
		if($user->rola != 'patient'){
			Rafyco_Logi::getInstance()->addText("Ten użytkownik nie jest pacjentem");
			return $this->_redirect('/index');
		}
		
		if(!Application_Models_Pacjenci::isMyDoctor($user->id,$this->view->identity->id) && $user->id != $this->view->identity->id && ! Application_Models_Udostepnienia::isUdststatic($user->id,$this->view->identity->id)){
			Rafyco_Logi::getInstance()->addText("Nie możesz oglądać raportu tego użytkownika");
			return $this->_redirect('/index');
		}
		
		
		
		// ------------- Dane konta -----------------------------
		
		$this->view->title = $user->imie." ".$user->nazwisko;
		
		$patient = new Application_Models_Pacjenci();
		$dzienniczek = new Application_Models_Dzienniczek();
		
		$me = $patient->getPatient($user->id);
		
		if($me->isDoc == 1){
			$doctor = Application_Models_Lekarze::getDoctor($me->idDoc);
			$this->view->doctor = $doctor->imie." ".$doctor->nazwisko;			
		} else {
			$this->view->doctor = "Brak lekarza prowadzącego";
		}
		
		$dane = $dzienniczek->getWyniki($user->id);
		$paginator = Zend_Paginator::factory($dane);
		$paginator->setItemCountPerPage(50);
		$page=$this->_getParam('page',1);
		$paginator->setCurrentPageNumber($page);
		
		
		
		$this->view->dane = $paginator;
		$this->view->dane2 = $dzienniczek->getWynikiwykres($user->id);
	
		
		$dane = Application_Models_Lekarstwa::getForTree($user->id);
		$pagleki = Zend_Paginator::factory($dane);
		$pagleki->setItemCountPerPage(20);
		$page=$this->_getParam('paglek',1);
		$pagleki->setCurrentPageNumber($page);
			
		$this->view->leki = $pagleki;
		$this->view->user = $user;
		

		
	}

	public function addAction() {
		if($this->view->identity->rola != 'patient'){
			return $this->_redirect('/');
		}
		$this->view->title = "Wprowadź dane"; 
		$form = new Application_Form_Board($this->view->identity->id);
		$this->loadJavaScript('/public/scripts/board.js');
		$form->setAction(Zend_Controller_Front::getInstance()->getBaseUrl().'/board/add')->setMethod('post');
			
		if($this->_request->isPost() && $form->isValid($_POST)){
			$dane['idPacjenta'] = $this->view->identity->id;
			$dane['cisskurcz'] = $form->cisS->getValue();
			$dane['cisrozkurcz'] = $form->cisR->getValue();
			$dane['tetno'] = $form->tetno->getValue();
			$dane['samopoczucie'] = $form->samopoczucie->getValue();
			$dane['tresc'] = $form->opis->getValue();
			$dane['uprawnienia'] = $form->uprawnienia->getValue();
			
			$doc = Application_Models_Pacjenci::getDoctorByID($this->view->identity->id);
			
			if(isset($doc->id)){
				$pacjenci = new Application_Models_Pacjenci();
				$pac = $pacjenci->getPatient($this->view->identity->id);
				
				if($pac->maxcisroz < $dane['cisskurcz'] || $pac->maxcisskur < $dane['cisrozkurcz'] || $pac->maxtent < $dane['tetno']){
					
					// Wystąpiło przeciążenie wyników.
						
						$user = new Application_Models_Users();
						$view = new Zend_View();
						$view->setScriptPath('./Application/views/scripts/mail/');
						
						$view->imie = $doc->imie;
						$view->podpis = $this->view->idenitity->imie." ".$this->view->idenitity->nazwisko;
						$view->adres = Zend_Controller_Front::getInstance()->getBaseUrl()."/board/index/id/".$this->view->identity->id;
						$view->profil = Zend_Controller_Front::getInstance()->getBaseUrl()."/index/show/id/".$this->view->identity->id;
						
						$view->cisskurcz = $dane['cisskurcz']." z limitem: ".$pac->maxcisskur;
						$view->cisrozkurcz = $dane['cisrozkurcz']." z limitem: ".$pac->maxcisroz;
						$view->tetno = $dane['tetno']." z limitem: ".$pac->maxtent;
						
						
						Application_Models_Wiadomosci::newSysMessage(
							$this->view->identity->id,
							$doc->id,
							"Pacjent ".$this->view->identity->imie." ".$this->view->ideniety->nazwisko." wprowadził zbyt duże wartości.",
							$view->render('maximale.phtml')
						);
					
				}
				
			}
			
			$dz = new Application_Models_Dzienniczek();
			$dz->insert($dane);
			
			Rafyco_Logi::getInstance()->log('Informacja o Twoim samopoczuciu została zapisana');
			return $this->_redirect('/');
		
		}
		$this->view->form = $form;
	}

	public function delAction(){
		$this->view->title = "Usuń wpis aktualności"; 
		$this->form_setView(
			"/board/del",
			"Czy usunąć ten wpis: \"",
			"Nie można usunąć wpisu",
			"\"?");
		
		$dzienniczek = new Application_Models_Dzienniczek();
		
		if($this->_request->isPost()){
			$filter = new Zend_Filter_Alpha();
			$this->view->id = (int) $this->_request->getPost('id');
			$this->json['id'] = $this->view->id;
			$del = $filter->filter($this->_request->getPost('del'));
			$this->json['del'] = $del;
			$al = $dzienniczek->fetchRow('idDzienniczka='.$this->view->id);
			if($del == 'tak' && ($this->view->identity->rola == 'admin' || $this->view->identity->id == $al->idPacjenta)){
				$row = $dzienniczek->delWpis($this->view->id);
				$this->form_ok();
				Rafyco_Logi::getInstance()->addText("Usunięto wpis");
			}
			
			
		} else {
			
			$this->view->id = (int)$this->_request->getParam('id');
			$por = $dzienniczek->fetchRow("idDzienniczka=".$this->view->id);
			
			if($por->idDzienniczka!=$this->view->id){
				return $this->form_redirect('/info');
			}
			
			if($this->view->identity->rola == 'admin' || $por->idPacjenta == $this->view->identity->id){
				$bbcode = Zend_Markup::factory('Bbcode'); 
				if($por->tresc != ""){
					$this->form_setMiddle($bbcode->render($por->tresc));
				} else {
					$this->form_setMiddle($por->tresc);
				}
				$dana['id'] = $por->idDzienniczka;
				$this->form_setDane($dana);
				$this->form_yesno();
				return;
			} 
			
		}
		
		$this->form_redirect('/index');
		
	}	
	
	public function medicalAction() {
		if($this->view->identity->rola != 'patient'){
			return $this->_redirect('/');
		}
		$this->view->title = "Moje proszki"; 
		
		$this->loadCss('/public/scripts/calendar/fullcalendar/fullcalendar.css');
		$this->loadJavaScript('/public/scripts/calendar/fullcalendar/fullcalendar.min.js');
	}

	public function settingAction() {
		if($this->view->identity->rola != 'patient'){
			return $this->_redirect('/');
		}
		$this->view->title = "Udostępnij dane";
		
		$udst = new Application_Models_Udostepnienia();
		$dane = $udst->getMyUdost($this->view->identity->id);
		
		$paginator = Zend_Paginator::factory($dane);
		$paginator->setItemCountPerPage(50);
		$page=$this->_getParam('page',1);
		$paginator->setCurrentPageNumber($page);
		
		$this->view->dane = $paginator;
		
		$form = new Application_Form_Share($this->view->identity->id);
		$form->setAction(Zend_Controller_Front::getInstance()->getBaseUrl().'/board/setting')->setMethod('post');
		
		if($this->_request->isPost() && $form->isValid($_POST)){
			$user = new Application_Models_Users();
			if(Application_Models_Users::isUserExist($form->email->getValue())){
				// Istnieje konto użytkownika
				$new = $udst->fetchNew();
				$new->idPac = $this->view->identity->id;
				$new->idViewer = $user->getId($form->email->getValue());
				$new->save();
				
				Rafyco_Logi::getInstance()->addText("Udostępniono Twoje dane użytkownikowi: ".$user->getAlias($new->idViewer));
				return $this->_redirect('/board/setting');
				
			} else {
				/* 1. Utwórz nowe konto */
					$new = $user->fetchNew();
					$new->imie = "Imie";
					$new->nazwisko = "Nazwisko";
					$new->rola = 'viewer';
					$new->email = $form->email->getValue();
					$new->hash = 0;
					$new->save();
				
				/* 2. Generuj kod */
					$newkod = Application_Models_HashForgot::setKod($new->id,0);
				
				/* 3. Wyślij maila */
					$config = Zend_Registry::getInstance()->get('config');
					
					$email = new Zend_View();
					$email->setScriptPath('./Application/views/scripts/mail/');
					$email->imie = $new->imie;
					$email->adres = "http://".$config->my->config->host.$this->view->baseUrl()."/index/forgot/kod/$newkod";
					
					$mail = new Zend_Mail('UTF-8');
					$mail->setSubject('Ktoś udostępnił Ci swoje wyniki w serwisie HealthCom')
						->addTo($new['email'])
						->setBodyHtml($email->render('authorizeViewer.phtml'))
						->send();
				
				/* 4. Utwórz powiązanie */
					$udst->addUdst($this->view->identity->id,$new->id);
					
					Rafyco_Logi::getInstance()->addText("Udostępniłeś dane użytkownikowi po adresem: ".$new->email);
					return $this->_redirect('/board/setting');
			
			}
		
		
		}	
		
		$this->view->form = $form;
		
	}
   
	public function deludstAction() {	// Usunięcie udostępniania
		if($this->view->identity->rola != 'patient'){
			Rafyco_Logi::getInstance()->addText("Nie możesz cofnąć udostępniania");
			return $this->_redirect('/');
		}
		$this->view->title = "Przestań udostępniać";
		$this->form_setView(
			"/board/deludst",
			"Czy cofnąć dostęp użytkownikowi ",
			"Nie można cofnąć dostępu",
			" ?");
		
			$this->view->id = (int) $this->_request->getParam('id');
		
		if($this->view->identity->rola != 'patient'){
			Rafyco_Logi::getInstance()->addText("Nie możesz cofnąć udostępniania");
			return $this->form_redirect('/board/setting');
		}
		$this->form_setDane(null);
		$users = new Application_Models_Users();
		$udst = new Application_Models_Udostepnienia();
		
		if($this->_request->isPost()){
			$filter = new Zend_Filter_Alpha();
			$del = $filter->filter($this->_request->getPost('del'));
			
			if($del == 'tak' && ($udst->isUdst($this->view->identity->id,$this->view->id))){
				$row = $udst->delUdst($this->view->identity->id,$this->view->id);
				$this->form_ok();
				Rafyco_Logi::getInstance()->addText("Usunięto powiązanie");
			}
			
			
			
		} else {
			
			$por = $users->fetchRow("id=".$this->view->id);
			
						
			if($this->view->identity->rola == 'patient'){
				$this->form_setDane($por);
				
				$imie = ' '; $nazwisko = ' ';
				if(isset($por->imie)) $imie = $por->imie;
				if(isset($por->nazwisko)) $nazwisko = $por->nazwisko;
					
				$this->form_setMiddle($imie." ".$nazwisko);
				return $this->form_yesno();
			} 
			
		}
		$this->form_redirect('/board/setting');
		
		
		
		
		
	}	
	
	public function acceptmedAction(){
		if($this->view->identity->rola != 'patient'){
			Rafyco_Logi::getInstance()->addText("Nie możesz potwierdzić tego leku");
			return $this->_redirect('/board/medical');
		}
		$this->view->title = "Potwierdzenie wzięcia leku";
		$this->form_setView(
			"/board/acceptmed",
			"Czy pobrałeś lek ",
			"Nie możesz pobrać leku",
			" ?");
		
		$przyjmowanie = new Application_Models_Przyjmowanie();
		$leki = new Application_Models_Lekarstwa();
		
		$this->view->id = (int) $this->_request->getParam('id');
		
		$this->view->lek = $przyjmowanie->getByPrzyjmowanie($this->view->id,$this->view->identity->id);
		
		if($this->view->lek == null){
			Rafyco_Logi::getInstance()->addText("Nie możesz potwierdzić tego leku");
			return $this->_redirect('/board/medical');
		}
		
		$this->form_setDane(null);		
		
		if($this->_request->isPost()){
			$filter = new Zend_Filter_Alpha();
			$del = $filter->filter($this->_request->getPost('del'));
			
			if($del == 'tak'){
				
				$this->view->lek->isprzyjal = 1;
				$this->view->lek->save();
				
				
				$this->form_ok();
				Rafyco_Logi::getInstance()->addText("Potwierdzono wzięcie tego leku");
			}
			
			
			
		} else {
						
				$dane['id'] = $this->view->id;
				$this->form_setDane($dane);
				
				
					
				$this->form_setMiddle($this->view->lek->nazwa);
				return $this->form_yesno();
			
		}
		$this->form_redirect('/board/medical');
		
		
		
	
	}
	
}