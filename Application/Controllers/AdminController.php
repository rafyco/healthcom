<?php
class AdminController extends Application_Controllers_DefaultController {

	public function init(){
		
		$auth = Zend_Auth::getInstance();
		if($auth->hasIdentity()){
			$this->view->identity = $auth->getIdentity();
		}
		
		if($this->view->identity == null){
			return $this->login();
		}
		
		if($this->view->identity->rola != 'admin'){
			return $this->_redirect('/error/unenter');
		}
		
		
	}
   
	public function indexAction() { // wyświetlenie wiadomości
		$this->view->title = "Lista użytkowników";
		
		$us = new Application_Models_Users();
		$us->clear();
		$data = $us->fetchAll();
		
		$paginator = Zend_Paginator::factory($data);
		$paginator->setItemCountPerPage(20);
		$page=$this->_getParam('page',1);
		$paginator->setCurrentPageNumber($page);
		
		
		$this->view->dane = $paginator;
		$form = new Application_Form_Admin();
		
		if($this->_request->isPost() && $form->isValid($_POST)){
			$new = $us->CreateRow();
			
			$new['email'] = $form->email->getValue();
			$new['imie'] = $form->first_name->getValue();
			$new['nazwisko'] = $form->last_name->getValue();
			$new['rola'] = 'admin';
			$new['status'] = 0;
			$new->save();
			
			$newkod = Application_Models_HashForgot::setKod($new->id,0);
			
			// ---------- Mail aktywacyjny ---------------------------
				$config = Zend_Registry::getInstance()->get('config');
				
				$email = new Zend_View();
				$email->setScriptPath('./Application/views/scripts/mail/');
				$email->imie = $new->imie;
				$email->adres = "http://".$config->my->config->host.$this->view->baseUrl()."/index/forgot/kod/$newkod";
				
				$mail = new Zend_Mail('UTF-8');
				$mail->setSubject('Zostałeś wybrany na administratora serwisu HealthCom')
					->addTo($new['email'])
					->setBodyHtml($email->render('addAdmin.phtml'))
					->send();
			
			
			
			// --------------------------------------------------------
			
			
			Rafyco_Logi::getInstance()->addText("Nowy administrator: ".$new->imie." ".$new->nazwisko);
			Rafyco_Logi::getInstance()->addText("Prosimy o sprawdzenie maila w celu ustawienia hasła");
			return $this->_redirect('/admin'); 
		}
		
		
		$this->view->form = $form;
	}
	
	public function acceptAction() { // wyświetlenie wiadomości
		$this->view->title = "Potwierdzenie użytkownika";
		$this->form_setView(
			"/admin/accept",
			"Czy chcesz potwierdzasz konto: ",
			"Nie można wykonać akcji.",
			"?"); 
			
		$users = new Application_Models_Users();
		$this->view->id = (int) $this->_request->getParam('id');
		
		if($this->view->id == $this->view->identity->id){
			Rafyco_Logi::getInstance()->addText("Nie można potwierdzić własnego konta");
			return $this->form_redirect('/admin');
		}
		
		if($this->_request->isPost()){
		
			$filter = new Zend_Filter_Alpha();
			$acc = $filter->filter($this->_request->getPost('del'));
			$al = $users->fetchRow('id='.$this->view->id);
			if($acc == 'tak' && $this->view->identity->rola == 'admin'){
				$row = $users->fetchRow("id=".$this->view->id);
				$row->status = 1;
				$row->save();
				$this->form_ok();
				Rafyco_Logi::getInstance()->addText("Aktywowano konto");
			}
			
			
		} else {
			
			
			$us = $users->fetchRow('id='.$this->view->id);
			if(!isset($us->id)){
				return $this->form_redirect('/admin');
			}			
			
			if($this->view->identity->rola == 'admin'){
				$this->form_setDane($us);
				$this->form_setMiddle($us->imie." ".$us->nazwisko);
				$this->form_yesno();
				return;
			} 
			
		}
		
		$this->form_redirect('/admin');
	}
	
	public function delAction() { // wyświetlenie wiadomości
		$this->view->title = "Usuwanie konta";
		$this->form_setView(
			"/admin/del",
			"Czy chcesz usunąć użytkownika: ",
			"Nie można wykonać akcji.",
			"?"); 
			
		$this->view->id = (int) $this->_request->getParam('id');
		$users = new Application_Models_Users();
		
		if($this->view->id == $this->view->identity->id){
			Rafyco_Logi::getInstance()->addText("Nie skasować własnego konta");
			return $this->form_redirect('/admin');
		}
		
		if($this->_request->isPost()){
		
			$filter = new Zend_Filter_Alpha();
			$acc = $filter->filter($this->_request->getPost('del'));
			$al = $users->fetchRow('id='.$this->view->id);
			if($acc == 'tak' && $this->view->identity->rola == 'admin'){
				Application_Models_Users::deleteById($this->view->id);
				$this->form_ok();
				Rafyco_Logi::getInstance()->addText("Skasowano konto");
			}
			
			
		} else {
			
			
			$us = $users->fetchRow('id='.$this->view->id);
			if(!isset($us->id)){
				return $this->form_redirect('/admin');
			}			
			
			if($this->view->identity->rola == 'admin'){
				$this->form_setDane($us);
				$this->form_setMiddle($us->imie." ".$us->nazwisko);
				$this->form_yesno();
				return;
			} 
			
		}
		
		$this->form_redirect('/admin');
	}
	
	public function disacceptAction(){
		$this->view->title = "Zablokowanie użytkownika";
		$this->form_setView(
			"/admin/disaccept",
			"Czy chcesz zablokować konto: ",
			"Nie można wykonać akcji.",
			"?"); 
			
		$users = new Application_Models_Users();
		
		$id = $this->_request->getParam('id');
		
		if($id == $this->view->identity->id){
			Rafyco_Logi::getInstance()->addText("Nie można zablokować własnego konta");
			return $this->form_redirect('/admin');
		}		
		
		if($this->_request->isPost()){
		
			$filter = new Zend_Filter_Alpha();
			$acc = $filter->filter($this->_request->getPost('del'));
			$al = $users->fetchRow('id='.$id);
			if($acc == 'tak' && $this->view->identity->rola == 'admin'){
				$row = $users->fetchRow("id=".$id);
				$row->status = 0;
				$row->at = date('0000-00-00 00:00:00');
				$row->save();
				$this->form_ok();
				Rafyco_Logi::getInstance()->addText("Zablokowano konto");
			}
			
			
		} else {
					
			
			$us = $users->fetchRow('id='.(int)$id);
			if(!isset($us->id)){
				Rafyco_Logi::getInstance()->addText("Konto nie istnieje");
				return $this->form_redirect('/admin');
			}			
			
			if($this->view->identity->rola == 'admin'){
				$this->form_setDane($us);
				$this->form_setMiddle($us->imie." ".$us->nazwisko);
				$this->form_yesno();
				return;
			} 
			
		}
		
		$this->form_redirect('/admin');
	
	
	}
   
}