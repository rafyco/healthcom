<?php 

class Application_Models_Api extends Zend_Db_Table_Abstract {
	protected $_name = "api";
	protected $_primary = "id";
	
	/**
		Losuje dowolny ci�g znak�w
	*/
	public static function generateKode($liczba=30){
		
		$w = '';
		//Wyst�puj�ce znaki
		$temp = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
		$n=strlen($temp);
		for($i=0;$i<$liczba;$i++){
		
			$w .= substr($temp, rand(0, $n), 1);
		}
		return $w;
	}
	
	/**
		Oczyszczanie bazy danych
	*/
	public function cleanBase(){
		
		//Usuwanie wpis�w starszych ni� 3 godziny.
			$date = date('Y-m-d H:i:s', strtotime('-2 days'));
			$this->delete("data<'".$date."' AND end=1");
			
	}
	
	public function isKodExist($kod){
		$odp = $this->fetchRow("kod LIKE '".$kod."'");
		return true;//isset($odp->kod);	
		
	}
	
	public function login($login,$haslo,$kod){
		// 1. Sprawdzanie istnienia takiego loginu i has�a
		$user = new Application_Models_Users();
		$apipanel = new Application_Models_Apipanel();
		
		
		$select = $user->select()->where("email LIKE '".$login."' AND hash LIKE '".md5($haslo)."' AND status=1 ");
		
		$us = $user->fetchRow($select);
		
		if(!isset($us->id)){
			return null;
		}
		
		if(!$apipanel->isExistKod($kod)){
			return null;
		}
		
		if(!Application_Models_Ustawienia::factory($us->id)->apilog){
			return null;
		}
		
		// 2. Losowanie kodu
		
		do{
			$newkod = self::generateKode();
		}while(!$this->iskodExist($newkod));
		// 3. Wpis do bazy
			$dane['idUsera'] = $us->id;
			$dane['kod'] = $newkod;
			$dane['Apipanel'] = $apipanel->getByKod($kod)->id;
			$this->insert($dane);
		
		return $newkod;
	}
	
	public function getByKode($kod){
		return $this->fetchRow("kod LIKE '".$kod."'");
	}
	
	public function getUserByKode($kod){
		$ob = $this->getByKode($kod);
		if(!isset($ob->kod)){
			return null;
		}
		
		$users = new Application_Models_Users();
		$us = $users->fetchRow("id=".$ob->idUsera);
		
		if(!isset($us->id)){
			return null;
		}
		return $us;
	}
	
	public function updateMe($kod){
		$op = $this->fetchRow("kod LIKE '".$kod."'");
		if(isset($op->kod)){
			$date = date('Y-m-d H:i:s');
			$op->data = $date;
			$op->save();
		}
	}
		
}

