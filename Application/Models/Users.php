<?php 

class Application_Models_Users extends Zend_Db_Table_Abstract {
	protected $_name = "users";
	protected $_primary = "id";
	
	public function getAlias($id){
		$w = $this->fetchRow("id=".$id);
		if(!isset($w->id)){
			return 'Nieznany';
		}
		return $w->imie." ".$w->nazwisko;	
	}
	
	public static function getEmail($id){
		$users = new Application_Models_Users();
		$w = $users->fetchRow("id=".(int)$id);
		if(!isset($w->email)){
			return "";
		}
		return $w->email;
	}
	
	public static function staticAlias($id){
		$u = new Application_Models_Users();
		return $u->getAlias($id);
	}
	
	public static function isUserExist($email){
		$user = new Application_Models_Users();		
		$odp = $user->fetchRow("email LIKE '".$email."'");
		return isset($odp->email);		
	}
	
	public static function isUserExistId($id){
		$user = new Application_Models_Users();		
		$odp = $user->fetchRow("id=".$id);
		return isset($odp->id);
	}
	
	public static function isUserWork($email){
		$user = new Application_Models_Users();		
		$odp = $user->fetchRow("email LIKE '".$email."' AND status=1");
		return isset($odp->email);	
	}
	
	public function getId($email){
		if(self::isUserExist($email)){
			$odp = $this->fetchRow("email LIKE '".$email."'");
			return $odp->id;
		} else {
			return 0;
		}
	}
	
	public function getUserById($id){
		return $this->fetchRow("id=".(int)$id);
	}
	
	public static function isDoctor($email){
		$user = new Application_Models_Users();
		if(Application_Models_Users::isUserExist($email)){
			$us = $user->getUsersByEmail($email);
			return $us->rola == 'doctor';
		} else {
			return false;
		}
	}
	
	public function getUsersByEmail($email){
		return $this->fetchRow("email LIKE '".$email."'");
	}
	/**
		Czyszczenie bazy danych z użytkowników nieaktywnych i niepotwierdzonych
	*/
	public function clear(){
		$date = date('Y-m-d H:i:s', strtotime('-3 days'));
		try {
			$odp = $this->fetchAll($this->select()->where("at<'".$date."' AND status=0 AND not(at='0000-00-00 00:00:00')"));
			if($odp != null){
				
				foreach($odp as $od){
				
					self::deleteById($od->id);
				
				}
			
			}
		} catch(Exception $ex){}
	}
	
	/**
		Usuwanie użytkownika o wybranym id
	*/
	public static function deleteById($id=0){
	
		if(self::isUserExistId($id)){
			try{
			$temp = new Application_Models_Users();
			$temp->delete("id=".$id);
			} catch (PDOExcepiton $ex){}
			
			try{
			$temp = new Application_Models_Pacjenci();
			$temp->delete("id=".$id);
			} catch (PDOExcepiton $ex){}
			
			try{
			$temp = new Application_Models_Lekarze();
			$temp->delete("id=".$id);
			} catch (PDOExcepiton $ex){}
			
			try{
			$temp = new Application_Models_Lekarzgabinet();
			$temp->delete("idLekarza=".$id);
			} catch (PDOExcepiton $ex){}
			
			try{
			$temp = new Application_Models_Authorize();
			$temp->delete("idUsera=".$id);
			} catch (PDOExcepiton $ex){}
			
			/*$temp = new Application_Models_Dzienniczek();
			$select = $temp->select(Zend_Db_Table_Abstract::SELECT_WITH_FROM_PART)
				->setIntegrityCheck(false)
				->
			*/
			
			try{
			$temp = new Application_Models_Dzienniczek();
			$temp->delete("idPacjenta=".$id);
			} catch (PDOExcepiton $ex){}
			
			try{
			$temp = new Application_Models_Udostepnienia();
			$temp->delete("idViewer=".$id);
			$temp->delete("idPac=".$id);
			} catch (PDOExcepiton $ex){}
			
			/* TO DO
			Skasować wszystkie komentarze ze skasowanych wpisów w dzienniczku */
			
			try{
			$temp = new Application_Models_Komentarze();
			$temp->update(array("idAutora" => "0"),"idAutora=".$id);
			} catch (PDOExcepiton $ex){}
			
			
			if(is_file('./database/photo/'.$id.".jpg")){
				unlink('./database/photo/'.$id.".jpg");
			}
			
			
			if(is_file('./database/photo/'.$id."_mini.jpg")){
				unlink('./database/photo/'.$id."_mini.jpg");
			}
			
			
			if(is_file('./database/photo/'.$id."_mobile.jpg")){
				unlink('./database/photo/'.$id."_mobile.jpg");
			}
		
		}	
	}
	
	public function athorize($id){
		if(self::isUserExistId($id)){
			$this->update(Array('status' => '1'),"id=".$id);
			return true;
		} else {
			return false;
		}
		
	}

	public function unActive($id){
		$date = date('Y-m-d H:i:s');
		$odp = $this->fetchRow("id=".$id);
		
		if($odp != null){
			$odp->status = 0;
			$odp->at = $date;
		
			$odp->save();
		}
	}
	
	public function search($q,$rola){
		$q = trim($q);
		
		if($q == null ) return $this->fetchAll($this->select()->where("email like ' '"));
		
		$odp = explode(" ",$q);
	
		$select = $this->select()->where("id LIKE '$q' OR email LIKE '$q' AND NOT(rola LIKE 'viewer')");
		
		foreach($odp as $od){
			switch($rola){
			case "admin":
				$select->orWhere("(email LIKE '$od%' OR imie LIKE '$od%' OR nazwisko LIKE '$od%') AND NOT(rola LIKE 'viewer')");
				break;
			case "doctor":
				$select->orWhere("(email LIKE '$od%' OR imie LIKE '$od%' OR nazwisko LIKE '$od%') AND rola LIKE 'doctor' AND NOT(rola LIKE 'viewer')");
			default:
				$select->orWhere("(email LIKE '$od%' OR imie LIKE '$od%' OR nazwisko LIKE '$od%') AND rola LIKE 'patient' AND NOT(rola LIKE 'viewer')");
			}
		}
	
		return $this->fetchAll($select);
	}
	
	public static function getMozaic(){
		$user = new Application_Models_Users();
		$select = $user->select()
			->where("status = 1")
			->order('rand()')
			->limit(12);
			
		return $user->fetchAll($select);
	}
}

