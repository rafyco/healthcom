<?php 

class Application_Models_Udostepnienia extends Zend_Db_Table_Abstract {
	protected $_name = "udostepnienia";
	protected $_primary = "idUdst";
	
	public function addUdst($me,$ktos){
		if(!$this->isUdst($me,$ktos)){
			$new = $this->fetchNew();
			$new->idViewer = $ktos;
			$new->idPac = $me;
			$new->save();
		}
	}
	
	public static function isUdststatic($pac,$view){
		$udst = new Application_Models_Udostepnienia();
		return $udst->isUdst($pac,$view);
	}
	
	public function isUdst($pac,$ktos){
		$pac2 = (int)$pac;
		$ktos2 = (int)$ktos;
		if($pac2 == 0 || $ktos2 == 0 ) return false;
		$odp = $this->fetchRow("idViewer=$ktos2 AND idPac=$pac2");
		return isset($odp->idUdst);
	}
	
	public function getWhoUdst($view){ // Pobierz list� tych, kt�rzy mi udost�pniaj�
		$select = $this->select(Zend_Db_Table_Abstract::SELECT_WITH_FROM_PART)
				->setIntegrityCheck(false)
				->joinleft('users','idPac = users.id')
				->where("idViewer=$view");
		
		return $this->fetchAll($select);
	}
	
	public function getMyUdost($pac){ // Pobierz list� komu udost�pniam
		$select = $this->select(Zend_Db_Table_Abstract::SELECT_WITH_FROM_PART)
				->setIntegrityCheck(false)
				->joinleft('users','idViewer = users.id')
				->where("idPac=$pac");
		
		return $this->fetchAll($select);
	}
	
	public function delUdst($pac,$view){
		$this->delete("idViewer=$view AND idPac=$pac");
	}
	
	public function getList($me){
		$list = $this->fetchAll(
			$this->select(Zend_Db_Table_Abstract::SELECT_WITH_FROM_PART)
			->setIntegrityCheck(false)
			->joinleft('users','idPac=users.id')
			->where("idViewer=$me")
			->order('rand()')
			->limit(5)
			);
		if($list->count() == 0 )
			return null;
		else 
			return $list;
	}
	
}

