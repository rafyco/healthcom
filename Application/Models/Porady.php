<?php 

class Application_Models_Porady extends Zend_Db_Table_Abstract {
	protected $_name = "porady";
	protected $_primary = "idPorady";
	
	public function losuj(){
		try{
		$sql = $this->select();
		$sql->order('rand()');
		$sql->limit(1);
		
		$rows=$this->fetchRow($sql);
		
		if($rows == null){
			throw new Exception();
		}

		
		
		$dane['porada'] = $rows->tresc;
		
		Zend_Loader::loadClass('Application_Models_Users');
		$user = new Application_Models_Users();
		
		$dane['autor'] = $user->getAlias($rows->idAutora);
		$dane['id'] = $rows->idAutora;
		} catch (Exception $ex){
			$dane['porada'] = "Przykro mi, ale brak porad w bazie.";
			$dane['autor'] = "Nieznany";
			$dane['id'] = 0;
		}
		
		return $dane;
		
	}
	
	public function add($id,$text){
	
		$dane['tresc'] = $text;
		$dane['idAutora'] = $id;
		$this->insert($dane);
	}
	

	
}

