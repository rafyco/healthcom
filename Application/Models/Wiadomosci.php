<?php 

class Application_Models_Wiadomosci extends Zend_Db_Table_Abstract {
	
	protected $_name = "wiadomosci";
	protected $_primary = "id";
	private static $tempCounter;
	
	public function getMess($id){
		$select = $this->select();
		$select->where("do=".$id)
			->order("data DESC")
		;
		
		return $this->fetchAll($select);
		
	}
	
	public function getMessIdentity($id,$identity){
		$select = $this->select();
		$select->where("id=".$id);
		$select->where("do=".$identity);
		
		return $this->fetchRow($select);	
	}
	
	public function countMess($id){
		
		if(isset(self::$tempCounter[$id]))
			return self::$tempCounter[$id];
			
		$select = $this->select()
			->from('wiadomosci', array('count(*) as liczba'))
			->where("`read`=0 AND do=".$id);
			
		$mess = $this->fetchAll($select);
		//if(isset($mess[0]['liczba']))
			$i = (int)$mess[0]['liczba'];
		//else
			//$i = 0;
		
		self::$tempCounter[$id] = $i;
		return $i;
	}
	
	public static function newSysMessage($id,$do,$temat,$tresc){
		
		$dane['od'] = $id;
		$dane['do'] = $do;
		$dane['temat'] = $temat;
		$dane['tresc'] = $tresc;
		
		$mess = new Application_Models_Wiadomosci();
		$mess->insert($dane);
		
	}
	
	public static function newMessage($id,$do,$temat,$tresc){
		self::newSysMessage($id,$do,strip_tags($temat),strip_tags($tresc));
	}
	
	public static function staticCount($id){
	
		$mess = new Application_Models_Wiadomosci();
		return $mess->countMess($id);
	}
}

