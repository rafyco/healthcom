<?php 

class Application_Models_Authorize extends Zend_Db_Table_Abstract {
	protected $_name = "authorize";
	protected $_primary = "id";
	
	/**
		Losuje dowolny ci�g znak�w
	*/
	public static function generateKode($liczba=30){
		
		$w = '';
		//Wyst�puj�ce znaki
		$temp = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
		$n=strlen($temp);
		for($i=0;$i<$liczba;$i++){
		
			$w .= substr($temp, rand(0, $n), 1);
		}
		return $w;
	}
	
	public function isKodExist($kod=null){
		if($kod==null){
			$kod = $this->kod;
		}
		$odp = $this->getByKod($kod);
		return isset($odp->kod);	
		
		
	}
		
	public function getByKod($kod){
		return $this->fetchRow("kod LIKE '".$kod."'");
	}
	
	public function getUserByKod($kod){
		$odp = $this->getByKod($kod);
		$user = new Application_Models_Users();
		return $user->fetchRow('id='.$odp->idUsera);
	}
	
		
	public function clearKod($kod){
		$this->delete("kod LIKE '".$kod."'");
	}
	
	public static function setKod($id){
		$ob = new Application_Models_Authorize();
		do{
			$newkod = Application_Models_Authorize::generateKode();
		} while ($ob->isKodExist($newkod));
		
		$dane = Array(
						'kod' => $newkod ,
						'idUsera' => $id);
		$ob->insert($dane);
		
		return $newkod;	
	}
	
}

