<?php 

class Application_Models_Przyjmowanie extends Zend_Db_Table_Abstract {
	
	protected $_name = "przyjmowanie";
	protected $_primary = "idPrzyjmowania";
	private static $przyjmowanie=null;
	
	public function getByPrzyjmowanie($idPrzyjecia,$id){
		$select = $this->select(Zend_Db_Table_Abstract::SELECT_WITH_FROM_PART)
				->setIntegrityCheck(false)
				->joinleft('lekarstwa','lekarstwa.idLeku = przyjmowanie.idLeku')
				->joinleft('rodzaj','rodzajlacz=rodzaj.idRodzaju')
				->where("przyjmowanie.idPrzyjmowania=".$idPrzyjecia)
				->where("idPac=".$id)
				->where('(UNIX_TIMESTAMP(data)+3600) > UNIX_TIMESTAMP(NOW()) AND (UNIX_TIMESTAMP(data)-3600) < UNIX_TIMESTAMP(NOW())')
				
				;
				
		$odp = $this->fetchRow($select);
		
		if(isset($odp->idPrzyjmowania)){
			return $odp;
		} else {
			return null;
		}
				
	}

	public static function getPrzyjmowanie($id){
		if(self::$przyjmowanie == null)
			self::$przyjmowanie = new Application_Models_Przyjmowanie();
		
		$select = self::$przyjmowanie->select(Zend_Db_Table_Abstract::SELECT_WITH_FROM_PART)
				->setIntegrityCheck(false)
				->joinleft('lekarstwa','lekarstwa.idLeku = przyjmowanie.idLeku')
				->joinleft('rodzaj','rodzajlacz=rodzaj.idRodzaju')
				->where("lekarstwa.idLeku=".$id)
				->order('data DESC');
				;
				
		return self::$przyjmowanie->fetchAll($select);
		
	
	}
	
}

