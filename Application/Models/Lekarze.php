<?php 

class Application_Models_Lekarze extends Zend_Db_Table_Abstract {
	protected $_name = "lekarze";
	protected $_primary = "id";
	
	public static function getDoctor($id){
		$doc = new Application_Models_Lekarze();
		return $doc->fetchRow(
			$doc->select(Zend_Db_Table_Abstract::SELECT_WITH_FROM_PART)
					->setIntegrityCheck(false)
					->joinleft('users','lekarze.id=users.id')
					->where('lekarze.id='.$id)		
		);
	}
	
}

