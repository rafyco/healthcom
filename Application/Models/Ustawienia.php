<?php 

class Application_Models_Ustawienia extends Zend_Db_Table_Abstract {
	protected $_name = "ustawienia";
	protected $_primary = "idUsera";
	
	private static $instance = null;
	
	public static function deleteById($id){
		Application_Models_Ustawienia::gI()->delete('IdUsera=$id');
	}
	
	public static function gI(){
		if(self::$instance == null){
			self::$instance = new Application_Models_Ustawienia();
		}
		return self::$instance;
	}
	
	public static function factory($id){
		$ust = Application_Models_Ustawienia::gI();
		$one = $ust->fetchRow("idUsera=$id");
		if(!isset($one->idUsera)){
			$one = $ust->fetchNew();
			$one->idUsera = $id;
			$one->save();
		}
		
		return $one;
	}
	
}

