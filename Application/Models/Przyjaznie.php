<?php 

class Application_Models_Przyjaznie extends Zend_Db_Table_Abstract {
	protected $_name = "przyjaznie";
	protected $_primary = "idprzyjazni";
	
	public static function isFriendStatic($id1,$id2){
		if($id1 == $id2) return true;
		$frend = new Application_Models_Przyjaznie();
		return $frend->isFriend($id1,$id2);
	}
	
	public function isFriend($id1,$id2){
		$odp = $this->fetchRow("idOne=$id1 AND idTwo=$id2 AND isOne=1 AND isTwo=1");
		return isset($odp->idprzyjazni);
	}	
	
	public static function isConnectStatic($id1,$id2){
		$frend = new Application_Models_Przyjaznie();
		return $frend->isConnect($id1,$id2);	
	}
	
	public function isConnect($id1,$id2){
		$odp = $this->fetchRow("idOne=$id1 AND idTwo=$id2");
		return isset($odp->idprzyjazni);
	}
	
	public static function getDane($id,$ida){
		$frend = new Application_Models_Przyjaznie();
		$odp = $frend->fetchRow("idOne=$id AND idTwo=$ida");
		if(!isset($odp->idprzyjazni)) return 0; // Nie istnieje przyja��
		if($odp->isOne == 1){
			if($odp->isTwo ==1 ){
				return 3; // Potwierdzono
			} else {
				return 1; // Ja zaprosi�em
			}
		
		} else {
			return 2;//Zaproszono mnie
		}
		
		
	}
	
	public function add($me,$friend){
		if($me == $fiend) return;
	
		if($this->isConnect($me,$friend)){
			$odp = $this->fetchRow("idOne=$me AND idTwo=$friend");
			$odp2 = $this->fetchRow("idTwo=$me AND idOne=$friend");
		} else {
			$odp = $this->fetchNew();
			$odp2 = $this->fetchNew();
		}
		
		$odp->idOne = $me;
		$odp->idTwo = $friend;
		$odp->isOne = 1;
		$odp->save();
		
		$odp2->idOne = $friend;
		$odp2->idTwo = $me;
		$odp2->isTwo = 1;
		$odp2->save();	
	}
	
	public function del($me,$friend){
		if($me == $friend) return;
		$this->delete("idOne=$me AND idTwo=$friend");
		$this->delete("idTwo=$me AND idOne=$friend");	
	}
	
	public function getList($me){
		$list = $this->fetchAll(
			$this->select(Zend_Db_Table_Abstract::SELECT_WITH_FROM_PART)
			->setIntegrityCheck(false)
			->joinleft('users','idTwo=users.id')
			->where("idOne=$me AND isOne=1 AND isTwo=1 ")
			->order('rand()')
			->limit(5)
			);
		if($list->count() == 0 )
			return null;
		else 
			return $list;
	}
	
	public function getAll($me){
		return $this->fetchAll(
			$this->select(Zend_Db_Table_Abstract::SELECT_WITH_FROM_PART)
			->setIntegrityCheck(false)
			->joinleft('users','idTwo=users.id')
			->where("idOne=$me")
			);
	}
}

