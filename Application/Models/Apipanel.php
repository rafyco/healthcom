<?php 

class Application_Models_Apipanel extends Zend_Db_Table_Abstract {
	protected $_name = "apipanel";
	protected $_primary = "id";
	
	/**
		Losuje dowolny ci�g znak�w
	*/
	public static function generateKode($liczba=30){
		
		$w = '';
		//Wyst�puj�ce znaki
		$temp = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
		$n=strlen($temp);
		for($i=0;$i<$liczba;$i++){
		
			$w .= substr($temp, rand(0, $n), 1);
		}
		return $w;
	}
	
	public function isExistKod($kod){
		$odp = $this->fetchRow("numer LIKE '".$kod."'");
		return isset($odp->numer);
	}
	
	public function getNewKod(){
		do{
			$newKey = self::generateKode(30);
		} while($this->isExistKod($newKey));
		return $newKey;
	}
	
	public function getByKod($kod){
		return $this->fetchRow("numer LIKE '".$kod."'");
	}
		
}

