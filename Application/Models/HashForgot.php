<?php 

class Application_Models_HashForgot extends Zend_Db_Table_Abstract {
	protected $_name = "hashforgot";
	protected $_primary = "id";
	
	/**
		Losuje dowolny ci�g znak�w
	*/
	public static function generateKode($liczba=30){
		
		$w = '';
		//Wyst�puj�ce znaki
		$temp = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
		$n=strlen($temp);
		for($i=0;$i<$liczba;$i++){
		
			$w .= substr($temp, rand(0, $n), 1);
		}
		return $w;
	}
	
	public function isKodExist($kod){
		
		$odp = $this->getByKod($kod);
		return isset($odp->kod);	
		
		
	}
	
	public function getByKod($kod){
		return $this->fetchRow("kod LIKE '".$kod."'");
	}
	
	public function getUserByKod($kod){
		$odp = $this->getByKod($kod);
		Zend_Loader::loadClass('Application_Models_Users');
		$user = new Application_Models_Users();
		return $user->fetchRow('id='.$odp->idUsera);
	}
	
	public function clearBase(){
		//TODO		
			
			
			//Usuwanie wpis�w starszych ni� 3 godziny.
			$date = date('Y-m-d H:i:s', strtotime('-3 hours'));
			$this->delete("data<'".$date."' AND data>0");
			
		
	}
	
	public static function setKod($id,$przel = 1){
		$ob = new Application_Models_HashForgot();
		do{
			$newkod = self::generateKode();
		} while ($ob->isKodExist($newkod));
		
		$osoba = $ob->fetchRow("idUsera=".$id);
		
		if(isset($osoba->id)){
			$ob->update(array('kod' => $newkod),"idUsera=".$id);
		} else {		
			$dane = Array(
							'kod' => $newkod ,
							'idUsera' => $id);
			if($przel == 0){
				$dane['data'] = 0;
			}
							
			$ob->insert($dane);
		}
		return $newkod;	
	}
	
	public function clearKod($kod){
		$this->delete("kod LIKE '".$kod."'");
	}
		
}

