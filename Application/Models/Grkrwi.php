<?php 

class Application_Models_Grkrwi extends Zend_Db_Table_Abstract {
	protected $_name = "grkrwi";
	protected $_primary = "id";
	
	public function getGrKrwi($numer){
		$krew = $this->fetchRow("id=".$numer);
		$gr = $krew->grupa;
		if($krew->rh){
			$gr .= " rh+";
		} else {
			$gr .= " rh-";
		}
		return $gr;
	}
	
	public function toOwnArray(){
		$lista = $this->fetchAll();
		
		foreach($lista as $li){
			$tab[$li->id] = $this->getGrKrwi($li->id);
		}
		
		return $tab;
	}
	
	public static function toArray(){
		$ob = new Application_Models_Grkrwi();
		return $ob->toOwnArray();
	}
	
}

