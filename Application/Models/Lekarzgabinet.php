<?php 

class Application_Models_Lekarzgabinet extends Zend_Db_Table_Abstract {
	protected $_name = "lekarzgabinet";
	protected $_primary = "idpol";
	
	/**
		Zwraca napis oznaczaj�cy grup� krwi wed�ug numeru grupy
	*/
	public static function getByDoctor($id){
		$ob = new Application_Models_Lekarzgabinet();
		$select = $ob->select(Zend_Db_Table::SELECT_WITH_FROM_PART)
                        ->setIntegrityCheck(false);
						
		$select->joinleft('gabinety','idGabinet = gabinety.id')->where('idLekarza='.$id);
		
		
		return $ob->fetchAll($select);
	}
	
	public static function connect($idDok,$idGab){
		$gab = new Application_Models_Lekarzgabinet();
		
		$odp = $gab->fetchRow("idLekarza=".$idDok." AND idGabinet=".$idGab);
		
		if(isset($odp->id)){
			return false;
		}
		
		$dana['idLekarza'] = $idDok;
		$dana['idGabinet'] = $idGab;
		$gab->insert($dana);
		return true;
		
	}
	
	
	
}

