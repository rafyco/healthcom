<?php 

class Application_Models_Dzienniczek extends Zend_Db_Table_Abstract {
	protected $_name = "dzienniczek";
	protected $_primary = "idDzienniczka";
	
	public function getQuestion($identity){
	
		if($identity->rola == 'doctor'){
			
			return $this->select(Zend_Db_Table_Abstract::SELECT_WITH_FROM_PART)
				->setIntegrityCheck(false)
				->joinleft('users','idPacjenta = users.id')
				->joinleft('pacjenci','users.id=pacjenci.id')
				->joinleft('przyjaznie','idPacjenta = przyjaznie.idOne')
				->where('(idDoc='.$identity->id)
				->orwhere('isOne=1 AND isTwo=1 AND przyjaznie.idTwo='.$identity->id.' AND (uprawnienia=0 OR uprawnienia=1)')
				->orwhere('idPacjenta='.$identity->id.' )')
				->group('idDzienniczka')
				->order('data DESC');
			
		} 
		
		
		if($identity->rola == 'patient'){
			
			
			return $this->select(Zend_Db_Table_Abstract::SELECT_WITH_FROM_PART)
				->setIntegrityCheck(false)
				->joinleft('users','idPacjenta = users.id')
				->joinleft('pacjenci','users.id=pacjenci.id')
				->joinleft('przyjaznie','idPacjenta = przyjaznie.idOne')
				->where('(users.id='.Application_Models_Pacjenci::getIdDoctor($identity))
				->orwhere('isOne=1 AND isTwo=1 AND przyjaznie.idTwo='.$identity->id.' AND (uprawnienia=0 OR uprawnienia=1)')
				->orwhere('idPacjenta='.$identity->id.' )')
				->group('idDzienniczka')
				->order('data DESC');
		}
		
		if($identity->rola == 'viewer'){
			return $this->select(Zend_Db_Table_Abstract::SELECT_WITH_FROM_PART)
				->setIntegrityCheck(false)
				->joinleft('users','idPacjenta = users.id')
				->join('udostepnienia','idPacjenta = udostepnienia.idPac')
				->where('(idViewer='.$identity->id.' )')
				->group('idDzienniczka')
				->order('data DESC');
		}
		
		if(true/* inni użytkownicy */){
			return $this->select(Zend_Db_Table_Abstract::SELECT_WITH_FROM_PART)
				->setIntegrityCheck(false)
				->joinleft('users','idPacjenta = users.id')
				->join('przyjaznie','idPacjenta = przyjaznie.idOne')
				->where('(isOne=1 AND isTwo=1 AND przyjaznie.idTwo='.$identity->id.' AND (uprawnienia=0 OR uprawnienia=1)')
				->orwhere('idPacjenta='.$identity->id.' )')
				->group('idDzienniczka')
				->order('data DESC');
		}
	}
	
	/**
		Zwraca wszystkie aktualności dla zalogowanego pacjenta
	*/
	public function getAktual($identity){
		return $this->fetchAll($this->getQuestion($identity));
	}
	
	public static function isMyWpis($id,$idwpisu){
		$dzienniczek = new Application_Models_Dzienniczek();
		$user = new Application_Models_Users();
		
		$select = $dzienniczek->getQuestion($user->getUserById($id));
		$select->where("idDzienniczka = $idwpisu");
		$odp = $dzienniczek->fetchRow($select);
		return isset($odp->idDzienniczka);
	}
	
	public static function isWpis($id){
		$dzienniczek = new Application_Models_Dzienniczek();
		$odp = $dzienniczek->fetchRow("id=".$id);
		return isset($odp->id);
	
	}
	
	public function lastWpis($id){
		$select = $this->select()->where('idPacjenta='.$id." AND (cisskurcz <> 0 OR cisrozkurcz <> 0 OR samopoczucie <> 0)")->order('data DESC');
		
		$odp = $this->fetchRow($select);
		
		if(!isset($odp['data'])){
			return 0;
		} else {
			return $odp['data'];
		}
	}
	
	public function isMust($id){
		$temp = $this->lastWpis($id);
		if( $temp != 0){
			$date = new DateTime($temp);
		} else {
			return true;
		}
		
		
		$today = new DateTime();
		
		return ($today->format('U') - $date->format('U'))>259200; // 3dni
	}
	
	public static function isMuststatic($id){
		$dzienniczek = new Application_Models_Dzienniczek();
		return $dzienniczek->isMust($id);
	
	}
	
	public function getWyniki($id){
		$select = $this->select()->where('idPacjenta='.$id." AND (cisskurcz <> 0 OR cisrozkurcz <> 0 OR samopoczucie <> 0)")->order('data DESC');
		return $this->fetchAll($select);
	}
	
	public function getWynikiwykres($id){
		$select = $this->select()->where('idPacjenta='.$id." AND (cisskurcz <> 0 OR cisrozkurcz <> 0 OR samopoczucie <> 0)")->order('data DESC')->limit(100);
		return $this->fetchAll($select);
	}
	
	public function delWpis($id){
		$this->delete("idDzienniczka=$id");
		
		$koment = new Application_Models_Komentarze();
		$koment->delete("idPosta=$id");
		
	}
	
}

