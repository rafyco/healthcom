<?php 

class Application_Models_Pacjenci extends Zend_Db_Table_Abstract {
	protected $_name = "pacjenci";
	protected $_primary = "id";
	
	/**
		Zwraca napis oznaczaj�cy grup� krwi wed�ug numeru grupy
	*/
	public static function getGrKrwi($numer){
		Zend_Loader::loadClass('Application_Models_Grkrwi');
		$krew = new Application_Models_GrKrwi();
		return $krew->getGrKrwi($numer);
	}
	
	public function getByDoctor($id){
			
			return $this->fetchAll(
				$this->select(Zend_Db_Table_Abstract::SELECT_WITH_FROM_PART)
					->setIntegrityCheck(false)
					->joinleft('lekarze','pacjenci.idDoc=lekarze.id')
					->joinleft('users','pacjenci.id=users.id')
					->where('idDoc='.$id)
			);
			
	}
	
	public function getPatient($id){
		return $this->fetchRow('id='.$id);
	}
	
	public static function getDoctorByID($id){
		$pacjenci = new Application_Models_Pacjenci();
		$users = new Application_Models_Users();
		
		
		$iddoc = $pacjenci->fetchRow("id=".$id);
			
		$us = $users->getUserById($iddoc->idDoc);
		
		if(isset($us->id)){
			return $us;
		} else {
			return null;
		}
		
	}
	
	public static function getDoctor($identity){
		$pacjenci = new Application_Models_Pacjenci();
		$users = new Application_Models_Users();
		if($identity->rola != 'patient'){
			return null;
		}
		
		$id = $pacjenci->fetchRow("id=".$identity->id);
				
		return $users->getUserById($id->idDoc);
	}
	
	public static function getIdDoctor($identity){
		//$doctor = self::getDoctor($identity);
		if(!isset($identity->id)){
			return 0;
		} 
		return $identity->id;
	}
	
	public static function isMyDoctor($idpac,$iddoc){
		$pacjenci = new Application_Models_Pacjenci();
		$pac = $pacjenci->fetchRow("id=".$idpac);
		return ($pac->idDoc == $iddoc) && ($pac->isDoc==1);
	}
	
	public function countByDoc($id){
		
		$select = $this->select()
			->from('pacjenci', array('count(*) as liczba'))
			->where("idDoc=".$id." AND isDoc=0");
			
		$pac = $this->fetchAll($select);
		return (int)$pac[0]['liczba'];
	}
	
	public static function staticCountByDoc($id){
		$pacjent = new Application_Models_Pacjenci();
		return $pacjent->countByDoc($id);
	
	}
	
	public function getOneBy($id){
			return $this->fetchRow(
				$this->select(Zend_Db_Table_Abstract::SELECT_WITH_FROM_PART)
					->setIntegrityCheck(false)
					->joinleft('lekarze','pacjenci.idDoc=lekarze.id')
					->joinleft('users','pacjenci.id=users.id')
					->where('users.id='.$id)
			);
	
	}
}

