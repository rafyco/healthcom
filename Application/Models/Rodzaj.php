<?php 

class Application_Models_Rodzaj extends Zend_Db_Table_Abstract {
	
	protected $_name = "rodzaj";
	protected $_primary = "idRodzaju";
	
	public static function toArray(){
		$rodzaj = new Application_Models_Rodzaj();
		$odp = $rodzaj->fetchAll();
		foreach($odp as $pole){
			$array[$pole->idRodzaju] = $pole->rodzaj;
		}
		
		if(isset($array)) 
			return $array;
		return null;
	}
	
}

