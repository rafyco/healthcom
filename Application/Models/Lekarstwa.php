<?php 

class Application_Models_Lekarstwa extends Zend_Db_Table_Abstract {
	
	protected $_name = "lekarstwa";
	protected $_primary = "idLeku";
	
	protected static $cachedate=null;
	
	public static function getAllFor($id){
		$leki = new Application_Models_Lekarstwa();
		$select = $leki->select(Zend_Db_Table_Abstract::SELECT_WITH_FROM_PART)
				->setIntegrityCheck(false)
				->joinleft('przyjmowanie','lekarstwa.idLeku = przyjmowanie.idLeku')
				->joinleft('rodzaj','rodzajlacz=rodzaj.idRodzaju')
				->where('idPac='.$id)
				->group('przyjmowanie.idPrzyjmowania')
				->order('data DESC');
			
		
		return $leki->fetchAll($select);
	}
	
	public static function getForTree($id){
		$leki = new Application_Models_Lekarstwa();
		$select = $leki->select(Zend_Db_Table_Abstract::SELECT_WITH_FROM_PART)
				->setIntegrityCheck(false)
				->joinleft('przyjmowanie','lekarstwa.idLeku = przyjmowanie.idLeku')
				->joinleft('rodzaj','rodzajlacz=rodzaj.idRodzaju')
				->where('idPac='.$id)
				->group('lekarstwa.idLeku')
				->order('data DESC');
			
		
		return $leki->fetchAll($select);
	}
	
	public function getOne($id){
		return $this->fetchRow("idLeku=".(int)$id);
	}
	
	public function getToHave($id){
		$id=(int)$id;
		if(!isset(self::$cachedate[$id])){
			$select = $this->select(Zend_Db_Table_Abstract::SELECT_WITH_FROM_PART)
				->setIntegrityCheck(false)
				->joinleft('przyjmowanie','lekarstwa.idLeku = przyjmowanie.idLeku')
				->joinleft('rodzaj','rodzajlacz=rodzaj.idRodzaju')
				->where('idPac='.$id)
				->where('(UNIX_TIMESTAMP(data)+3600) > UNIX_TIMESTAMP(NOW()) AND (UNIX_TIMESTAMP(data)-3600) < UNIX_TIMESTAMP(NOW())')
				->where('isprzyjal=0')
				->group('przyjmowanie.idPrzyjmowania')
				->order('data DESC');
		
			self::$cachedate[$id] = $this->fetchAll($select);
		}
		return self::$cachedate[$id];
	}
	
	public function getToHaveCount($id){
		$temp = $this->getToHave($id);
		return count($temp);
	}
	
	public function delLekById($id){
		$id = (int)$id;
		$this->delete("idLeku=".$id);
		$przyjmowanie = new Application_Models_Przyjmowanie();
		$przyjmowanie->delete("idLeku=".$id);
	}
	
	public function isCanDel($idDoc,$idLek){
		
		$lek = $this->fetchRow("idLeku=".(int)$idLek);
		$pac = (int)$lek->idPac;
		
		return Application_Models_Pacjenci::isMyDoctor($pac,$idDoc);
		
	}

	public function isCanAccept($idPac,$idLek){
		$lek = $this->fetchRow("idLeku=".(int)$idLek);
		return (int)$lek->idPac == $idPac;
	}
}

