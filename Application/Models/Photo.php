<?php 

class Application_Models_Photo {

	private static $base = null;
	
	public static function getPhoto($id,$base=null){
		if(self::$base == null){
			self::$base = Zend_Controller_Front::getInstance()->getBaseUrl();
		}
		return self::$base."/photo/index/id/$id";
	}
	
	public static function getMini($id,$base=null){
		if(self::$base == null){
			self::$base = Zend_Controller_Front::getInstance()->getBaseUrl();
		}
		return self::$base."/photo/mini/id/$id";
	}
	
}

