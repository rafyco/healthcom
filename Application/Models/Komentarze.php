<?php 

class Application_Models_Komentarze extends Zend_Db_Table_Abstract {
	protected $_name = "komentarze";
	protected $_primary = "idkoment";
	
	public static function getAll($idposta){
		$kom = new Application_Models_Komentarze();
		return $kom->fetchAll(
				$kom->select(Zend_Db_Table_Abstract::SELECT_WITH_FROM_PART)
					->setIntegrityCheck(false)
					->joinleft('users','idAutora = users.id')
					->where('idPosta='.$idposta)
			);

	}	
	
	public function countFor($id){
		$odp = $this->fetchAll($this->select()->from('komentarze', array('count(*) as liczba'))
			->where("idPosta=".$id));
		return $odp[0]['liczba'];
	}
}

