<?php 
	
Zend_Loader::loadClass('Zend_Log');
Zend_Loader::loadClass('Zend_Log_Writer_Stream');

	class Rafyco_Apilog{
	
		private static $instance=null;
		private $log;
				
		private function __construct (){
			$this->log = new Zend_Log();
			$this->log->addWriter(new Zend_Log_Writer_Stream('./Application/Api.log'));			
		}
		
		public static function getInstance(){
			if(self::$instance == null)
				self::$instance = new Rafyco_Apilog();
			return self::$instance;
		}
		
		public function log($action, $kod,$param=""){
			$this->log->info("akcja: $action Sesja: $kod $param");
		}
		
		public function login($login, $kod,$param="Zalogowano"){
			$this->log->info("$param jako $login; Kod: $kod");
		}
		
			
	}