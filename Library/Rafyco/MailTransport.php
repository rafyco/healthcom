<?php 

	class Rafyco_MailTransport extends Zend_Mail_Transport_Sendmail{
		protected $_savePath;

		public function getSavePath(){
			return $this->_savePath;
		}

		public function setSavePath($path){
			$this->_savePath = $path;
		}

		function _sendMail(){
		
			$fileName = time() . '.htm';
			$data = '<ul><li>Subject: ' . $this->_mail->getSubject() . "</li>\n"
				  . '<li>To: ' . $this->recipients . "</li></ul>\n"
				  . $this->header . "\n\n"
				  . $this->body;
			file_put_contents($this->getSavePath() . '/' . $fileName, $data);
		}
	}
	