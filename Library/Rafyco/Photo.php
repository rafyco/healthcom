<?php 

	class Rafyco_Photo{
	
		private $zd = null;
		private $address;
		
		public function __construct($ad=null){
				$this->address = $ad;
		}
		
		public function open($adres){
			if (is_file($adres)){
				$roz = @getimagesize($adres);
				$roz = $roz['mime'];
				if(($roz=="image/jpeg")||($roz=="image/jpg")){
					$this->zd = imagecreatefromjpeg($adres);		
					return true;
				} else if($roz="image/png") {
					$this->zd = imagecreatefrompng($adres);		
					return true;
				} else
					throw new Exception("Nie mo�na wczyta� pliku");
			} else 
				return false;
		
		}
		
		public function resize($x=40,$y=40){
			
			$img = imagecreatetruecolor($x,$y);
			
			imagecopyresized($img,$this->zd,0,0,0,0,$x,$y,imagesx($this->zd),imagesy($this->zd));
						
			$this->zd = $img;
			
			return $this;
		}
		
		public function display(){
			$this->ver();
			imagejpeg($this->zd);
			return $this;
		}
		
		public function __destruct(){
			imagedestroy($this->zd);
		}
		
		public function ver(){
			try{
				if($this->zd == null){
					$this->open($this->address);
				}
			}catch(Exception $ex){}
		}
		
		public function save($adres){
			if(is_file($adres)){
				unlink($adres);
			}
			$this->ver();
			imagejpeg($this->zd,$adres);
			
		}
		
		public function delete($id,$string){ // './database/photo/*ID*.jpg'
		
			$adress[0] = str_replace("*ID*",$id."_mini",$string);
			$adress[1] = str_replace("*ID*",$id."_mobile",$string);
			
			foreach($adress as $ad){
				if(is_file($ad)){
					unlink($ad);
				}
			}
			
		}
	
	
	}