<?php 
	
Zend_Loader::loadClass('Zend_Log');
Zend_Loader::loadClass('Zend_Log_Writer_Stream');

	class Rafyco_Logi{
	
		private static $instance=null;
		private $log;
		private $temp;
				
		private function __construct (){
			$this->log = new Zend_Log();
			$this->log->addWriter(new Zend_Log_Writer_Stream('./Application/healthcom.log'));			
		}
		
		public function log($haslo){
			$this->log->info($haslo);
		}
		
		public function emerg($haslo){
			$this->log->emerg($haslo);
		}
		
		public static function getInstance(){
			if(self::$instance==null){
				self::$instance = new Rafyco_Logi();
			}
			return self::$instance;
		}
		
		/** 
			Dodaje wiadomo�� do wy�wietlenia na nowej stronie
		*/
		public function addText($text){
			$session = new Zend_Session_Namespace('message');
			$this->temp = $text;
			if(!$session->message){
				$session->message = $text;
			} else {
				$session->message .= "<br />".$text;
			}
			
			return $this;
		}
		
		/**
			Dodaje do log�w ostatni� wiadomo�� do wy�wietlenia na nowej stronie
		*/
		public function toLog($typ ='info'){
			if($typ=='info')
				$this->log($this->temp);
			else 
				$this->emerg($this->temp);
		}
		
		/**
			Sprawdza czy jest istnieje wiadomo�� do wy�wietlenia
		*/
		public function isText(){
			$session = new Zend_Session_Namespace('message');
			return (bool)$session->message;
		}
		
		/**
			Pobiera wiadomo�� i kasuje j�
		*/
		public function getText(){
			$session = new Zend_Session_Namespace('message');
			$w = $session->message;
			$session->message = null;
			return $w;
		}
		
			
	}