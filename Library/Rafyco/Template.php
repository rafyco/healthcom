<?php 

	class Rafyco_Template{
	
		public static function noBBcode(){
			return new Rafyco_Template();
		}
	
		public static function telefon($tel){
			return "+".substr($tel,0,2)." ".substr($tel,2,3)." ".substr($tel,5,3)." ".substr($tel,8,3)." ";
		}
		
		private static function kod($kod){
			return substr($kod,0,2)."-".substr($kod,2,3);
		}
		
		public static function adres($ul,$nr,$kod,$miasto,$separator = "<br />\n"){
			return $ul." ".$nr.$separator.self::kod($kod)." ".$miasto;
		}
		
		public static function data($date,$bool=true){
			$today = time();
			
			$wpisu = mktime($date->format("H"),$date->format("i"),$date->format("s"),$date->format("m"),$date->format("d"),$date->format("Y"));
			
			$temu = $today - $wpisu;
			
			if($bool){
				if($temu <= 0){
					$w = $date->format("H:i d M Y")."r.";
				
					$co = array('Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec');
					$naco = array('Sty','Lut','Mar','Kwi','Maj','Cze','Lip','Sie','Wrz','Paź','Lis','Gru');
						
					return str_replace($co,$naco,$w);				
				}
				
				if($temu<60){
					$mess = round($temu/10)."0 s";
				}else if($temu<3600){
					$mess = round($temu/60)." min";
				}else if($temu<86400){
					$mess = round($temu/3600)." godz";
				} else if($temu < 2592000){
					$n = round($temu/86400);
					if($n == 1) 
						$mess = "1 dzień";
					else 
						$mess = round($temu/86400)." dni";
				} else if($temu < 31104000){
					$mess = round($temu/2592000)." miesięcy";
				} else {
					$mess = round($temu/31104000);
					switch($mess){
						case 1: 
							$mess = 'rok';
							break;
						case 2:
						case 3:
						case 4:
							$mess .= ' lata';
							break;
						default:
							$mess .= ' lat';
					
					}
					
				
				}
			
				$w = $date->format("H:i d M Y")."r. (".$mess." temu)";
				
				$co = array('Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec');
				$naco = array('Sty','Lut','Mar','Kwi','Maj','Cze','Lip','Sie','Wrz','Paź','Lis','Gru');
					
				return str_replace($co,$naco,$w);
				
			} else {
				$w = $date->format("H:i d M Y")."r.";
				
				$co = array('Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec');
				$naco = array('Sty','Lut','Mar','Kwi','Maj','Cze','Lip','Sie','Wrz','Paź','Lis','Gru');
					
				return str_replace($co,$naco,$w);
			}
		}
		
		public static function rola($rola){
			$tab['admin'] = 'Administrator';
			$tab['doctor'] = 'Lekarz';
			$tab['patient'] = 'Pacjent';
			$tab['viewer'] = 'Widz';
			
			if(isset($tab[$rola])){
				return $tab[$rola];
			} else {
				return 'nieznany';
			}
		
		}
		
		public static function email($email='',$temat='',$body=''){
		
			$w = Zend_Controller_Front::getInstance()->getBaseUrl();
			
			$w .= '/comm/send?';
			
			if($email != ''){
				$w .= "mailto=$email";
			}
			
			if($temat != ''){
				$w .= "temat=$temat";
			}
			
			if($body != ''){
				$w .= "body=$body";
			}
			
			return $w;
		}
		
		public function render($text){
			$pattern = array(
				'/\[b\](.*?)\[\/b\]/Dius',
				'/\[i\](.*?)\[\/i\]/Dius',
				'/\[cite\](.*?)\[\/cite\]/Dius',
				'/\[del\](.*?)\[\/del\]/Dius',
				'/\[ins\](.*?)\[\/ins\]/Dius',
				'/\[sup\](.*?)\[\/sup\]/Dius',
				'/\[sub\](.*?)\[\/sub\]/Dius',
				'/\[span\](.*?)\[\/span\]/Dius',
				'/\[acronym\=(.*?)\](.*?)\[\/acronym\]/Dius',
				'/\[h1\](.*?)\[\/h1\]/Dius',
				'/\[img\](.*?)\[\/img\]/Dius',
				'/\[url\=(.*?)\](.*?)\[\/url\]/Dius',
				'/\[url\](.*?)\[\/url\]/Dius',
				'<br />',
				'/<a href\=(.*?)>(.*?)<\/a>/Dius',
				'/<h2>/','/<\/h2>/',
				'/<p>/','/<\/p>/'
				
			);
			
			$replace = array(
				'\\1', //b
				'\\1', //i
				'\\1', //cite
				'\\1', //del
				'\\1', //ins
				'\\1', //sup
				'\\1', //sub
				'\\1', //span
				'\\2', //acronym
				'\\1', //h1
				"\n(obrazek)\n", //img
				'\\2 (\\1)', //url
				'\\1 ', //url
				"\n",
				'\\2 (\\1)',
				'',"\n\n",
				'',"\n\n"
			);
			
			return preg_replace($pattern,$replace,$text);
		}
	
	
	}