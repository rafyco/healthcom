<?php 

class Krzycholud_Filters_AddressCode implements Zend_Filter_Interface{
    
	/**
		Ten filtr ma przekonwertowa� napisy w formacie xx-xxx do xxxxx tzn usuwa� zb�dny my�lnik je�li istnieje
	*/
	public function filter($value){
        // przeprowad� jakie� transformacje zmiennej $value do $valueFiltered
		$valueFiltered = str_replace("-"," ",$value);
        return $valueFiltered;
    }
}